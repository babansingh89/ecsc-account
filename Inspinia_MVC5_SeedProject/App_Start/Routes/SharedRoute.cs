﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace wbEcsc.App_Start.Routes
{
    public class SharedRoute: IRoute
    {
        private RouteCollection _routes { get; set; }
        public SharedRoute(RouteCollection routes)
        {
            this._routes = routes;
        }
        public void MapRoute()
        {
            _routes.MapRoute(
                name: "SharedRouter",
                url: "Shared/{controller}/{action}/{id}"
                ,defaults: new { controller="abc", action="act", id = UrlParameter.Optional }
                , namespaces: new[] { "wbEcsc.Controllers.Shared" }
            );
        }
    }
}