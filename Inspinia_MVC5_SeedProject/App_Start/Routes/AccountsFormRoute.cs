﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace wbEcsc.App_Start.Routes
{
    public class AccountsFormRoute : IRoute
    {
        private RouteCollection _routes { get; set; }
        public AccountsFormRoute(RouteCollection routes)
        {
            this._routes = routes;
        }
        public void MapRoute()
        {
            _routes.MapRoute(
                name: "AccountsFormRouter",
                url: "Accounts_Form/{controller}/{action}/{id}",
                defaults: new { id = UrlParameter.Optional },
                namespaces: new[] { "wbEcsc.Controllers.Accounts_Form" }
            );
        }
    }
}