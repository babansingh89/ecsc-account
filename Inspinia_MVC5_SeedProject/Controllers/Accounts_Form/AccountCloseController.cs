﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class AccountCloseController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();

        SessionData sData;
        public AccountCloseController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View("~/Views/Accounts/AccountClose.cshtml");
        }

        public ActionResult CloseAccount(string SectorID, string FinYear, string TransType, string ClosingDate)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PROC_Acc_Account_Close", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            cmd.Parameters.AddWithValue("@TransBy", sData.UserID);
            cmd.Parameters.AddWithValue("@TransOn", ClosingDate);
            cmd.Parameters.AddWithValue("@SectorId", SectorID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        public ActionResult AccountClosingStatus(string MenuId, string TransDt, string SectId, string FromModule)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Proc_Account_Closing_Status", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MenuId", MenuId);
            cmd.Parameters.AddWithValue("@TransDt", TransDt);
            cmd.Parameters.AddWithValue("@SectId", SectId);
            cmd.Parameters.AddWithValue("@FromModule", FromModule);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }
    }
}