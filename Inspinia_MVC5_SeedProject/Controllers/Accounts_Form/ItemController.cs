﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.IO;
using wbEcsc.App_Start;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class ItemController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;

        public ItemController()
        {
            sData = SessionContext.SessionData;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        if (dr[column.ColumnName] == DBNull.Value)
                            pro.SetValue(obj, null, null);
                        else
                            pro.SetValue(obj, dr[column.ColumnName], null);
                    }
                    else
                        continue;
                }
            }
            return obj;
        }
        private static T GetItem1<T>(DataTable dt)
        {
            Type temp = typeof(T);

            T data = Activator.CreateInstance<T>();
            foreach (DataRow row in dt.Rows)
            {
                data = GetItem<T>(row);
            }


            return data;
        }
        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View("~/Views/Accounts/Item.cshtml");
        }

        [HttpPost]
        public JsonResult InsertUpdate(Item_MD item, List<Item_MD> CatDesc)
        {
            try
            {
                string result = new Item_BLL().InsertUpdate(item, CatDesc, sData.UserID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult PO_InsertUpdate(PO_MD POMaster, List<PODetail_MD> PODetail, List<POSub_MD> POSub)
        {
            try
            {
                string result = new PO_BLL().InsertUpdate(POMaster, PODetail, POSub, sData.UserID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult BILL_InsertUpdate(Bill_MD BillMaster, List<BillDetail_MD> BillDetail, List<BillSub_MD> BillSub, List<BillSubVch_MD> BillAccVch)
        {
            try
            {
                string result = new PO_BLL().InsertUpdate_Bill(BillMaster, BillDetail, BillSub, BillAccVch, sData.UserID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Payment_InsertUpdate(Bill_MD BillMaster, List<BillDetail_MD> BillDetail)
        {
            try
            {
                string result = new PO_BLL().InsertUpdate_Payment(BillMaster, BillDetail, sData.UserID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", result);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Load_Item(string AccountCode)
        {
            Item_MD item = new Item_MD();
            var catList = item;
            List<Item_MD> lst;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Load_Item", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode == "" ? DBNull.Value : (object)AccountCode);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                var master = CommonMethod.ConvertToList<Item_MD>(ds.Tables[0]);

                cr.Data = master;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Load_ItemDetail(string AccountCode)
        {
            Item_MD item = new Item_MD();
            var master = item;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Load_Item", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode == "" ? DBNull.Value : (object)AccountCode);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                item = CommonMethod.ConvertToList<Item_MD>(ds.Tables[0])[0];

                if (ds.Tables[1] != null)
                {
                    item.CatDetail = ConvertDataTable<ItemCatDetail_MD>(ds.Tables[1]);
                }

                master = item;
                cr.Data = master;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult UploadPicture()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        string fileWithoutEXT = Path.GetFileNameWithoutExtension(fname);
                        string fileWithEXT = fname;

                        //deleting code starts here
                        string dfname = Path.Combine(Server.MapPath("~/UploadItemImage/Item/"));
                        string[] dfiles = System.IO.Directory.GetFiles(dfname, fileWithoutEXT + ".*");
                        foreach (string f in dfiles)
                        {
                            System.IO.File.Delete(f);
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/UploadItemImage/Item/"), fname);
                        file.SaveAs(fname);


                        using (SqlConnection con = new SqlConnection())
                        {
                            con.ConnectionString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString; 
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.Connection = con;
                                cmd.CommandText = "UPDATE account.MST_Accounts SET DocFilePath='/UploadItemImage/Item/" + file.FileName + "' WHERE AccountCode ='" + fileWithoutEXT + "'";
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }
                    // Returns message that successfully uploaded  

                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details:" + ex.Message); 
                }
            }
            else
            {
                return Json("Data Saved But No File Selected");  
            }
        }

        [HttpPost]
        public ActionResult UploadPicture_Expense()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        string fileWithoutEXT = Path.GetFileNameWithoutExtension(fname);
                        string fileWithEXT = fname;

                        //deleting code starts here
                        string dfname = Path.Combine(Server.MapPath("~/UploadItemImage/PO/"));
                        string[] dfiles = System.IO.Directory.GetFiles(dfname, fileWithoutEXT + ".*");
                        foreach (string f in dfiles)
                        {
                            System.IO.File.Delete(f);
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/UploadItemImage/PO/"), fname);
                        file.SaveAs(fname);


                        using (SqlConnection con = new SqlConnection())
                        {
                            con.ConnectionString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.Connection = con;
                                cmd.CommandText = "UPDATE account.MST_Acc_PO SET DocFilePath='/UploadItemImage/PO/" + fileWithEXT + "' WHERE PoId ='" + fileWithoutEXT + "'";
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }
                    // Returns message that successfully uploaded  

                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details:" + ex.Message);
                }
            }
            else
            {
                return Json("Data Saved But No File Selected");
            }
        }

        [HttpPost]
        public ActionResult UploadPicture_bill()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        string fileWithoutEXT = Path.GetFileNameWithoutExtension(fname);
                        string fileWithEXT = fname;

                        //deleting code starts here
                        string dfname = Path.Combine(Server.MapPath("~/UploadItemImage/Bill/"));
                        string[] dfiles = System.IO.Directory.GetFiles(dfname, fileWithoutEXT + ".*");
                        foreach (string f in dfiles)
                        {
                            System.IO.File.Delete(f);
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/UploadItemImage/Bill/"), fname);
                        file.SaveAs(fname);


                        using (SqlConnection con = new SqlConnection())
                        {
                            con.ConnectionString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.Connection = con;
                                cmd.CommandText = "UPDATE account.MST_Acc_Bill SET DocFilePath='/UploadItemImage/Bill/" + fileWithEXT + "' WHERE BillId ='" + fileWithoutEXT + "'";
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }
                    // Returns message that successfully uploaded  

                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details:" + ex.Message);
                }
            }
            else
            {
                return Json("Data Saved But No File Selected");
            }
        }

        [HttpPost]
        public ActionResult UploadPicture_payment()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        string fileWithoutEXT = Path.GetFileNameWithoutExtension(fname);
                        string fileWithEXT = fname;

                        //deleting code starts here
                        string dfname = Path.Combine(Server.MapPath("~/UploadItemImage/Payment/"));
                        string[] dfiles = System.IO.Directory.GetFiles(dfname, fileWithoutEXT + ".*");
                        foreach (string f in dfiles)
                        {
                            System.IO.File.Delete(f);
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/UploadItemImage/Payment/"), fname);
                        file.SaveAs(fname);


                        using (SqlConnection con = new SqlConnection())
                        {
                            con.ConnectionString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.Connection = con;
                                cmd.CommandText = "UPDATE account.MST_Acc_Bill SET DocFilePath='/UploadItemImage/Payment/" + fileWithEXT + "' WHERE BillId ='" + fileWithoutEXT + "'";
                                con.Open();
                                cmd.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }
                    // Returns message that successfully uploaded  

                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details:" + ex.Message);
                }
            }
            else
            {
                return Json("Data Saved But No File Selected");
            }
        }

        [HttpPost]
        public ActionResult Delete_Picture(string transactionType, string ID, string urls)
        {
            string fileWithoutEXT = ID;

            //deleting code starts here
            string dfname = Path.Combine(Server.MapPath(urls));
            string[] dfiles = System.IO.Directory.GetFiles(dfname, fileWithoutEXT + ".*");
            foreach (string f in dfiles)
            {
                System.IO.File.Delete(f);
            }

            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Delete_TableImage", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", transactionType);
            cmd.Parameters.AddWithValue("@ID", ID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                cr.Data = "success";
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = "fail";
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Load_TaxTypeDetail(string TaxTypeID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Load_TaxTypeDetail", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TaxTypeID", TaxTypeID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Load_PO(string POID, string TransactionType, string TransType)
        {
            PO_MD obj = new PO_MD();
            var record = obj;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Load_PO", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType == "" ? "Select" : TransactionType);
            cmd.Parameters.AddWithValue("@POID", POID == "" ? DBNull.Value : (object)POID);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                con.Open();
                da.Fill(ds);

                cr.Data = ds;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", ds.Tables.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Load_AutoPO(string Desc)
        {
            PO_MD obj = new PO_MD();
            var record = obj;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Load_PO", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", "Load");
            cmd.Parameters.AddWithValue("@Desc", Desc == "" ? DBNull.Value : (object)Desc);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Load_BillAccHead(string TransType, string SectorID)
        {
            PO_MD obj = new PO_MD();
            var record = obj;
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Load_BillAdjHead", con);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Account_Description(string TransactionType, string Desc, string MND, string SetorID, string VendorCode)
        {
            try
            {
                List<Account_MD> dt = new Item_BLL().Account_Description(TransactionType, Desc, MND, SetorID, VendorCode);
                cr.Data = dt;
                cr.Message = "success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {

                cr.Data = "fail";
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Load_AccountInformation(string BillID, string VendorCode)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.PROC_Acc_Bill_Post_Tmp", con);
            cmd.Parameters.AddWithValue("@BillId", BillID);
            cmd.Parameters.AddWithValue("@VendorCode", VendorCode);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr, JsonRequestBehavior.AllowGet);
        }

        #region
        public static class CommonMethod
        {
            public static List<T> ConvertToList<T>(DataTable dt)
            {
                var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
                var properties = typeof(T).GetProperties();
                return dt.AsEnumerable().Select(row => {
                    var objT = Activator.CreateInstance<T>();
                    foreach (var pro in properties)
                    {
                        if (columnNames.Contains(pro.Name.ToLower()))
                        {
                            try
                            {
                                pro.SetValue(objT, row[pro.Name]);
                            }
                            catch (Exception ex) { }
                        }
                    }
                    return objT;
                }).ToList();
            }
        }
        #endregion
    }
}