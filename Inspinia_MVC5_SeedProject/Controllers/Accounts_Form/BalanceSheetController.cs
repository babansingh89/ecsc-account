﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models.Account;
using System.Data;
using ClosedXML.Excel;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace wbEcsc.Controllers.Accounts_Form
{
    [SecuredFilter]
    public class BalanceSheetController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public BalanceSheetController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            return View("~/Views/Accounts/BalanceSheet.cshtml");
        }

        [HttpPost]
        public ActionResult Show_Details(string FromDate, string ToDate, string SectorID)
        {
            try
            {
                DataSet lstTrail = new BalanceSheet_BLL().Show_Details(FromDate, ToDate, SectorID);
                cr.Data = lstTrail;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "found");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult Excel(string ReportName)
        {
            try
            {
                if (ReportName != null)
                {
                    JObject obj = JObject.Parse(ReportName);
                    JArray master = (JArray)obj.SelectToken("Master");

                    string constr = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(constr))
                    {
                        SqlCommand cmd = new SqlCommand("account.PROC_ACC_BALANCE_SHEET_Excel", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@p_FROM_DATE", master[0]["FromDate"].ToString());
                        cmd.Parameters.AddWithValue("@p_TO_DATE", master[0]["ToDate"].ToString());
                        cmd.Parameters.AddWithValue("@p_SECTORIDS", master[0]["SectorID"].ToString());

                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            using (DataSet ds = new DataSet())
                            {
                                con.Open();
                                sda.Fill(ds);

                                //Set Name of DataTables.
                                ds.Tables[0].TableName = "Liability";
                                ds.Tables[1].TableName = "Assets";

                                using (XLWorkbook wb = new XLWorkbook())
                                {
                                    foreach (DataTable dt in ds.Tables)
                                    {
                                        //Add DataTable as Worksheet.
                                        wb.Worksheets.Add(dt);
                                    }

                                    //Export the Excel file.
                                    Response.Clear();
                                    Response.Buffer = true;
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                    Response.AddHeader("content-disposition", "attachment; filename= BalanceSheet" + "_" + master[0]["FromDate"].ToString() + "-" + master[0]["ToDate"].ToString() + ".xlsx");
                                    using (MemoryStream MyMemoryStream = new MemoryStream())
                                    {
                                        wb.SaveAs(MyMemoryStream);
                                        MyMemoryStream.WriteTo(Response.OutputStream);
                                        Response.Flush();
                                        Response.End();
                                    }
                                }
                            }
                        }

                    }

                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}