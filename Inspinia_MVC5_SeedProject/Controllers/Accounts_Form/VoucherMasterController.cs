﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels.Accounts;
using wbEcsc.App_Codes.BLL.Account;
using Newtonsoft.Json;
using System.Data;
using System.Configuration;
using CrystalDecisions.CrystalReports.Engine;
using System.Data.SqlClient;
using System.IO;

namespace wbEcsc.Controllers.Accounts_Form
{
    [SecuredFilter]
    public class VoucherMasterController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        CrystalDecisions.CrystalReports.Engine.ReportDocument crystalReport = new ReportDocument();

        SessionData sData;
        public VoucherMasterController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index(string id)
        {
            VoucherInfo voucherInfo = new VoucherInfo();
            VoucherMaster vchMast = new VoucherMaster();
            List<VoucherDetail> vchDet = new List<VoucherDetail>();
            List<VoucherInstType> vchInstType = new List<VoucherInstType>();
            List<VoucherSubLedgerType> vchSL = new List<VoucherSubLedgerType>();


            voucherInfo.SessionExpired = "N";
            voucherInfo.EntryType = "I";
            vchMast.SectorID = Convert.ToInt32(sData.CurrSector);
            voucherInfo.VoucherMast = vchMast;
            voucherInfo.VoucherDetail = vchDet.ToArray();
            voucherInfo.tmpVchInstType = vchInstType.ToArray();
            voucherInfo.tmpVchSubLedger = vchSL.ToArray();


            ViewBag.PopulateJSON = JsonConvert.SerializeObject(voucherInfo);
            ViewBag.hdnGlobal_VoucherID = id;


            return View("~/Views/Accounts/Voucher_Master.cshtml");
        }

        [HttpPost]
        public ActionResult Search_AccountDetails(string SearchValue, string SearchType, string VoucherTypeID, string SectorID, string DRCR)
        {
            try
            {
                List<VoucherMaster_VM> lstVoucherDetails = new VoucherMaster_BLL().Search_AccountDetails(SearchValue, SearchType, VoucherTypeID, SectorID, DRCR);
                cr.Data = lstVoucherDetails;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", lstVoucherDetails.Count);
            }
            catch(Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Search_VoucherNo(string VoucherNo, string VoucherTypeID, string VoucherDate)
        {
            try
            {
                List<VoucherMaster_VM> lstVoucherDetails = new VoucherMaster_BLL().Search_VoucherNo(VoucherNo, VoucherTypeID, VoucherDate);
                cr.Data = lstVoucherDetails;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", lstVoucherDetails.Count);
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Load_VoucherNo(string VoucherNo, string FromDate, string ToDate, string SectorID)
        {
            try
            {
                List<VoucherMaster_VM> lstVoucherDetails = new VoucherMaster_BLL().Load_VoucherNo(VoucherNo, FromDate, ToDate, SectorID, sData.UserID);
                cr.Data = lstVoucherDetails;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", lstVoucherDetails.Count);
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Get_VoucherDetails(string VoucherTypeID, string VoucherDate, string VoucherNo, string AccFinYear, string SectorID)
        {
            try
            {
                var result = new VoucherMaster_BLL().Get_VoucherDetails(VoucherTypeID, VoucherDate, VoucherNo, sData.UserID, AccFinYear, SectorID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult SaveVoucher(string VoucherForm)
        {
            try
            {
                var result = new VoucherMaster_BLL().SaveVoucher(VoucherForm, sData.UserID);
                cr.Data = result;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult Generate_Report(string SectorID, string VoucherNo)
        {
            string reportName = ""; string fileName = "";

            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            try
            {

                reportName = "VoucherCash.rpt";
                fileName = "VoucherReport";

                crystalReport.Load(Server.MapPath("~/Reports/" + reportName));

                crystalReport.Refresh();

                string server = Connection_Details.ServerName();
                string database = Connection_Details.DatabaseName();
                string userid = Connection_Details.UserID();
                string password = Connection_Details.Password();



                crystalReport.DataSourceConnections[0].SetConnection(server, database, userid, password);
                crystalReport.DataSourceConnections[0].IntegratedSecurity = false;
                crystalReport.SetDatabaseLogon(server, database, userid, password);


                for (int i = 0; i < crystalReport.Subreports.Count; i++)
                {
                    crystalReport.Subreports[i].DataSourceConnections[0].SetConnection(server, database, userid, password);
                    crystalReport.Subreports[i].DataSourceConnections[0].IntegratedSecurity = false;
                    crystalReport.Subreports[i].SetDatabaseLogon(server, database, userid, password);
                }

             



                crystalReport.VerifyDatabase();
crystalReport.SetParameterValue("@VoucherNumber", Convert.ToInt32(VoucherNo));
                crystalReport.SetParameterValue("@sectorid", Convert.ToInt32(SectorID));



                Stream stream = crystalReport.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", fileName + ".pdf");

            }
            catch (Exception ex)
            {
                transaction.Rollback();
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
                return Json(cr.Message);
                //return View("~/Views/Error/Error404/Index.cshtml");
            }
            finally
            {
                dbConn.Close();
            }
        }

        //[HttpPost]
        //public ActionResult Generate_Report(string FromDate, string ToDate, string VoucherNo, string FormName, string ReportName, string ReportType)
        //{
        //    try
        //    {
        //        System.Web.HttpContext.Current.Session["FormName"] = FormName;
        //        System.Web.HttpContext.Current.Session["ReportName"] = ReportName;
        //        System.Web.HttpContext.Current.Session["ReportType"] = ReportType;

        //        var result = new VoucherMaster_BLL().Generate_Report(FromDate, ToDate, VoucherNo, FormName, ReportName, ReportType, sData.UserID);
        //        cr.Data = result;
        //        cr.Status = ResponseStatus.SUCCESS;
        //        cr.Message = string.Format("{0}", "");
        //    }
        //    catch (Exception ex)
        //    {
        //        cr.Data = null;
        //        cr.Message = ex.Message;
        //        cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
        //    }
        //    return Json(cr);
        //}

        [HttpPost]
        public JsonResult Auto_SubLedger_Details(string AccountCode, string Desc, string FinYear, string SectorID, string VchTypeID)
        {
            try
            {
                DataTable dt = new AccountVoucherOpening().Auto_Ledger_Details(AccountCode, Desc, FinYear, SectorID, VchTypeID);
                cr.Data = dt;
                cr.Message = "Success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult AdjustmentNumber(string SubCode)
        {
            try
            {
                DataTable dt = new VoucherMaster_BLL().Get_AdjustmentNumber(SubCode);
                cr.Data = dt;
                cr.Message = "Success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult AccDept_AccCostCenter(string Desc, string SectorID, string AccDept_CostCenter)
        {
            try
            {
                List<VoucherMaster_VM> lst = new VoucherMaster_BLL().Get_AccountDeptCostCenter(Desc, SectorID, AccDept_CostCenter);
                cr.Data = lst;
                cr.Message = "Success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_DeptCodeCostCenterCode(string VchDetailID, string VoucherNo, string AccCode, string Condition, string DeptCode, string CostCenterCode)
        {
            try
            {
                DataTable dt = new VoucherMaster_BLL().Get_Voucher_DeptCodeCostCenterCode(VchDetailID, VoucherNo, AccCode, Condition, DeptCode, CostCenterCode, sData.UserID);
                cr.Data = dt;
                cr.Message = "Success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Get_InstDeptCodeCostCenterCode(string BillID, string VoucherNo, string AccCode, string Condition, string DeptCode, string CostCenterCode)
        {
            try
            {
                DataTable dt = new VoucherMaster_BLL().Get_InstDeptCodeCostCenterCode(BillID, VoucherNo, AccCode, Condition, DeptCode, CostCenterCode, sData.UserID);
                cr.Data = dt;
                cr.Message = "Success";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = ex;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }
		
		        [HttpPost]
        public JsonResult AccountOpeningDate_MenuWise(string MenuID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.AccountOpeningDate_MenuWise", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MenuID", MenuID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                string Acc_OpenDate = dt.Rows[0]["Acc_OpenDate"].ToString();

                cr.Data = Acc_OpenDate;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }
    }
}