﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models.Account;
using System.Data;
using wbEcsc.App_Codes.BLL;
using System.IO;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace wbEcsc.Controllers.Accounts_Form
{
    [SecuredFilter]
    public class TrailBalanceController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public TrailBalanceController()
        {
            sData = SessionContext.SessionData;
        }

        public ActionResult Index()
        {
            Logo lObj = new Logo();
            Logo_MD modelObj = lObj.GetAppsInfo();
            ViewBag.logo = modelObj.LogoPath;
            ViewBag.header = modelObj.OfficeName;
            ViewBag.address = modelObj.Address;

            return View("~/Views/Accounts/TrailBalance.cshtml");
        }

        [HttpPost]
        public ActionResult Show_Details(string FromDate, string ToDate, string SectorID, string GLSL, string ParentAccountCode)
        {
            try
            {
                DataTable lstTrail = new TrailBalance_BLL().Show_Details(FromDate, ToDate, SectorID, GLSL, ParentAccountCode);
                cr.Data = lstTrail;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Show_Exp_Main_1(string AccountCode)
        {
            try
            {
                DataTable lstTrail = new TrailBalance_BLL().Show_Exp_Main_1(AccountCode);
                cr.Data = lstTrail;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Show_Exp_Main_2(string AccountCode, string StartDate, string EndDate, string SectorID)
        {
            try
            {
                DataTable lstTrail = new TrailBalance_BLL().Show_Exp_Main_2(AccountCode, StartDate, EndDate, SectorID);
                cr.Data = lstTrail;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Show_Exp_Main_3(string AccountCode, string VoucherStartDate, string VoucherEndDate)
        {
            try
            {
                DataTable lstTrail = new TrailBalance_BLL().Show_Exp_Main_3(AccountCode, VoucherStartDate, VoucherEndDate);
                cr.Data = lstTrail;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

		 [HttpGet]
        public ActionResult Excel(string ReportName)
        {
            try
            {
                if (ReportName != null)
                {
                    JObject obj = JObject.Parse(ReportName);
                    JArray master = (JArray)obj.SelectToken("Master");

                    DataTable dt = new TrailBalance_BLL().Show_Details(master[0]["FromDate"].ToString(), master[0]["ToDate"].ToString(),
                        master[0]["SectorID"].ToString(), master[0]["GLSL"].ToString(), master[0]["ParentAccountCode"].ToString());


                    GridView gv = new GridView();
                    gv.DataSource = dt;
                    gv.DataBind();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment; filename= TrailBalance" + "_" + master[0]["FromDate"].ToString() + "-" + master[0]["ToDate"].ToString() + ".xls");
                    Response.ContentType = "application/ms-excel";
                    Response.Charset = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    gv.RenderControl(htw);
                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                    
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}