﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace wbEcsc.Controllers.Accounts
{
    [SecuredFilter]
    public class VoucherApprovalController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        VoucherApproval_BLL vapp = new VoucherApproval_BLL();
        SessionData sData;
        public VoucherApprovalController()
        {
            sData = SessionContext.SessionData;
        }
        // GET: VoucherApproval
        public ActionResult Index()
        {
            return View("~/Views/Accounts/voucherApproval.cshtml");
        }

        [HttpPost]
        public JsonResult getSessionData() {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;
                var finyear = sData.CurFinYear;

                cr.Data = sData.UserName;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAllVoucherType() {
            try
            {
                int? SectorID = sData.CurrSector;
                long InsertedBy = sData.UserID;
                var finyear = sData.CurFinYear;

                var lst = vapp.getAllVoucherType();
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;

            }
            return Json(cr);

        }

        [HttpPost]
        public JsonResult GetAllVoucherApproval(string VCH_NO, int VCH_TYPE,string fromDate, string toDate) {
            try
            {
                int? SecID = sData.CurrSector;
                long InsertedBy = sData.UserID;
                var finyear = sData.CurFinYear;

                var lst = vapp.getAllVoucherApproval(VCH_NO, VCH_TYPE, finyear, SecID, InsertedBy, fromDate, toDate);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GetAllVoucherApprovalDetails(string VCH_NO)
        {
            try
            {
                int? SecID = sData.CurrSector;
                long InsertedBy = sData.UserID;
                var finyear = sData.CurFinYear;

                var lst = vapp.getAllVoucherApprovalDetails(VCH_NO, finyear, SecID, InsertedBy);
                cr.Data = lst;
                cr.Message = "";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult UpdateVoucherApproval(string VCH_NO) {
            try
            {
                int? SecID = sData.CurrSector;
                long InsertedBy = sData.UserID;
                var finyear = sData.CurFinYear;

                vapp.updateVoucherApproval(VCH_NO, SecID, InsertedBy);
                cr.Data = "";
                cr.Message = "Update Successfully";
                cr.Status = ResponseStatus.SUCCESS;
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }





    }
}