﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.App_Start;
using wbEcsc.Models;
using wbEcsc.Models.Application;
using wbEcsc.App_Codes.BLL.Account;
using wbEcsc.Models.Account;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Configuration;

namespace wbEcsc.Controllers.Accounts_Form
{
    [SecuredFilter]
    public class Profit_LossController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public Profit_LossController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
           ViewBag.AccFinYear=  sData.CurFinYear;
            return View("~/Views/Accounts/Profit_Loss.cshtml");
        }

        [HttpPost]
        public ActionResult Show_Details(string FromDate, string ToDate, string SectorID)
        {
            try
            {
                DataSet  lstTrail = new Profit_Loss_BLL().Show_Details(FromDate, ToDate, SectorID);
                cr.Data = lstTrail;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "found");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpPost]
        public ActionResult Show_Group(string AccCode, string FromDate, string ToDate, string SectorID)
        {
            try
            {
                DataSet lstTrail = new Profit_Loss_BLL().Show_Group(AccCode, FromDate, ToDate, SectorID);
                cr.Data = lstTrail;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0}", "found");
            }
            catch (Exception ex)
            {
                cr.Data = null;
                cr.Message = ex.Message;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult Excel(string ReportName)
        {
            try
            {
                if (ReportName != null)
                {
                    JObject obj = JObject.Parse(ReportName);
                    JArray master = (JArray)obj.SelectToken("Master");

                    string constr = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(constr))
                    {
                        SqlCommand cmd = new SqlCommand("account.PROC_ACC_PROFIT_LOSS_Excel", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@p_FROM_DATE", master[0]["FromDate"].ToString());
                        cmd.Parameters.AddWithValue("@p_TO_DATE", master[0]["ToDate"].ToString());
                        cmd.Parameters.AddWithValue("@p_SECTORIDS", master[0]["SectorID"].ToString());
                        cmd.Parameters.AddWithValue("@p_PL", "Y");          //DEFAULT FOR PROFIT AND LOSS

                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            using (DataSet ds = new DataSet())
                            {
                                con.Open();
                                sda.Fill(ds);

                                //Set Name of DataTables.
                                ds.Tables[0].TableName = "Direct-Exp";
                                ds.Tables[1].TableName = "Direct-Inc";
                                ds.Tables[2].TableName = "Indirect-Exp";
                                ds.Tables[3].TableName = "Indirect-Inc";

                                using (XLWorkbook wb = new XLWorkbook())
                                {
                                    foreach (DataTable dt in ds.Tables)
                                    {
                                        //Add DataTable as Worksheet.
                                        wb.Worksheets.Add(dt);
                                    }

                                    //Export the Excel file.
                                    Response.Clear();
                                    Response.Buffer = true;
                                    Response.Charset = "";
                                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                                    Response.AddHeader("content-disposition", "attachment; filename= Profit-Loss" + "_" + master[0]["FromDate"].ToString() + "-" + master[0]["ToDate"].ToString() + ".xlsx");
                                    using (MemoryStream MyMemoryStream = new MemoryStream())
                                    {
                                        wb.SaveAs(MyMemoryStream);
                                        MyMemoryStream.WriteTo(Response.OutputStream);
                                        Response.Flush();
                                        Response.End();
                                    }
                                }
                            }
                        }

                    }

                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }

    }
}