﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;
using NPOI.HSSF.UserModel;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class BankStatementController : Controller
    {
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;

        public BankStatementController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/BankStatement.cshtml");
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public JsonResult BindVendor(string TransType, string AccountCode, string FromDate, string To, string ChequeNo, string Desc)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_BankStatement", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@AccountCode", AccountCode == "" ? DBNull.Value : (object)AccountCode);
            cmd.Parameters.AddWithValue("@VoucherDate", FromDate == "" ? DBNull.Value : (object)FromDate);
            cmd.Parameters.AddWithValue("@MemoDate", To == "" ? DBNull.Value : (object)To);
            cmd.Parameters.AddWithValue("@ChequeNo", ChequeNo == "" ? DBNull.Value : (object)ChequeNo);
            cmd.Parameters.AddWithValue("@Desc", Desc == "" ? DBNull.Value : (object)Desc);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.CommandTimeout = 0;
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult CloseMemo(string TransType, string StatementID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_BankStatement", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            cmd.Parameters.AddWithValue("@StatementID", StatementID);
            cmd.Parameters.AddWithValue("@UserID", sData.UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Auto_Bank(string TransactionType, string Desc, string MND, string SetorID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_AccountDescByGroupLedger", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@AccountDesc", Desc);
            cmd.Parameters.AddWithValue("@MND", MND);
            cmd.Parameters.AddWithValue("@SectorID", SetorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult GeneratedAdvice(string TransType)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Get_BankStatement", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransType);
            //cmd.Parameters.AddWithValue("@AccountCode", AccountCode);
            //cmd.Parameters.AddWithValue("@Desc", Desc);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);

                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Data Found", dt.Rows.Count);
            }
            catch (Exception ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Insert(string BankAccountCode, string Remarks, List<BankStatement_MD> EffectedGL, string SectorID)
        {
            int StatementID = 0; string RMemoNo="";
            SqlConnection dbConn = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            SqlDataReader dr;
            try
            {
                try {
                    cmd = new SqlCommand("account.Get_BankStatement", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", "Insert");
                    cmd.Parameters.AddWithValue("@BankAccCode", BankAccountCode);
                    cmd.Parameters.AddWithValue("@Remarks", Remarks);
                    cmd.Parameters.AddWithValue("@SectorID", SectorID);
                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            StatementID = Convert.ToInt32(dr["StatementID"].ToString());
                            RMemoNo = dr["MemoNo"].ToString();
                        }
                    }
                    dr.Close();

                    if (StatementID > 0)
                    {
                        if (EffectedGL != null)
                        {
                            if (EffectedGL.Count > 0)
                            {
                                int i = 0;
                                foreach (var list in EffectedGL)
                                {
                                    i = i + 1;
                                    cmd = new SqlCommand("account.Get_BankStatement", dbConn, transaction);
                                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                                    cmd.Parameters.AddWithValue("@TransactionType", "InsertDetail");
                                    cmd.Parameters.AddWithValue("@StatementID", StatementID);
                                    cmd.Parameters.AddWithValue("@SerialNo", i);
                                    cmd.Parameters.AddWithValue("@AccountCode", list.AccountCode);
                                    cmd.Parameters.AddWithValue("@VendorName", list.VendorName);
                                    cmd.Parameters.AddWithValue("@VoucherNo", list.VoucherNo);
                                    cmd.Parameters.AddWithValue("@VoucherDate", list.VoucherDate);
                                    cmd.Parameters.AddWithValue("@Amount", list.Amount);
                                    cmd.Parameters.AddWithValue("@IFSC", list.IFSC);
                                    cmd.Parameters.AddWithValue("@BankName", list.BankName);
                                    cmd.Parameters.AddWithValue("@BranchName", list.BranchName);
                                    cmd.Parameters.AddWithValue("@VoucherDetailID", list.VoucherDetailID);
                                    cmd.Parameters.AddWithValue("@InstrumentNumber", list.InstrumentNumber);
                                    cmd.Parameters.AddWithValue("@InstrumentType", list.InstrumentType);
                                    cmd.Parameters.AddWithValue("@UserID", sData.UserID);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }

                    transaction.Commit();
                    cr.Data = RMemoNo;
                    cr.Status = ResponseStatus.SUCCESS;
                    cr.Message = string.Format("{0} Data Found", "success");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    cr.Data = false;
                    cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                    cr.Message = ex.Message;
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                dbConn.Close();
            }
            return Json(cr);
        }

        [HttpGet]
        public ActionResult Excel(string ReportName)
        {
            try
            {
                JObject obj = JObject.Parse(ReportName);
                JArray master = (JArray)obj.SelectToken("Master");

                decimal Total = 0; string Position = ""; string SumFieldName = "";string BankName = master[0]["BankNm"].ToString();
                if(BankName == "")
                {
                    BankName = "Others";
                }
                string fileName = "Bank Report" + "_" + BankName + "_" + master[0]["MemoNo"].ToString() + ".xls";

                if (ReportName != null)
                {
                    SqlConnection con = new SqlConnection(conString);
                    SqlCommand cmd = new SqlCommand("account.Get_BankStatement", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TransactionType", "Excel");
                    cmd.Parameters.AddWithValue("@StatementID", master[0]["StatementID"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();

                    con.Open();
                    cmd.CommandTimeout = 0;
                    da.Fill(dt);

                    if (dt.Rows.Count > 0)
                    {
                        var workbook = new HSSFWorkbook();
                        var sheet = workbook.CreateSheet();

                        //Create a header row
                        var headerRow = sheet.CreateRow(0);

                        string[] columnNames = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                        for (int j = 0; j < columnNames.Length; j++)
                        {
                            headerRow.CreateCell(j).SetCellValue(columnNames[j]);
                        }
                        int rowNumber = 1;
                        var row = sheet.CreateRow(rowNumber);
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            //Create a new Row
                            row = sheet.CreateRow(rowNumber++);

                            string[] columnNamess = dt.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                            for (int j = 0; j < columnNamess.Length; j++)
                            {
                                row.CreateCell(j).SetCellValue(dt.Rows[i][columnNamess[j]].ToString());
                            }

                            if (SumFieldName != "")
                            {
                                DataColumnCollection columns = dt.Columns;
                                if (columns.Contains(SumFieldName))
                                {
                                    string value = dt.Rows[i][SumFieldName].ToString();
                                    Total = Total + Convert.ToDecimal(value == "" ? "0" : value);

                                    Position = dt.Columns.IndexOf(SumFieldName).ToString();
                                }
                                else
                                {
                                    Total = Total + 0;
                                }
                            }
                        }

                        if (SumFieldName != "")
                        {
                            // Add a row for the detail row
                            row = sheet.CreateRow(dt.Rows.Count + 1);
                            var cell = row.CreateCell(Convert.ToInt32(Position) - 1);
                            cell.SetCellValue("Total:");

                            string FinalAmt = Total.ToString();
                            row.CreateCell(Convert.ToInt32(Position)).SetCellValue(FinalAmt);
                        }
                        using (var exportData = new MemoryStream())
                        {
                            workbook.Write(exportData);
                            string saveAsFileName = fileName;
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
                            Response.Clear();
                            Response.BinaryWrite(exportData.GetBuffer());
                            Response.End();
                        }
                    }

                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }

    public class Api
    {
        public string BANK { get; set; }
    }
}