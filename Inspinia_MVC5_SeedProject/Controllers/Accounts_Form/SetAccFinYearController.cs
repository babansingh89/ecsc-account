﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.Models;

namespace Inspinia_MVC5_SeedProject.Controllers.Accounts_Form
{
    public class SetAccFinYearController : Controller
    {
        ClientJsonResult cr = new ClientJsonResult();
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;

        public ActionResult Index()
        {
            return View("~/Views/Accounts/SetAccFinYear.cshtml");
        }

        [HttpPost]
        public JsonResult Bind_Form()
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.SetAcc_FinYear", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                da.Fill(dt);
                cr.Data = dt;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", dt.Rows.Count);
            }
            catch (SqlException ex)
            {
                cr.Data = false;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }

        [HttpPost]
        public JsonResult Save(string accDate, string menuID)
        {
            SqlConnection con = new SqlConnection(conString);
            SqlCommand cmd = new SqlCommand("account.Update_Acc_FinYear", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ACCDATE", accDate);
            cmd.Parameters.AddWithValue("@MenuID", menuID);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataTable dt = new DataTable();
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                cr.Data = 1;
                cr.Status = ResponseStatus.SUCCESS;
                cr.Message = string.Format("{0} Found", "success");
            }
            catch (SqlException ex)
            {
                cr.Data = 0;
                cr.Status = ResponseStatus.UNKNOWN_SERVER_ERROR;
                cr.Message = ex.Message;
            }
            finally
            {
                con.Close();
            }
            return Json(cr);
        }
    }
}