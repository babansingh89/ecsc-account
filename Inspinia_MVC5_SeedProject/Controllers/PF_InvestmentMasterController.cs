﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wbEcsc.App_Codes;
using wbEcsc.Models;
using wbEcsc.Models.Application;

namespace Inspinia_MVC5_SeedProject.Controllers
{
    public class PF_InvestmentMasterController : Controller
    {
        // GET: PF_InvestmentMaster
        string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
        ClientJsonResult cr = new ClientJsonResult();
        SessionData sData;
        public PF_InvestmentMasterController()
        {
            sData = SessionContext.SessionData;
        }
        public ActionResult Index()
        {
            return View("~/Views/Accounts/PF_InvestmentMaster.cshtml");
        }
    }
}