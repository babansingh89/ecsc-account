﻿using System.Collections.Generic;
using System.Web;

namespace wbEcsc.App_Codes
{
    public class GlobalContext
    {
        private static Dictionary<string, object> _loggedInUsers;
        static GlobalContext()
        {
            _loggedInUsers = HttpContext.Current.Application[KeysLibrary.GlobalKeys.LoggedInUsers_Key] as Dictionary<string, object>;
            if (_loggedInUsers == null)
            {
                _loggedInUsers = new Dictionary<string, object>();
                HttpContext.Current.Application[KeysLibrary.GlobalKeys.LoggedInUsers_Key] = _loggedInUsers;
            }
        }
        public static Dictionary<string, object> LoggedInUsers
        {
            get
            {
                return _loggedInUsers;

            }
           private set {
                _loggedInUsers = value;
            }
        }
    }
}