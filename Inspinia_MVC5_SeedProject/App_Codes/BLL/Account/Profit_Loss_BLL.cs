﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class Profit_Loss_BLL:BllBase
    {
        public DataSet Show_Details(string FromDate, string ToDate, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.PROC_ACC_PROFIT_LOSS", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_FROM_DATE", FromDate);
            cmd.Parameters.AddWithValue("@p_TO_DATE", ToDate);
            cmd.Parameters.AddWithValue("@p_SECTORIDS", SectorID);
            cmd.Parameters.AddWithValue("@p_PL", "Y");          //DEFAULT FOR PROFIT AND LOSS
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
 cmd.CommandTimeout = 0;
                da.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataSet Show_Group(string AccCode, string FromDate, string ToDate, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.PROC_ACC_GROUP", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_ACCCODE", AccCode);
            cmd.Parameters.AddWithValue("@p_FROM_DATE", FromDate);
            cmd.Parameters.AddWithValue("@p_TO_DATE", ToDate);
            cmd.Parameters.AddWithValue("@p_SECTORIDS", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            try
            {
                cn.Open();
 cmd.CommandTimeout = 0;
                da.Fill(ds);
                return ds;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
    }
}