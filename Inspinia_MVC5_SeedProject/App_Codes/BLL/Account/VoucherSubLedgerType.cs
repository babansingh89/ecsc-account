﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for VoucherFixedAsset
/// </summary>
public class VoucherSubLedgerType
{
    public int? BillID { get; set; }
    public int? SLSlNo { get; set; }
    public string SubLedgerTypeID { get; set; }
    public string SubLedgerType { get; set; }
    public string Number { get; set; }
    public decimal? Qty { get; set; }
    public decimal? IRate { get; set; }
    public string Amount { get; set; }
    public string DRCR { get; set; }
    public string AccountCODE { get; set; }
    public string SubLedgerCODE { get; set; }
    public string SubLedger { get; set; }
    public string IsDelete { get; set; }

    public string AccDeptCode { get; set; }
    public string AccCostCenterCode { get; set; }
    public string AccDept { get; set; }
    public string AccCostCenter { get; set; }

    public VoucherSubLedgerType()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}