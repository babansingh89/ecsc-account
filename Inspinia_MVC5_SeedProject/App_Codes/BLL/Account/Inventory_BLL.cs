﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class Inventory_BLL: BllBase
    {
        public object GetAuto(string AccountDescription, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.AccountHeaderAuto", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountDescription", (object)AccountDescription ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                var lst = dt.AsEnumerable().Where(r => r.Field<string>("GroupLedger") == "L").Select(t => new
                {
                    AccountDescription = t.Field<string>("AccountDescription"),
                    AccountCode = t.Field<string>("AccountCode"),
                }).Take(10).ToList();
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public void InsUpdInventorydetails(InventoryDetails InvDetails) {
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[account].[Sp_InsUpdInventorydetails]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@Id", InvDetails.Id);
                cmd.Parameters.AddWithValue("@stockdate", InvDetails.InvDate);
                cmd.Parameters.AddWithValue("@AccountCode", InvDetails.AccountCode);
                cmd.Parameters.AddWithValue("@Amount", InvDetails.Amount);
                cmd.Parameters.AddWithValue("@SectorID", InvDetails.SectorID);
                //cmd.Parameters.AddWithValue("@ScoreDesc", !string.IsNullOrEmpty(assmtBeltSheetInsUp.ToRange) ? assmtBeltSheetInsUp.ToRange : (object)DBNull.Value);
                cmd.Parameters.AddWithValue("@InsertedBy", InvDetails.InsertedBy);

                //comd.Parameters.AddWithValue("@FirstName", obj.FirstName);
                //comd.Parameters.AddWithValue("@LastName", obj.LastName);
                //comd.Parameters.AddWithValue("@Email", obj.Email ?? DBNull.Value as object);
                //comd.Parameters.AddWithValue("@PhoneNo", obj.PhoneNo);
                //comd.Parameters.AddWithValue("@IsAccountLocked", obj.IsAccountLocked);
                //comd.Parameters.AddWithValue("@IsAccountVerified", obj.IsAccountVerified);
                //comd.Parameters.AddWithValue("@UserType", obj.UserType);
                //comd.Parameters.AddWithValue("@MunicipalityID", obj.MunicipalityID);
                //comd.Parameters.AddWithValue("@UserName", obj.UserName);
                //comd.Parameters.AddWithValue("@Password", Password);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch (Exception ex)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<InventoryDetails> GetAllInventoryDetails(string stockdate)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "[account].[sp_getInventoryDetails]";
            cmd.Parameters.AddWithValue("@stockdate",stockdate);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                var lst = dt.AsEnumerable().Select(t => new InventoryDetails
                {
                    Id = t.Field<int>("Id"),
                    InvDate = t.Field<string>("InvDate"),
                    AccountDescription = t.Field<string>("AccountDescription"),
                    AccountCode = t.Field<string>("AccountCode"),
                    Amount = t.Field<decimal>("Amount"),
                    SectorID = t.Field<int?>("sectorid")
                }).ToList();
                return lst;
            }
            catch (SqlException)
            {
                throw new Exception("Database error");
            }
            finally
            {
                cn.Close();
            }
        }

        public void DeleteInvDetailsById(int Id) {
            SqlTransaction Transaction = null;
            try
            {
                var cmd = new SqlCommand();
                cn.Open();
                Transaction = cn.BeginTransaction();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "[account].[Sp_DeleteInvDetailsById]";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@Id",Id);
                cmd.Transaction = Transaction;
                cmd.ExecuteNonQuery();
                Transaction.Commit();
            }
            catch (Exception ex)
            {
                Transaction.Rollback();
                throw;
            }
            finally
            {
                cn.Close();
            }
        }
    }
}