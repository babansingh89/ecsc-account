﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;
using wbEcsc.ViewModels.Accounts;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class VoucherMaster_BLL:BllBase
    {
        public List<VoucherMaster_VM> Get_Acc_VchType()
        {
            SqlCommand cmd = new SqlCommand("account.Get_Acc_VchType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_Get_Acc_VchType(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<VoucherMaster_VM> Search_AccountDetails(string SearchValue, string SearchType, string VoucherTypeID, string SectorID, string DRCR)
        {
            SqlCommand cmd = new SqlCommand("account.Get_Acc_VchGLSL", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Type", SearchType);
            cmd.Parameters.AddWithValue("@Description", SearchValue);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@DRCR", DRCR);
            cmd.Parameters.AddWithValue("@VoucherTypeID", VoucherTypeID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_Search_AccountDetails(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<VoucherMaster_VM> Get_Instrument()
        {
            SqlCommand cmd = new SqlCommand("account.Get_Instrument", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_Get_Instrument(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<VoucherMaster_VM> Get_SubLedgerType()
        {
            SqlCommand cmd = new SqlCommand("account.Get_SubLedgerType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_Get_SubLedgerType(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Get_Voucher_DeptCodeCostCenterCode(string VchDetailID, string VoucherNo, string AccCode, string Condition, string DeptCode, string CostCenterCode, long UserID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_Voucher_DeptCodeCostCenterCode", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@VchDetailID", VchDetailID);
            cmd.Parameters.AddWithValue("@VoucherNo", VoucherNo);
            cmd.Parameters.AddWithValue("@Condition", Condition);
            cmd.Parameters.AddWithValue("@DeptCode", DeptCode);
            cmd.Parameters.AddWithValue("@CostCenterCode", CostCenterCode);
            cmd.Parameters.AddWithValue("@AccCode", AccCode);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Get_InstDeptCodeCostCenterCode(string BillID, string VoucherNo, string AccCode, string Condition, string DeptCode, string CostCenterCode, long UserID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_InstDeptCodeCostCenterCode", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BillID", BillID);
            cmd.Parameters.AddWithValue("@VoucherNo", VoucherNo);
            cmd.Parameters.AddWithValue("@Condition", Condition);
            cmd.Parameters.AddWithValue("@DeptCode", DeptCode);
            cmd.Parameters.AddWithValue("@CostCenterCode", CostCenterCode);
            cmd.Parameters.AddWithValue("@AccCode", AccCode);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<VoucherMaster_VM> Get_AccountDeptCostCenter(string Desc, string SectorID, string AccDept_CostCenter)
        {
            SqlCommand cmd = new SqlCommand("account.Get_AccountDepartment", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@AccDept_CostCenter", AccDept_CostCenter);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_Get_AccountDepartment(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable Get_AdjustmentNumber(string SubCode)
        {
            SqlCommand cmd = new SqlCommand("account.Get_AdjustmentNo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubCode", SubCode);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<VoucherMaster_VM> Search_VoucherNo(string VoucherNo, string VoucherTypeID, string VoucherDate)
        {
            SqlCommand cmd = new SqlCommand("account.Get_VoucherNo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@VoucherNo", VoucherNo);
            cmd.Parameters.AddWithValue("@VoucherTypeID", VoucherTypeID);
            cmd.Parameters.AddWithValue("@VoucherDate", VoucherDate);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_Search_VoucherNo(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public List<VoucherMaster_VM> Load_VoucherNo(string VoucherNo, string FromDate, string ToDate, string SectorID, long UserID)
        {
            SqlCommand cmd = new SqlCommand("account.Load_VoucherNo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RefVoucherSlNo", VoucherNo);
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@SecID", SectorID);
            cmd.Parameters.AddWithValue("@Userid", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert_Search_VoucherNo(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public VoucherInfo SaveVoucher(string VoucherForm, long UserID)
        {
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();
            string noNewLines = VoucherForm.Replace("\n", "");
            VoucherInfo voucherInfo = JsonConvert.DeserializeObject<VoucherInfo>(noNewLines);

            SqlDataReader dr;

            try
            {
                try
                {
                    VoucherMaster voucherMast = voucherInfo.VoucherMast;
                    VoucherDetail[] voucherDetail = voucherInfo.VoucherDetail;

                    if (UserID > 0)
                    {
                        if (voucherMast != null)
                        {
                            if (voucherInfo.EntryType == "I")
                            {
                                SaveVoucherMaster(voucherMast, UserID, dbConn, transaction);

                                if (voucherMast.VCHNo > 0)
                                {
                                    if (voucherDetail.Length > 0)
                                    {
                                        SaveVoucherDetail(voucherDetail, voucherMast, UserID, dbConn, transaction);
                                    }
                                }
                            }
                            else if (voucherInfo.EntryType == "E")
                            {
                                string Result = UpdateVoucherMast(voucherMast, UserID, dbConn, transaction);
                                if (Result == "Y")
                                {
                                    Update_VoucherDetail(voucherDetail, voucherMast, UserID, dbConn, transaction);

                                    voucherInfo.SearchResult = "U";
                                    voucherInfo.Message = "Voucher No " + voucherMast.VoucherNo + '(' + voucherMast.VCHNo + ')' + "" + " Updated Successfully !!";
 					voucherInfo.VoucherNo = voucherMast.VoucherNo;
                                    voucherInfo.VoucherID = voucherMast.VCHNo;
                                }
                                else if (Result == "N")
                                {
                                    voucherInfo.SearchResult = "F";
                                    voucherInfo.Message = "Voucher has been finalized cannot update the voucher.";
                                }
                                else if (Result == "R")
                                {
                                    voucherInfo.SearchResult = "R";
                                    voucherInfo.Message = "Error in Deleting Voucher";
                                }
                            }
                            transaction.Commit();
                            
                        }
                    }
                    else
                    {
                        voucherInfo.SessionExpired = "Y";
                    }
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return voucherInfo;
        }

        private static void SaveVoucherMaster(VoucherMaster voucherMast, long UserID, SqlConnection db, SqlTransaction transaction)
        {
            string TR_Type = "";
            SqlDataReader dr;
            try
            {
                if (voucherMast != null)
                {
                    SqlCommand cmd = new SqlCommand("account.Get_Acc_VchTypeByID", db, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@VoucherTypeID", voucherMast.VoucherType);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        TR_Type = dr["Abbv"].ToString();
                    }
                    dr.Close();

                    cmd = new SqlCommand("account.Insert_Acc_VoucherMaster", db, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AutoTrans", "N");
                    cmd.Parameters.AddWithValue("@VCH_TYPE", voucherMast.VoucherType);
                    cmd.Parameters.AddWithValue("@VOUCHER_DATE", voucherMast.VCHDATE);
                    cmd.Parameters.AddWithValue("@VCH_AMOUNT", voucherMast.VCHAMOUNT);
                    cmd.Parameters.AddWithValue("@NARRATION", voucherMast.NARRATION);
                    cmd.Parameters.AddWithValue("@STATUS", voucherMast.STATUS);
                    cmd.Parameters.AddWithValue("@NP_SP", voucherMast.FoundBank);
                    cmd.Parameters.AddWithValue("@TR_TYPE", TR_Type);
                    cmd.Parameters.AddWithValue("@SectorID", voucherMast.SectorID);
                    cmd.Parameters.AddWithValue("@InsertedBy", UserID);
                    cmd.Parameters.Add("@Vch_NoResult", SqlDbType.Int);
                    cmd.Parameters.Add("@VoucherNoResult", SqlDbType.VarChar, 25);
                    cmd.Parameters["@Vch_NoResult"].Direction = ParameterDirection.Output;
                    cmd.Parameters["@VoucherNoResult"].Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    voucherMast.VCHNo = Convert.ToInt32(cmd.Parameters["@Vch_NoResult"].Value.ToString());
                    voucherMast.VoucherNo = cmd.Parameters["@VoucherNoResult"].Value.ToString();

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static void SaveVoucherDetail(VoucherDetail[] voucherDetail, VoucherMaster voucherMast, long UserID, SqlConnection db, SqlTransaction transaction)
        {
            SqlDataReader dr;
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (voucherDetail.Length > 0)
                {
                    int i = 0;
                    foreach (VoucherDetail vchdet in voucherDetail)
                    {
                        i = i + 1;
                        cmd = new SqlCommand("account.Insert_Acc_VoucherDetail", db, transaction);

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@MaxVoucherNumber", voucherMast.VCHNo);
                        cmd.Parameters.AddWithValue("@SerialNumber", i);
                        cmd.Parameters.AddWithValue("@Accountcode", vchdet.GLID);
                        cmd.Parameters.AddWithValue("@DRCR", vchdet.DRCR);
                        cmd.Parameters.AddWithValue("@Amount", vchdet.AMOUNT);
                        cmd.Parameters.AddWithValue("@sectorID", vchdet.SectorID);
                        cmd.Parameters.AddWithValue("@insertedBy", UserID);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            vchdet.VchSrl = Convert.ToInt32(dt.Rows[0]["VCHSRL"].ToString());
                        }


                        if (vchdet.VchInstType.Length > 0)
                        {
                            foreach (VoucherInstType vchinst in vchdet.VchInstType)
                            {
                                cmd = new SqlCommand("account.Updt_AccDatBills", db, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.AddWithValue("@InFavourof", vchinst.Drawee == "" ? DBNull.Value : (object)vchinst.Drawee);
                                cmd.Parameters.AddWithValue("@InstrumentType", vchinst.InstType == "" ? DBNull.Value : (object)vchinst.InstType);
                                cmd.Parameters.AddWithValue("@InstrumentNo", vchinst.InstNo == "" ? DBNull.Value : (object)vchinst.InstNo);
                                cmd.Parameters.AddWithValue("@InstrumentDate", vchinst.InstDT == "" ? DBNull.Value : (object)vchinst.InstDT);
                                cmd.Parameters.AddWithValue("@insertedBy", UserID);
                                cmd.Parameters.AddWithValue("@sectorID", vchinst.SectorID);
                                cmd.Parameters.AddWithValue("@Accountcode", vchdet.GLID);
                                cmd.Parameters.AddWithValue("@reimbursementTypeID", 0);
                                cmd.Parameters.AddWithValue("@MaxVoucherNumber", voucherMast.VCHNo);
                                cmd.Parameters.AddWithValue("@AdjustmentAmount", vchinst.Amount);
                                cmd.Parameters.AddWithValue("@DRCR", vchinst.DRCR);
                                cmd.Parameters.AddWithValue("@VoucherSerial", vchdet.VchSrl);
                                cmd.Parameters.AddWithValue("@AccDeptCode", vchinst.AccDeptCode == "" ? DBNull.Value : (object)vchinst.AccDeptCode);
                                cmd.Parameters.AddWithValue("@AccCostCenterCode", vchinst.AccCostCenterCode == "" ? DBNull.Value : (object)vchinst.AccCostCenterCode);
                                cmd.ExecuteNonQuery();
                            }
                        }
                        if (vchdet.VchSubLedger.Length > 0)
                        {
                            foreach (VoucherSubLedgerType vsl in vchdet.VchSubLedger)
                            {
                                cmd = new SqlCommand("account.Insert_Acc_SubLedgerType", db, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.AddWithValue("@VCHNo", voucherMast.VCHNo);
                                cmd.Parameters.AddWithValue("@VCHSlNo", vchdet.VchSrl);   //vsl.SLSlNo
                                cmd.Parameters.AddWithValue("@AccountCode", vsl.AccountCODE);
                                cmd.Parameters.AddWithValue("@SubCode", vsl.SubLedgerCODE);
                                cmd.Parameters.AddWithValue("@ModeofAdj", vsl.SubLedgerTypeID);
                                cmd.Parameters.AddWithValue("@AdjNumber", vsl.Number);
                                cmd.Parameters.AddWithValue("@AdjDate", voucherMast.VCHDATE);
                                cmd.Parameters.AddWithValue("@AdjAmount", vsl.Amount);
                                cmd.Parameters.AddWithValue("@DRCR", vsl.DRCR);
                                cmd.Parameters.AddWithValue("@Qty", vsl.Qty == null ? DBNull.Value : (object)vsl.Qty);
                                cmd.Parameters.AddWithValue("@IRate", vsl.IRate == null ? DBNull.Value : (object)vsl.IRate);
                                cmd.Parameters.AddWithValue("@SectorID", vchdet.SectorID);
                                cmd.Parameters.AddWithValue("@InsertedBy", UserID);
                                SqlDataAdapter daSL = new SqlDataAdapter(cmd);
                                DataTable dtSL = new DataTable();
                                daSL.Fill(dtSL);

                                if (dtSL.Rows.Count > 0)
                                {
                                    vsl.BillID = Convert.ToInt32(dtSL.Rows[0]["BillID"].ToString());
                                }

                            }
                        }
                }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static string UpdateVoucherMast(VoucherMaster voucherMast, long UserID, SqlConnection db, SqlTransaction transaction)
        {
            string Result = "";
            SqlDataReader dr;
            try
            {
                if (voucherMast != null)
                {
                    SqlCommand cmd = new SqlCommand("account.Update_Acc_VoucherMaster", db, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@VCHNo", voucherMast.VCHNo);
                    cmd.Parameters.AddWithValue("@FIN_YEAR", voucherMast.YearMonth);
                    cmd.Parameters.AddWithValue("@VCH_TYPE", voucherMast.VoucherType);
                    cmd.Parameters.AddWithValue("@VCH_DATE", voucherMast.VCHDATE);
                    cmd.Parameters.AddWithValue("@VCH_AMOUNT", voucherMast.VCHAMOUNT);
                    cmd.Parameters.AddWithValue("@NARRATION", voucherMast.NARRATION);
                    cmd.Parameters.AddWithValue("@SectorID", voucherMast.SectorID);
                    cmd.Parameters.AddWithValue("@InsertedBy", UserID);

                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Result = dr["Result"].ToString();
                        }
                    }
                    else
                    {
                        Result = "R";
                    }
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return Result;
        }

        private static void Update_VoucherDetail(VoucherDetail[] voucherDetail, VoucherMaster voucherMast, long UserID, SqlConnection db, SqlTransaction transaction)
        {
            SqlDataReader dr;
            SqlCommand cmd = new SqlCommand();
            try
            {
                if (voucherDetail.Length > 0)
                {
                    int i = 0;
                    foreach (VoucherDetail vchdet in voucherDetail)
                    {
                        i = i + 1;
                        cmd = new SqlCommand("account.Update_Acc_VoucherDetail", db, transaction);

                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VoucherDetailID", vchdet.VoucherDetailID == null ? 0 : (object)vchdet.VoucherDetailID);
                        cmd.Parameters.AddWithValue("@MaxVoucherNumber", voucherMast.VCHNo);
                        cmd.Parameters.AddWithValue("@SerialNumber", vchdet.VchSrl);
                        cmd.Parameters.AddWithValue("@Accountcode", vchdet.GLID);
                        cmd.Parameters.AddWithValue("@DRCR", vchdet.DRCR);
                        cmd.Parameters.AddWithValue("@Amount", vchdet.AMOUNT);
                        cmd.Parameters.AddWithValue("@sectorID", vchdet.SectorID);
                        cmd.Parameters.AddWithValue("@IsDelete", vchdet.IsDelete == null ? DBNull.Value : (object)vchdet.IsDelete);
                        cmd.Parameters.AddWithValue("@insertedBy", UserID);

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            vchdet.VchSrl = Convert.ToInt32(dt.Rows[0]["VCHSRL"].ToString());
                        }


                        if (vchdet.VchInstType.Length > 0)
                        {
                            foreach (VoucherInstType vchinst in vchdet.VchInstType)
                            {
                                cmd = new SqlCommand("account.Update_AccDatInstBills", db, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.AddWithValue("@BillID", vchinst.BillID == null ? 0 : (object)vchinst.BillID);
                                cmd.Parameters.AddWithValue("@InFavourof", vchinst.Drawee == "" ? DBNull.Value : (object)vchinst.Drawee);
                                cmd.Parameters.AddWithValue("@InstrumentType", vchinst.InstType == "" ? DBNull.Value : (object)vchinst.InstType);
                                cmd.Parameters.AddWithValue("@InstrumentNo", vchinst.InstNo == "" ? DBNull.Value : (object)vchinst.InstNo);
                                cmd.Parameters.AddWithValue("@InstrumentDate", vchinst.InstDT == "" ? DBNull.Value : (object)vchinst.InstDT);
                                cmd.Parameters.AddWithValue("@insertedBy", UserID);
                                cmd.Parameters.AddWithValue("@sectorID", vchinst.SectorID);
                                cmd.Parameters.AddWithValue("@Accountcode", vchdet.GLID);
                                cmd.Parameters.AddWithValue("@reimbursementTypeID", 0);
                                cmd.Parameters.AddWithValue("@MaxVoucherNumber", voucherMast.VCHNo);
                                cmd.Parameters.AddWithValue("@AdjustmentAmount", vchinst.Amount);
                                cmd.Parameters.AddWithValue("@DRCR", vchinst.DRCR);
                                cmd.Parameters.AddWithValue("@VoucherSerial", vchdet.VchSrl);
                                cmd.Parameters.AddWithValue("@IsDelete", vchinst.IsDelete == null ? DBNull.Value : (object)vchinst.IsDelete);
                                cmd.Parameters.AddWithValue("@DeptCode", (vchinst.AccDeptCode == null || vchinst.AccDeptCode == "") ? DBNull.Value : (object)vchinst.AccDeptCode);
                                cmd.Parameters.AddWithValue("@CostCenterCode", (vchinst.AccCostCenterCode == null || vchinst.AccCostCenterCode == "") ? DBNull.Value : (object)vchinst.AccCostCenterCode);

                                cmd.ExecuteNonQuery();
                            }
                        }
                        if (vchdet.VchSubLedger.Length > 0)
                        {
                            foreach (VoucherSubLedgerType vsl in vchdet.VchSubLedger)
                            {
                                cmd = new SqlCommand("account.Update_Acc_SubLedgerType", db, transaction);
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.AddWithValue("@BillID", vsl.BillID == null ? 0 : (object)vsl.BillID);
                                cmd.Parameters.AddWithValue("@VCHNo", voucherMast.VCHNo);
                                cmd.Parameters.AddWithValue("@VCHSlNo", vchdet.VchSrl);   //vsl.SLSlNo
                                cmd.Parameters.AddWithValue("@AccountCode", vsl.AccountCODE);
                                cmd.Parameters.AddWithValue("@SubCode", vsl.SubLedgerCODE);
                                cmd.Parameters.AddWithValue("@ModeofAdj", vsl.SubLedgerTypeID);
                                cmd.Parameters.AddWithValue("@AdjNumber", vsl.Number);
                                cmd.Parameters.AddWithValue("@AdjDate", voucherMast.VCHDATE);
                                cmd.Parameters.AddWithValue("@AdjAmount", vsl.Amount);
                                cmd.Parameters.AddWithValue("@DRCR", vsl.DRCR);
                                cmd.Parameters.AddWithValue("@Qty", vsl.Qty == null ? DBNull.Value : (object)vsl.Qty);
                                cmd.Parameters.AddWithValue("@IRate", vsl.IRate == null ? DBNull.Value : (object)vsl.IRate);
                                cmd.Parameters.AddWithValue("@SectorID", vchdet.SectorID);
                                cmd.Parameters.AddWithValue("@IsDelete", vsl.IsDelete == null ? DBNull.Value : (object)vsl.IsDelete);
                                cmd.Parameters.AddWithValue("@InsertedBy", UserID);

                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public VoucherInfo Get_VoucherDetails(string VoucherTypeID, string VoucherDate, string VoucherNo, long UserID, string AccFinYear, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_VoucherSearchInfo", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@VCH_NO", VoucherNo);
            cmd.Parameters.AddWithValue("@VCH_TYPE", VoucherTypeID);
            cmd.Parameters.AddWithValue("@YrMonth", AccFinYear);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            VoucherInfo voucherInfo = new VoucherInfo();
            try
            {
                cn.Open();
                da.Fill(ds);
                if (ds.Tables.Count > 0)
                {
                   
                    VoucherMaster vchMast = new VoucherMaster();
                    VoucherDetail vchDetail = new VoucherDetail();
                    List<VoucherDetail> vchDet = new List<VoucherDetail>();
                    List<VoucherInstType> vchInstType = new List<VoucherInstType>();
                    List<VoucherSubLedgerType> vchslType = new List<VoucherSubLedgerType>();

                    DataTable dtMast = ds.Tables[0];
                    DataTable dtDetail = ds.Tables[1];
                    DataTable dtInst = ds.Tables[2];
                    DataTable dtSl = ds.Tables[2];

                    if (dtMast.Rows.Count > 0)
                    {
                        vchMast.YearMonth = dtMast.Rows[0]["FinYear"].ToString();
                        vchMast.VCHDATE = dtMast.Rows[0]["VCH_DATE"].ToString();
                        vchMast.VCHNo = Convert.ToInt32(dtMast.Rows[0]["VoucherNumber"].ToString());
                        vchMast.VoucherType = Convert.ToInt32(dtMast.Rows[0]["VoucherTypeID"].ToString());
                        vchMast.STATUS = dtMast.Rows[0]["STATUS"].ToString();
                        vchMast.VoucherNo = dtMast.Rows[0]["RefVoucherSlNo"].ToString();
                        vchMast.SectorID = Convert.ToInt32(dtMast.Rows[0]["SectorID"].ToString());
                        vchMast.NARRATION = dtMast.Rows[0]["NARRATION"].ToString();
                        vchMast.VCHAMOUNT = dtMast.Rows[0]["VCH_AMOUNT"].ToString();

                      
                        foreach (DataRow dr in dtDetail.Rows)
                        {
                            VoucherDetail vd = new VoucherDetail();
                            vd.YearMonth = dr["FinYear"].ToString();
                            vd.VoucherDetailID = Convert.ToInt32(dr["VoucherDetailID"].ToString());
                            vd.VoucherType = Convert.ToInt32(dr["VoucherTypeID"].ToString());
                            vd.VCHNo = Convert.ToInt32(dr["VoucherNumber"].ToString());
                            vd.VchSrl = Convert.ToInt32(dr["VCH_SRL"].ToString());
                            vd.OLDSLID = dr["OldAccountHead"].ToString();
                            vd.GLName = (dr["GL_NAME"].ToString());
                            vd.GLID = (dr["GL_ID"].ToString());
                            vd.SLID = "";
                            vd.SubID = "";
                            vd.OLDSUBID = "";
                            vd.SUBDesc = "";
                            vd.DRCR = (dr["DRCR"].ToString());
                            vd.GlType = (dr["GL_TYPE"].ToString());
                            vd.AMOUNT = Convert.ToDouble(dr["AMOUNT"].ToString());
                            vd.SectorID = Convert.ToInt32(dr["SectorID"].ToString());
                            vd.IsItemDetail = dr["IsItemDetail"].ToString();
                            //vd.AccCostCenterCode = (dr["CostCenterCode"].ToString());
                            //vd.AccDeptCode = (dr["DeptCode"].ToString());
                            //vd.AccDept = (dr["DeptDesc"].ToString());
                            //vd.AccCostCenter = (dr["CostCenterDesc"].ToString());
                            //vchDet.Add(vd);

                            //DataRow[] rows = dtInst.Select("GL_ID='" + dr["GL_ID"].ToString() + "' and SubCodeType='" + "I" + "'");
                            DataRow[] rows = dtInst.Select("GL_ID='" + dr["GL_ID"].ToString() + "' and VoucherDetailID='" + dr["VoucherDetailID"].ToString() + "' and SubCodeType='" + "I" + "'");
                            List<VoucherInstType> vchTmpInstType = new List<VoucherInstType>();
                            int j = 0;
                            foreach (DataRow row in rows)
                            {
                                VoucherInstType vit = new VoucherInstType();
                                vit.InstSrNo = j + 1;
                                vit.YearMonth = row["FinYear"].ToString();
                                vit.BillID = Convert.ToInt32(row["BillID"].ToString());
                                vit.VoucherType = Convert.ToInt32(row["VoucherTypeID"].ToString());
                                vit.VCHNo = Convert.ToInt32(row["VoucherNumber"].ToString());
                                vit.GLID = (row["GL_ID"].ToString());
                                vit.SLID = "";
                                vit.DRCR = (row["DRCR"].ToString());
                                vit.SlipNo = "";
                                vit.InstType = (row["InstrumentType"].ToString());
                                vit.InstTypeName = (row["InstTypeName"].ToString());
                                vit.InstNo = (row["InstrumentNumber"].ToString());
                                vit.InstDT = (row["INST_DT"].ToString());
                                vit.Amount = Convert.ToDouble(row["AdjustmentAmount"].ToString());
                                vit.ActualAmount = Convert.ToDouble(row["AdjustmentAmount"].ToString());
                                vit.Drawee = (row["DRAWEE"].ToString());
                                vit.Cleared = (row["CLEARED"].ToString());
                                vit.ClearedOn = (row["CLEARED_ON"].ToString());
                                vit.SectorID = Convert.ToInt32(row["SectorID"].ToString());
                                vit.AccCostCenterCode = (row["CostCenterCode"].ToString());
                                vit.AccDeptCode = (row["DeptCode"].ToString());
                                vit.AccDept = (row["DeptDesc"].ToString());
                                vit.AccCostCenter = (row["CostCenterDesc"].ToString());
                                vchTmpInstType.Add(vit);
                            }

                            //DataRow[] slrow = dtInst.Select("GL_ID='" + dr["GL_ID"].ToString() + "' and SubCodeType='" + "S" + "'");
                            DataRow[] slrow = dtInst.Select("GL_ID='" + dr["GL_ID"].ToString() + "' and VoucherDetailID='" + dr["VoucherDetailID"].ToString() + "' and SubCodeType='" + "S" + "'");
                            List<VoucherSubLedgerType> vchTmpSlType = new List<VoucherSubLedgerType>();
                            int k = 0;
                            foreach (DataRow row in slrow)
                            {
                                VoucherSubLedgerType sl = new VoucherSubLedgerType();
                                sl.SLSlNo = k + 1;
                                sl.BillID = Convert.ToInt32(row["BillID"].ToString());
                                sl.AccountCODE = row["GL_ID"].ToString();
                                sl.SubLedgerCODE = row["SubCode"].ToString();
                                sl.SubLedger = row["SubLedger"].ToString();
                                sl.SubLedgerTypeID = row["SubLedgerTypeID"].ToString();
                                sl.SubLedgerType = row["SubLedgerType"].ToString();
                                sl.Number = row["AdjustmentNumber"].ToString();
                                sl.Qty = Convert.ToDecimal(row["Qty"].ToString());
                                sl.IRate = Convert.ToDecimal(row["IRate"].ToString());
                                sl.Amount = row["AdjustmentAmount"].ToString();
                                sl.DRCR = row["DRCR"].ToString();
                                //sl.AccCostCenterCode = (row["CostCenterCode"].ToString());
                                //sl.AccDeptCode = (row["DeptCode"].ToString());
                                //sl.AccDept = (row["DeptDesc"].ToString());
                                //sl.AccCostCenter = (row["CostCenterDesc"].ToString());
                                vchTmpSlType.Add(sl);
                            }

                            vd.VchInstType = vchTmpInstType.ToArray();
                            vd.VchSubLedger = vchTmpSlType.ToArray();

                           
                            vchDet.Add(vd);
                        }

                    }
                    voucherInfo.SessionExpired = "N";
                    voucherInfo.EntryType = "E";

                    voucherInfo.VoucherMast = vchMast;
                    voucherInfo.VoucherDetail = vchDet.ToArray();
                    voucherInfo.tmpVchInstType = vchInstType.ToArray();
                    voucherInfo.tmpVchSubLedger = vchslType.ToArray();

                }
                return voucherInfo;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string Generate_Report(string FromDate, string ToDate, string VoucherNo, string FormName, string ReportName, string ReportType, long UserID)
        {
            string StrFormula = ""; string StrPaperSize = "Page - A4";

            string[] Fdate = FromDate.Split('/');
            string x1 = Fdate[0];
            string y1 = Fdate[1];
            string z1 = Fdate[2];
            string FinFromDate = z1 + "-" + y1 + "-" + x1;
            string FdateFormula = "Date(" + z1 + "," + y1 + "," + x1 + ")";

            string[] Tdate = ToDate.Split('/');
            string a = Tdate[0];
            string b = Tdate[1];
            string c = Tdate[2];
            string FinToDate = c + "-" + b + "-" + a;
            string TdateFormula = "Date(" + c + "," + b + "," + a + ")";

            SqlCommand cmd = new SqlCommand();
            cmd = new SqlCommand("account.Rpt_VoucherReportDateWise", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FRDate", FinFromDate);
            cmd.Parameters.AddWithValue("@TODate", FinToDate);
            cmd.Parameters.AddWithValue("@VoucherNumber", VoucherNo);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cn.Open();
            cmd.ExecuteNonQuery();
            try
            {
                if (Convert.ToInt32(VoucherNo) == 0)
                {
                    StrFormula = "{RPT_VoucherReport.VoucherDate}>=" + FdateFormula + "and{RPT_VoucherReport.VoucherDate}<=" + TdateFormula + "and{RPT_VoucherReport.InsertedBy}=" + UserID + "";
                }
                else
                {
                    StrFormula = "{RPT_VoucherReport.VoucherDate}>=" + FdateFormula + "and{RPT_VoucherReport.VoucherDate}<=" + TdateFormula + "and{RPT_VoucherReport.InsertedBy}=" + UserID + "and{RPT_VoucherReport.VoucherNumber}=" + VoucherNo + "";
                }

                cmd = new SqlCommand("account.Get_Check_ReportValue", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserID", UserID);
                cmd.Parameters.AddWithValue("@FormName", FormName);
                cmd.Parameters.AddWithValue("@ReportName", ReportName);
                cmd.Parameters.AddWithValue("@ReportType", ReportType);
                cmd.Parameters.AddWithValue("@StrFormula", StrFormula);
                cmd.Parameters.AddWithValue("@PaperSize", StrPaperSize);
                cmd.Parameters.Add("@ErrorCode", SqlDbType.Int);
                cmd.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 25);
                cmd.Parameters["@ErrorCode"].Direction = ParameterDirection.Output;
                cmd.Parameters["@ErrorMessage"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                
                return "";
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        #region Converter
        List<VoucherMaster_VM> Convert_Get_Acc_VchType(DataTable dt)
        {
            List<VoucherMaster_VM> lst = new List<VoucherMaster_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new VoucherMaster_VM();
                obj.VoucherTypeID = t.Field<int>("VoucherTypeID");
                obj.VoucherDescription = t.Field<string>("VoucherDescription");
                return obj;
            }).ToList();
            return lst;
        }

        List<VoucherMaster_VM> Convert_Search_AccountDetails(DataTable dt)
        {
            List<VoucherMaster_VM> lst = new List<VoucherMaster_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new VoucherMaster_VM();
                obj.GL_ID = t.Field<string>("GL_ID");
                obj.GL_NAME = t.Field<string>("GL_NAME");
                obj.GL_TYPE = t.Field<string>("GL_TYPE");
                obj.OLD_SL_ID = t.Field<string>("OLD_SL_ID");
                obj.MaintainItemDetail = t.Field<string>("MaintainItemDetail");
                return obj;
            }).ToList();
            return lst;
        }

        List<VoucherMaster_VM> Convert_Get_Instrument(DataTable dt)
        {
            List<VoucherMaster_VM> lst = new List<VoucherMaster_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new VoucherMaster_VM();
                obj.InstrumentTypeID = t.Field<string>("InstrumentTypeID");
                obj.Instrument = t.Field<string>("Instrument");
                return obj;
            }).ToList();
            return lst;
        }

        List<VoucherMaster_VM> Convert_Get_SubLedgerType(DataTable dt)
        {
            List<VoucherMaster_VM> lst = new List<VoucherMaster_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new VoucherMaster_VM();
                obj.SubLedgerTypeID = t.Field<string>("SubLedgerTypeID");
                obj.SubLedgerType = t.Field<string>("SubLedgerType");
                return obj;
            }).ToList();
            return lst;
        }

        List<VoucherMaster_VM> Convert_Search_VoucherNo(DataTable dt)
        {
            List<VoucherMaster_VM> lst = new List<VoucherMaster_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new VoucherMaster_VM();
                obj.VoucherNumber = t.Field<int?>("VoucherNumber");
                obj.RefVoucherSlNo = t.Field<string>("RefVoucherSlNo");
                return obj;
            }).ToList();
            return lst;
        }

        List<VoucherMaster_VM> Convert_Get_AccountDepartment(DataTable dt)
        {
            List<VoucherMaster_VM> lst = new List<VoucherMaster_VM>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new VoucherMaster_VM();
                obj.Code = t.Field<string>("Code");
                obj.Desccription = t.Field<string>("Desccription");
                return obj;
            }).ToList();
            return lst;
        }

        #endregion
    }
}