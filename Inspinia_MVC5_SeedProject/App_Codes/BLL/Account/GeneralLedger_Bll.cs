﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class GeneralLedger_Bll:BllBase
    {
        public DataTable Get_Ledger_Details(string Desc, string FinYear, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Load_Ledger", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Desc", Desc);
            cmd.Parameters.AddWithValue("@FinYear", FinYear);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
				 cmd.CommandTimeout = 0;
                da.Fill(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }

        public DataTable Show_GeneralLedger(string AccountCode, string Desc, string FinYear, string SectorID, string FromDate, string ToDate, string LegerType, string unPaidVch)
        {
            SqlCommand cmd = new SqlCommand("account.PROC_ACC_LEDGER", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@p_DT", FromDate);
            cmd.Parameters.AddWithValue("@p_DT1", ToDate);
            cmd.Parameters.AddWithValue("@p_SECTORIDS", SectorID);
            cmd.Parameters.AddWithValue("@p_GL_CODES", AccountCode);
            cmd.Parameters.AddWithValue("@p_GROUP_LEDGER", LegerType);
            cmd.Parameters.AddWithValue("@p_INCLUDE_UNPAID_VCH", unPaidVch == "" ? DBNull.Value : (object)unPaidVch);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
  cmd.CommandTimeout = 0;
                da.Fill(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return dt;
        }
    }
}