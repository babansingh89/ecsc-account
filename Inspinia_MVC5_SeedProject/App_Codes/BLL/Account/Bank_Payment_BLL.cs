﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.Models.Account;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class Bank_Payment_BLL:BllBase
    {
        public DataTable Show_Detail(string FromDate, string ToDate, string Desc)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.Load_BankPaymentDetail";
            cmd.Parameters.AddWithValue("@FromDate", FromDate);
            cmd.Parameters.AddWithValue("@ToDate", ToDate);
            cmd.Parameters.AddWithValue("@VoucherNo", Desc);
            cmd.Parameters.AddWithValue("@RepType", "D");
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return dt;
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public DataTable GetAuto(string AccountDescription, string SectorID)
        {
            SqlCommand cmd = new SqlCommand("account.Get_AutoBank", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AccountDescription", (object)AccountDescription ?? DBNull.Value);
            cmd.Parameters.AddWithValue("@SectorID", SectorID);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string Save(string BillID, string VoucherNumber, string VoucherDetailID, string AccountCode, string InstTypeID, string InstNo, string InstDate, string PaymentDate, long UserID)
        {
            string retunResult = "";

            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            SqlDataReader dr;
            try
            {
                try
                {
                    cmd = new SqlCommand("account.Update_BankPayment", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@BillID", BillID);
                    cmd.Parameters.AddWithValue("@VoucherNumber", VoucherNumber);
                    cmd.Parameters.AddWithValue("@VoucherDetailID", VoucherDetailID);
                    cmd.Parameters.AddWithValue("@AccountCode", AccountCode);
                    cmd.Parameters.AddWithValue("@InstTypeID", InstTypeID);
                    cmd.Parameters.AddWithValue("@InstNo", InstNo);
                    cmd.Parameters.AddWithValue("@InstDate", InstDate);
                    cmd.Parameters.AddWithValue("@PaymentDate", PaymentDate);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.ExecuteNonQuery();

                    transaction.Commit();
                    retunResult = "success";
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    retunResult = "fail";
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return retunResult;
        }
    }
}