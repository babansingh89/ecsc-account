﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL.Account
{
    public class PO_BLL: BllBase
    {
        public string InsertUpdate(PO_MD PO, List<PODetail_MD> PODetail, List<POSub_MD> POSub, long UserID)
        {
            string retunResult = ""; string POId = ""; string PONO = "";
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            SqlDataReader dr;
            try
            {
                try
                {
                    cmd = new SqlCommand("account.Insert_PO", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@PoId", PO.PoId == null ? DBNull.Value : (object)PO.PoId);
                    cmd.Parameters.AddWithValue("@VendorCode", PO.VendorCode == null ? DBNull.Value : (object)PO.VendorCode);
                    cmd.Parameters.AddWithValue("@VendEmail", PO.VendEmail == null ? DBNull.Value : (object)PO.VendEmail);
                    //cmd.Parameters.AddWithValue("@PoStatus", PO.PoStatus == null ? DBNull.Value : (object)PO.PoStatus);

                    cmd.Parameters.AddWithValue("@ShipTo", PO.ShipTo == null ? DBNull.Value : (object)PO.ShipTo);
                    cmd.Parameters.AddWithValue("@ShipAddr", PO.ShipAddr == null ? DBNull.Value : (object)PO.ShipAddr);
                    cmd.Parameters.AddWithValue("@ShipVia", PO.ShipVia == null ? DBNull.Value : (object)PO.ShipVia);
                    cmd.Parameters.AddWithValue("@PoNo", PO.PoNo == null ? DBNull.Value : (object)PO.PoNo);

                    cmd.Parameters.AddWithValue("@PoDate", PO.PoDate == null ? DBNull.Value : (object)PO.PoDate);
                    cmd.Parameters.AddWithValue("@CreditTerms", PO.CreditTerms == null ? DBNull.Value : (object)PO.CreditTerms);
                    //cmd.Parameters.AddWithValue("@TaxType", PO.TaxType == null ? DBNull.Value : (object)PO.TaxType);
                    cmd.Parameters.AddWithValue("@GrossAmt", PO.GrossAmt == null ? DBNull.Value : (object)PO.GrossAmt);

                    cmd.Parameters.AddWithValue("@AdjType", PO.AdjType == null ? DBNull.Value : (object)PO.AdjType);
                    cmd.Parameters.AddWithValue("@AdjAmt", PO.AdjAmt == null ? DBNull.Value : (object)PO.AdjAmt);
                    cmd.Parameters.AddWithValue("@BillAdjHead", PO.BillAdjHead == null ? DBNull.Value : (object)PO.BillAdjHead);
                    cmd.Parameters.AddWithValue("@RoundOff", PO.RoundOff == null ? DBNull.Value : (object)PO.RoundOff);

                    cmd.Parameters.AddWithValue("@NetAmt", PO.NetAmt == null ? DBNull.Value : (object)PO.NetAmt);
                    cmd.Parameters.AddWithValue("@Remarks", PO.Remarks == null ? DBNull.Value : (object)PO.Remarks);

                    cmd.Parameters.AddWithValue("@DocFilePath", PO.DocFilePath == null ? DBNull.Value : (object)PO.DocFilePath);
                    cmd.Parameters.AddWithValue("@SectorID", PO.Sectorid == null ? DBNull.Value : (object)PO.Sectorid);
                    cmd.Parameters.AddWithValue("@UserID", UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        POId = dr["POID"].ToString();
                        PONO = dr["PONO"].ToString();
                    }
                    dr.Close();

                    if (PODetail != null)
                    {
                        cmd = new SqlCommand("account.DELETE_FromAnyTable", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TableName", "DAT_Acc_PO");
                        cmd.Parameters.AddWithValue("@ColumnName", "PoId");
                        cmd.Parameters.AddWithValue("@PId", POId);
                        cmd.ExecuteNonQuery();

                        int i = 0;
                        foreach (var list in PODetail)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.Insert_PODetail", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PoId", POId);
                            cmd.Parameters.AddWithValue("@DatPoId", i);
                            cmd.Parameters.AddWithValue("@ItemId", list.ItemId);
                            cmd.Parameters.AddWithValue("@ProductServiseDesc", list.ProductServiseDesc);
                            cmd.Parameters.AddWithValue("@Qty", list.Qty);
                            cmd.Parameters.AddWithValue("@Rate", list.Rate);
                            cmd.Parameters.AddWithValue("@GrossAmt", list.GrossAmt);
                            cmd.Parameters.AddWithValue("@Discount", list.Discount);
                            cmd.Parameters.AddWithValue("@TaxTypeId", list.TaxTypeId);
                            cmd.Parameters.AddWithValue("@TaxAmt", list.TaxAmt);
                            cmd.Parameters.AddWithValue("@NetAmt", list.NetAmt);
                            cmd.Parameters.AddWithValue("@sectorid", list.sectorid);
                            cmd.Parameters.AddWithValue("@UserID", UserID);

                            cmd.ExecuteNonQuery();
                        }
                    }

                    if (POSub != null)
                    {
                        cmd = new SqlCommand("account.DELETE_FromAnyTable", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TableName", "DAT_Acc_POSub");
                        cmd.Parameters.AddWithValue("@ColumnName", "PoId");
                        cmd.Parameters.AddWithValue("@PId", POId);
                        cmd.ExecuteNonQuery();

                        int i = 0;
                        foreach (var list in POSub)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.Insert_POSubDetail", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@PoId", POId);
                            cmd.Parameters.AddWithValue("@DatPoId", i);
                            cmd.Parameters.AddWithValue("@TaxID", list.TaxID);
                            cmd.Parameters.AddWithValue("@taxTypeDetID", list.taxTypeDetID);
                            cmd.Parameters.AddWithValue("@TaxDes", list.TaxDes);
                            cmd.Parameters.AddWithValue("@TaxNetAmount", list.TaxNetAmount);
                            cmd.Parameters.AddWithValue("@GrossAmt", list.GrossAmt);
                            cmd.Parameters.AddWithValue("@UserID", UserID);

                            cmd.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();
                    retunResult = POId;
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    retunResult = "fail";
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return retunResult;
        }

        public string InsertUpdate_Bill(Bill_MD Bill, List<BillDetail_MD> BillDetail, List<BillSub_MD> BillSub, List<BillSubVch_MD> BillAccVch, long UserID)
        {
            string retunResult = ""; string BillId = ""; string BillNO = ""; string TabID = "";
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            SqlDataReader dr;
            try
            {
                try
                {
                    cmd = new SqlCommand("account.Insert_Bill", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@BillId", Bill.BillId == null ? DBNull.Value : (object)Bill.BillId);
                    cmd.Parameters.AddWithValue("@PoId", Bill.PoId == null ? DBNull.Value : (object)Bill.PoId);
                    cmd.Parameters.AddWithValue("@VendorCode", Bill.VendorCode == null ? DBNull.Value : (object)Bill.VendorCode);
                    cmd.Parameters.AddWithValue("@VendEmail", Bill.VendEmail == null ? DBNull.Value : (object)Bill.VendEmail);
                    cmd.Parameters.AddWithValue("@BillStatus", Bill.BillStatus == null ? DBNull.Value : (object)Bill.BillStatus);
                    cmd.Parameters.AddWithValue("@IncomeExpense", Bill.IncomeExpense == null ? DBNull.Value : (object)Bill.IncomeExpense);

                    cmd.Parameters.AddWithValue("@ShipTo", Bill.ShipTo == null ? DBNull.Value : (object)Bill.ShipTo);
                    cmd.Parameters.AddWithValue("@ShipAddr", Bill.ShipAddr == null ? DBNull.Value : (object)Bill.ShipAddr);
                    cmd.Parameters.AddWithValue("@ShipVia", Bill.ShipVia == null ? DBNull.Value : (object)Bill.ShipVia);
                    cmd.Parameters.AddWithValue("@BillNo", Bill.BillNo == null ? DBNull.Value : (object)Bill.BillNo);

                    cmd.Parameters.AddWithValue("@BillDate", Bill.BillDate == null ? DBNull.Value : (object)Bill.BillDate);
                    cmd.Parameters.AddWithValue("@DueDate", Bill.DueDate == null ? DBNull.Value : (object)Bill.DueDate);
                    cmd.Parameters.AddWithValue("@CreditTerms", Bill.CreditTerms == null ? DBNull.Value : (object)Bill.CreditTerms);
                    cmd.Parameters.AddWithValue("@VendorBillNo", Bill.VendorBillNo == null ? DBNull.Value : (object)Bill.VendorBillNo);
                    cmd.Parameters.AddWithValue("@VendorBillDate", Bill.VendorBillDate == null ? DBNull.Value : (object)Bill.VendorBillDate);

                    cmd.Parameters.AddWithValue("@BankID", Bill.BankID == null ? DBNull.Value : (object)Bill.BankID);
                    cmd.Parameters.AddWithValue("@InstType", Bill.InstrumentType == null ? DBNull.Value : (object)Bill.InstrumentType);
                    cmd.Parameters.AddWithValue("@InstNo", Bill.InstrumentNo == null ? DBNull.Value : (object)Bill.InstrumentNo);

                    cmd.Parameters.AddWithValue("@TaxType", Bill.TaxType == null ? DBNull.Value : (object)Bill.TaxType);
                    cmd.Parameters.AddWithValue("@TransType", Bill.TransType == null ? DBNull.Value : (object)Bill.TransType);
                    cmd.Parameters.AddWithValue("@GrossAmt", Bill.GrossAmt == null ? DBNull.Value : (object)Bill.GrossAmt);

                    cmd.Parameters.AddWithValue("@TaxTypeonTax", Bill.TaxTypeonTax == null ? DBNull.Value : (object)Bill.TaxTypeonTax);
                    cmd.Parameters.AddWithValue("@TaxTypeonTaxAmount", Bill.TaxTypeonTaxAmount == null ? DBNull.Value : (object)Bill.TaxTypeonTaxAmount);
                    cmd.Parameters.AddWithValue("@AdjType", Bill.AdjType == null ? DBNull.Value : (object)Bill.AdjType);
                    cmd.Parameters.AddWithValue("@AdjAmt", Bill.AdjAmt == null ? DBNull.Value : (object)Bill.AdjAmt);
                    cmd.Parameters.AddWithValue("@BillAdjHead", Bill.BillAdjHead == null ? DBNull.Value : (object)Bill.BillAdjHead);
                    cmd.Parameters.AddWithValue("@RoundOff", Bill.RoundOff == null ? DBNull.Value : (object)Bill.RoundOff);

                    cmd.Parameters.AddWithValue("@NetAmt", Bill.NetAmt == null ? DBNull.Value : (object)Bill.NetAmt);
                    cmd.Parameters.AddWithValue("@Remarks", Bill.Remarks == null ? DBNull.Value : (object)Bill.Remarks);

                    cmd.Parameters.AddWithValue("@DocFilePath", Bill.DocFilePath == null ? DBNull.Value : (object)Bill.DocFilePath);
                    cmd.Parameters.AddWithValue("@SectorID", Bill.Sectorid == null ? DBNull.Value : (object)Bill.Sectorid);
                    cmd.Parameters.AddWithValue("@UserID", UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        BillId = dr["BillID"].ToString();
                        BillNO = dr["BillNO"].ToString();
                    }
                    dr.Close();

                    if (BillDetail != null)
                    {
                        cmd = new SqlCommand("account.DELETE_FromAnyTable", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TableName", "DAT_Acc_Bill");
                        cmd.Parameters.AddWithValue("@ColumnName", "BillId");
                        cmd.Parameters.AddWithValue("@PId", BillId);
                        cmd.ExecuteNonQuery();

                        int i = 0;
                        foreach (var list in BillDetail)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.Insert_BillDetail", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@BillId", BillId);
                            cmd.Parameters.AddWithValue("@DatBillId", i);
                            cmd.Parameters.AddWithValue("@ItemId", list.ItemId);
                            cmd.Parameters.AddWithValue("@ProductServiseDesc", list.ProductServiseDesc);
                            cmd.Parameters.AddWithValue("@Qty", list.Qty);
                            cmd.Parameters.AddWithValue("@Rate", list.Rate);
                            cmd.Parameters.AddWithValue("@GrossAmt", list.GrossAmt);
                            cmd.Parameters.AddWithValue("@Discount", list.Discount);
                            cmd.Parameters.AddWithValue("@TaxTypeId", list.TaxTypeId);
                            cmd.Parameters.AddWithValue("@TaxAmt", list.TaxAmt);
                            cmd.Parameters.AddWithValue("@NetAmt", list.NetAmt);
                            cmd.Parameters.AddWithValue("@sectorid", list.sectorid);
                            cmd.Parameters.AddWithValue("@UserID", UserID);

                            cmd.ExecuteNonQuery();
                        }
                    }

                    if (BillSub != null)
                    {
                        cmd = new SqlCommand("account.DELETE_FromAnyTable", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TableName", "DAT_Acc_BillSub");
                        cmd.Parameters.AddWithValue("@ColumnName", "BillId");
                        cmd.Parameters.AddWithValue("@PId", BillId);
                        cmd.ExecuteNonQuery();

                        int i = 0;
                        foreach (var list in BillSub)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.Insert_BillSubDetail", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@BillId", BillId);
                            cmd.Parameters.AddWithValue("@DatBillId", i);
                            cmd.Parameters.AddWithValue("@TaxID", list.TaxID);
                            cmd.Parameters.AddWithValue("@taxTypeDetID", list.taxTypeDetID);
                            cmd.Parameters.AddWithValue("@TaxDes", list.TaxDes);
                            cmd.Parameters.AddWithValue("@TaxNetAmount", list.TaxNetAmount);
                            cmd.Parameters.AddWithValue("@GrossAmt", list.GrossAmt);
                            cmd.Parameters.AddWithValue("@UserID", UserID);

                            cmd.ExecuteNonQuery();
                        }
                    }

                    if (BillAccVch != null)
                    {
                        cmd = new SqlCommand("account.Insert_Bill_Voucher", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransType", "MST");
                        cmd.Parameters.AddWithValue("@BillId", BillId);
                        cmd.Parameters.AddWithValue("@PostStatus", "C");
                        cmd.Parameters.AddWithValue("@UserID", UserID);
                        dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            TabID = dr["TabID"].ToString();
                        }
                        dr.Close();

                        cmd = new SqlCommand("account.DELETE_FromAnyTable", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TableName", "MST_Acc_Bill_Voucher_Dat");
                        cmd.Parameters.AddWithValue("@ColumnName", "TabId");
                        cmd.Parameters.AddWithValue("@PId", TabID);
                        cmd.ExecuteNonQuery();

                        int i = 0;
                        foreach (var list in BillAccVch)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.Insert_Bill_Voucher", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@TransType", "DAT");
                            cmd.Parameters.AddWithValue("@TabID", TabID);
                            cmd.Parameters.AddWithValue("@Order", list.Ord);
                            cmd.Parameters.AddWithValue("@DRCR", list.Drcr);
                            cmd.Parameters.AddWithValue("@AccCode", list.AccountCode);
                            cmd.Parameters.AddWithValue("@Amount", list.Amount);
                            cmd.Parameters.AddWithValue("@UserID", UserID);

                            cmd.ExecuteNonQuery();
                        }
                        cmd = new SqlCommand("account.Insert_Bill_Voucher", dbConn, transaction);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TransType", "MST");
                        cmd.Parameters.AddWithValue("@BillId", BillId);
                        cmd.Parameters.AddWithValue("@PostStatus", "P");
                        cmd.Parameters.AddWithValue("@UserID", UserID);
                        cmd.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    retunResult = BillId;
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    retunResult = "fail";
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
                transaction.Dispose();
            }
            return retunResult;
        }

        public string InsertUpdate_Payment(Bill_MD Bill, List<BillDetail_MD> BillDetail, long UserID)
        {
            string retunResult = ""; string BillId = ""; string BillNO = ""; 
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            SqlDataReader dr;
            try
            {
                try
                {
                    cmd = new SqlCommand("account.Insert_Bill", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@BillId", Bill.BillId == null ? DBNull.Value : (object)Bill.BillId);
                    cmd.Parameters.AddWithValue("@VendorCode", Bill.VendorCode == null ? DBNull.Value : (object)Bill.VendorCode);
                    cmd.Parameters.AddWithValue("@VendEmail", Bill.VendEmail == null ? DBNull.Value : (object)Bill.VendEmail);
                    cmd.Parameters.AddWithValue("@BillStatus", Bill.BillStatus == null ? DBNull.Value : (object)Bill.BillStatus);
                    cmd.Parameters.AddWithValue("@IncomeExpense", Bill.IncomeExpense == null ? DBNull.Value : (object)Bill.IncomeExpense);

                    cmd.Parameters.AddWithValue("@BillNo", Bill.BillNo == null ? DBNull.Value : (object)Bill.BillNo);
                    cmd.Parameters.AddWithValue("@BillDate", Bill.BillDate == null ? DBNull.Value : (object)Bill.BillDate);

                    cmd.Parameters.AddWithValue("@BankID", Bill.BankID == null ? DBNull.Value : (object)Bill.BankID);
                    cmd.Parameters.AddWithValue("@InstType", Bill.InstrumentType == null ? DBNull.Value : (object)Bill.InstrumentType);
                    cmd.Parameters.AddWithValue("@InstNo", Bill.InstrumentNo == null ? DBNull.Value : (object)Bill.InstrumentNo);
                    cmd.Parameters.AddWithValue("@InstDate", Bill.InstrumentDate == null ? DBNull.Value : (object)Bill.InstrumentDate);

                    cmd.Parameters.AddWithValue("@TaxType", Bill.TaxType == null ? DBNull.Value : (object)Bill.TaxType);
                    cmd.Parameters.AddWithValue("@TransType", Bill.TransType == null ? DBNull.Value : (object)Bill.TransType);
                    

                    cmd.Parameters.AddWithValue("@TaxTypeonTax", Bill.TaxTypeonTax == null ? DBNull.Value : (object)Bill.TaxTypeonTax);
                    cmd.Parameters.AddWithValue("@TaxTypeonTaxAmount", Bill.TaxTypeonTaxAmount == null ? DBNull.Value : (object)Bill.TaxTypeonTaxAmount);
                    cmd.Parameters.AddWithValue("@AdjType", Bill.AdjType == null ? DBNull.Value : (object)Bill.AdjType);
                    cmd.Parameters.AddWithValue("@AdjAmt", Bill.AdjAmt == null ? DBNull.Value : (object)Bill.AdjAmt);
                    cmd.Parameters.AddWithValue("@BillAdjHead", Bill.BillAdjHead == null ? DBNull.Value : (object)Bill.BillAdjHead);
                    cmd.Parameters.AddWithValue("@RoundOff", Bill.RoundOff == null ? DBNull.Value : (object)Bill.RoundOff);
                    cmd.Parameters.AddWithValue("@GrossAmt", Bill.GrossAmt == null ? DBNull.Value : (object)Bill.GrossAmt);
                    cmd.Parameters.AddWithValue("@NetAmt", Bill.NetAmt == null ? DBNull.Value : (object)Bill.NetAmt);
                    cmd.Parameters.AddWithValue("@Remarks", Bill.Remarks == null ? DBNull.Value : (object)Bill.Remarks);

                    cmd.Parameters.AddWithValue("@DocFilePath", Bill.DocFilePath == null ? DBNull.Value : (object)Bill.DocFilePath);
                    cmd.Parameters.AddWithValue("@SectorID", Bill.Sectorid == null ? DBNull.Value : (object)Bill.Sectorid);
                    cmd.Parameters.AddWithValue("@UserID", UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        BillId = dr["BillID"].ToString();
                        BillNO = dr["BillNO"].ToString();
                    }
                    dr.Close();

                    if (BillDetail != null)
                    {
                        cmd = new SqlCommand("account.DELETE_FromAnyTable", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TableName", "DAT_Acc_Bill");
                        cmd.Parameters.AddWithValue("@ColumnName", "BillId");
                        cmd.Parameters.AddWithValue("@PId", BillId);
                        cmd.ExecuteNonQuery();

                        int i = 0;
                        foreach (var list in BillDetail)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.Insert_BillDetail", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@BillId", BillId);
                            cmd.Parameters.AddWithValue("@DatBillId", i);
                            cmd.Parameters.AddWithValue("@ItemId", list.ItemId);
                            cmd.Parameters.AddWithValue("@GrossAmt", list.GrossAmt);
                            cmd.Parameters.AddWithValue("@NetAmt", list.NetAmt);
                            cmd.Parameters.AddWithValue("@sectorid", list.sectorid);
                            cmd.Parameters.AddWithValue("@UserID", UserID);

                            cmd.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();
                    retunResult = BillId;
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    retunResult = "fail";
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return retunResult;
        }

        #region

        #endregion
    }
}