﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Account;
using wbEcsc.Models.Application;

namespace wbEcsc.App_Codes.BLL.Account
{
   
    public class Item_BLL: BllBase
    {
        List<Item_MD> lst = new List<Item_MD>();
        public List<Item_MD> ItemUnit()
        {
            SqlCommand cmd = new SqlCommand("account.Load_ItemUnit", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                lst = ConvertDataTable<Item_MD>(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return lst;
        }
        public List<Item_MD> ItemCategory()
        {
            SqlCommand cmd = new SqlCommand("account.Load_ItemCategory", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                lst = ConvertDataTable<Item_MD>(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return lst;
        }
        public List<Item_MD> TaxType()
        {
            SqlCommand cmd = new SqlCommand("account.Load_TaxType", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                lst = ConvertDataTable<Item_MD>(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return lst;
        }
        public List<Bill_MD> getAllBank()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.CommandText = "account.GET_BankList_BenMaster";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                sda.Fill(dt);
                return ConvertDataTable<Bill_MD>(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        public string InsertUpdate(Item_MD item, List<Item_MD> CatDesc, long UserID)
        {
            string retunResult = ""; string ItemId = "";
            SqlConnection dbConn = new SqlConnection(cn.ConnectionString);

            SqlCommand cmd = new SqlCommand();
            System.Data.SqlClient.SqlTransaction transaction;
            dbConn.Open();
            transaction = dbConn.BeginTransaction();

            SqlDataReader dr;
            try
            {
                try
                {
                    cmd = new SqlCommand("account.Insert_Item", dbConn, transaction);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@AccountCode", item.AccountCode == null ? DBNull.Value : (object)item.AccountCode);
                    cmd.Parameters.AddWithValue("@AccountTypeID", item.AccountTypeID == null ? DBNull.Value : (object)item.AccountTypeID);
                    cmd.Parameters.AddWithValue("@AccDesc", item.AccountDescription == null ? DBNull.Value : (object)item.AccountDescription);
                    cmd.Parameters.AddWithValue("@OldAccountHead", item.OldAccountHead == null ? DBNull.Value : (object)item.OldAccountHead);

                    cmd.Parameters.AddWithValue("@ParentAccCode", item.ParentAccountCode == null ? DBNull.Value : (object)item.ParentAccountCode);
                    cmd.Parameters.AddWithValue("@HSNCode", item.HsnCode == null ? DBNull.Value : (object)item.HsnCode);
                    cmd.Parameters.AddWithValue("@TaxTypeID", item.TaxTypeId == null ? DBNull.Value : (object)item.TaxTypeId);
                    cmd.Parameters.AddWithValue("@UnitID", item.UnitId == null ? DBNull.Value : (object)item.UnitId);
                    
                    cmd.Parameters.AddWithValue("@OPBalance", item.OpBalance == null ? DBNull.Value : (object)item.OpBalance);
                    cmd.Parameters.AddWithValue("@OPQty", item.OpbalQty == null ? DBNull.Value : (object)item.OpbalQty);
                    cmd.Parameters.AddWithValue("@OPBalAsonDate", item.OpbalAsOn == null ? DBNull.Value : (object)item.OpbalAsOn);
                    cmd.Parameters.AddWithValue("@LowStkAlert", item.LowStkAlert == null ? DBNull.Value : (object)item.LowStkAlert);

                    cmd.Parameters.AddWithValue("@PurAccCode", item.PurAccCode == null ? DBNull.Value : (object)item.PurAccCode);
                    cmd.Parameters.AddWithValue("@SaleAccCode", item.SaleAccCode == null ? DBNull.Value : (object)item.SaleAccCode);
                    cmd.Parameters.AddWithValue("@PrefVendorCode", item.PrefVendorCode == null ? DBNull.Value : (object)item.PrefVendorCode);

                    cmd.Parameters.AddWithValue("@DocFilePath", item.DocFilePath == null ? DBNull.Value : (object)item.DocFilePath);
                    cmd.Parameters.AddWithValue("@SectorID", item.Sectorid == null ? DBNull.Value : (object)item.Sectorid);
                    cmd.Parameters.AddWithValue("@UserID", UserID);

                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        ItemId = dr["AccountCode"].ToString();
                    }
                    dr.Close();

                    if (CatDesc != null)
                    {
                        cmd = new SqlCommand("account.DELETE_FromAnyTable", dbConn, transaction);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@TableName", "DAT_Item_Category");
                        cmd.Parameters.AddWithValue("@ColumnName", "AccountCode");
                        cmd.Parameters.AddWithValue("@PId", ItemId);
                        cmd.ExecuteNonQuery();

                        int i = 0;
                        foreach (var list in CatDesc)
                        {
                            i = i + 1;
                            cmd = new SqlCommand("account.Insert_ItemCategory", dbConn, transaction);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@CategoryDetailID", i);
                            cmd.Parameters.AddWithValue("@CategoryID", list.ItemCatId);
                            cmd.Parameters.AddWithValue("@AccountCode", ItemId);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    transaction.Commit();
                    retunResult = ItemId;
                }
                catch (SqlException sqlError)
                {
                    transaction.Rollback();
                    retunResult = "fail";
                    throw new Exception(sqlError.Message);
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            finally
            {
                dbConn.Close();
            }
            return retunResult;
        }

        // Bill
        public List<Term_MD> Term()
        {
            List<Term_MD> lst = new List<Term_MD>();
            SqlCommand cmd = new SqlCommand("account.Load_Term", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                lst = ConvertDataTable<Term_MD>(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return lst;
        }
        public List<BillAdjHead_MD> BillAdjHead(string TransType)
        {
            List<BillAdjHead_MD> lst = new List<BillAdjHead_MD>();
            SqlCommand cmd = new SqlCommand("account.Load_BillAdjHead", cn);
            cmd.Parameters.AddWithValue("@TransType", TransType);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                lst = ConvertDataTable<BillAdjHead_MD>(dt);
            }
            catch (SqlException)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
            return lst;
        }
        public List<Account_MD> Account_Description(string TransactionType, string Desc, string MND, string SetorID, string VendorCode)
        {
            SqlCommand cmd = new SqlCommand("account.Get_AccountDescByGroupLedger", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@TransactionType", TransactionType);
            cmd.Parameters.AddWithValue("@AccountDesc", Desc);
            cmd.Parameters.AddWithValue("@MND", MND);
            cmd.Parameters.AddWithValue("@SectorID", SetorID);
            cmd.Parameters.AddWithValue("@VendorCode", VendorCode);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            try
            {
                cn.Open();
                da.Fill(dt);
                return Convert(dt);
               
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                cn.Close();
            }
        }

        #region
        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        if (dr[column.ColumnName] == DBNull.Value)
                            pro.SetValue(obj, null, null);
                        else
                            pro.SetValue(obj, dr[column.ColumnName], null);
                    }
                    else
                        continue;
                }
            }
            return obj;
        }
        private static T GetItem1<T>(DataTable dt)
        {
            Type temp = typeof(T);

            T data = Activator.CreateInstance<T>();
            foreach (DataRow row in dt.Rows)
            {
                data = GetItem<T>(row);
            }


            return data;
        }

        List<Account_MD> Convert(DataTable dt)
        {
            List<Account_MD> lst = new List<Account_MD>();
            lst = dt.AsEnumerable().Select(t =>
            {
                var obj = new Account_MD();
                obj.AccountCode = t.Field<string>("AccountCode");
                obj.AccountDescription = t.Field<string>("AccountDescription");
                obj.ParentAccountCode = t.Field<string>("ParentAccountCode");
                return obj;
            }).ToList();
            return lst;
        }
        #endregion
    }


}