﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for VoucherInstType
/// </summary>
public class VoucherInstType
{
    public int? BillID { get; set; }
    public int InstSrNo { get; set; }
    public string YearMonth { get; set; }
    public int VoucherType { get; set; }
    public int VCHNo { get; set; }    
    public string GLID { get; set; }
    public string SLID { get; set; }
    public string DRCR { get; set; }
    public string SlipNo { get; set; }
    public string InstType { get; set; }
    public string InstTypeName { get; set; }
    public string InstNo { get; set; }
    public string InstDT { get; set; }
    public double Amount { get; set; }
    public double ActualAmount { get; set; }
    public string Drawee { get; set; }
    public string Cleared { get; set; }
    public string ClearedOn { get; set; }
    public int SectorID { get; set; }
    public string IsDelete { get; set; }

    public string AccDeptCode { get; set; }
    public string AccCostCenterCode { get; set; }
    public string AccDept { get; set; }
    public string AccCostCenter { get; set; }
    public VoucherInstType()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}