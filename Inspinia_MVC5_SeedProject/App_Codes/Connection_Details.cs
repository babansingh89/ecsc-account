﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using wbEcsc.App_Codes.BLL;

namespace wbEcsc.App_Codes
{
    public class Connection_Details
    {
       
        public static string ServerName()
        {
            string server = "";
            try
            {
                string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.ConnectionString = conString;
                server = builder.DataSource;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return server;
        }
        public static string DatabaseName()
        {
            string database = "";
            try
            {
                string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.ConnectionString = conString;
                database = builder.InitialCatalog;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return database;
        }
        public static string UserID()
        {
            string userid = "";
            try
            {
                string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.ConnectionString = conString;
                userid = builder.UserID;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return userid;
        }
        public static string Password()
        {
            string pwd = "";
            try
            {
                string conString = ConfigurationManager.ConnectionStrings["constring_payroll"].ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.ConnectionString = conString;
                pwd = builder.Password;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return pwd;
        }
    }
}