﻿using System.Security;
using wbEcsc.App_Codes.BLL;
using wbEcsc.Models.Account;
using wbEcsc.Models.Account.Logins;

namespace wbEcsc.App_Codes.Authenticator
{

    public class AdministrativeAuthenticator : IAuthentication<AdministrativeUser>
    {
        private AdministrativeLogin _userLogin;
        AdministrativeUser_BLL mnAuth = new AdministrativeUser_BLL();
        public AdministrativeAuthenticator(AdministrativeLogin userLogin)
        {
            this._userLogin = userLogin;
        }

        public AdministrativeUser Authenticate()
        {
            if (_userLogin == null || string.IsNullOrWhiteSpace(_userLogin.UserName) || string.IsNullOrWhiteSpace(_userLogin.Password))
            {
                throw new SecurityException("User Name or password Can not be Empty");
            }
            AdministrativeUser muObj = mnAuth.GetUser(_userLogin.UserName, _userLogin.Password);
            if (muObj == null)
            {
                throw new SecurityException("Invalid User Name or password");
            }
            if(muObj.IsAccountLocked==true)
            {
                throw new SecurityException("User Account Locked! Please Contact to Admnistrator");
            }
            return muObj;
        }
    }
   
}