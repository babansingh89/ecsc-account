﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.ViewModels
{
    public class BankViewModel
    {
        public Bank_MD SelectedBank { get; set; }
        public List<Bank_MD> BankList { get; set; } 
    }
}