﻿

$(document).ready(function () {

    AccountClosingDate();

    $(".select2_demo_2").select2({
        placeholder: "Select Sector",
        allowClear: true
    });

    $('#btnsubmit').click(function () {
        CloseAccount();
    });

    var sectorID = localStorage.getItem("SectorID");
    var FinYearstorage = localStorage.getItem("FinYear");
    //FinYear(sectorID, function (finyears) {
    //    $('#ddlAccCloseFinYear').find('option:contains("' + FinYearstorage + '")').prop('selected', true);
    //});
});
function AccountClosingDate() {
    $('#txtClosingDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        todayHighlight: true
    });

}
function CloseAccount() {

    var SectorID = $('#lstSector').val();
    var FinYear = $("#ddlGlobalFinYear option:selected").text(); 
    var TransType = $('#ddlAccClose').val();
    var ClosingDate = $('#txtClosingDate').val();

    if (TransType == '') {
        toastr.error('Please, Select Account Closing Type !!', 'Warning')
        $("#ddlAccClose").focus(); return false;
    }
    if (FinYear == '') {
        toastr.error('Please, Select Financial Year !!', 'Warning')
        $("#ddlGlobalFinYear").focus(); return false;
    }
    if (SectorID == null || SectorID == "") {
        toastr.error('Please, Select Sector !!', 'Warning')
        $(".select2-search__field").focus();  return false;
    }

    if (ClosingDate == '') {
        toastr.error('Please, Select Account Closing Date !!', 'Warning')
        $("#txtClosingDate").focus(); return false;
    }

    if ($('#txtClosingDate').val() != '') {
        var t = $('#txtClosingDate').val().split('/');
        ClosingDate = t[2] + '-' + t[1] + '-' + t[0];
    }

    var E = "{SectorID: '" + SectorID + "', FinYear: '" + FinYear + "', TransType: '" + TransType + "', ClosingDate: '" + ClosingDate + "'}";
    //alert(E);
    //return false;
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/AccountClose/CloseAccount',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var t = data.Data; 
            var ID = t[0].status; 
            var Msg = t[0].Mess;

           
                swal({
                    title: "Success",
                    text: Msg,
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            $('#ddlAccClose').val('');
                            $('#txtClosingDate').val('');
                            return false;
                        }
                    });
            
        }
    });
}
function FinYear(sectorId, callback) {
    var S = "{SectorID:'" + sectorId + "'}";
    $.ajax({
        url: '/Accounts_Form/AccountMaster/FinYear',
        type: 'POST',
        data: S,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var data = response.Data;

            if (data.length > 0) {
                var ctlr_ddlGlobalFinYear = $('#ddlAccCloseFinYear');
                ctlr_ddlGlobalFinYear.empty();
                ctlr_ddlGlobalFinYear.append('<option value="">Select</option>');
                $(data).each(function (index, item) {
                    ctlr_ddlGlobalFinYear.append('<option value=' + item.FinYrStatus + '>' + item.FinYr + '</option>');
                });
                callback(data);
            }
        }
    });
}




















