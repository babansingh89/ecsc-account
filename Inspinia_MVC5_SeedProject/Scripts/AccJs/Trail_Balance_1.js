﻿var ArrayOpenListID = []; var ArrayOpenAccDesc = []; var isGLSL = "L";
var hide_Columns = [];
var sum_Columns = [];
var Grid_Level = 0;

$(document).ready(function () {

    var frm_TrailBalance = $("#frmTrailBalance");
    MyForm_Validate();

    //Sub Ledger hide and show
    $('#radioGL').change(function () {
        isGLSL = "L";
        $("#pnl_SubLedger").css("display", "none");
        $('#txtSearchSubLedger').val('');
        $('#hdnSearchSubLedger').val('');
    });
    $('#radioSL').change(function () {
        isGLSL = "S";
        $("#pnl_SubLedger").css("display", "block");
        $('#txtSearchSubLedger').focus();
    });

    //Date Checking
    Set_DefaultDate();
    SetFrom_Date();
    SetTo_Date();
    $("#txtToDate").datepicker("option", "minDate", $('#txtFromDate').val());
    //Populate_Grid($('#txtFromDate').val(), $('#txtToDate').val());

    //Show Button Click
    $("#btnShow").click(function () {
        if ($("#txtFromDate").val() == "") { $("#txtFromDate").focus(); return false; }
        if ($("#txtToDate").val() == "") { $("#txtToDate").focus(); return false; }

       // Load();
        //Between_Dates();

        if (isGLSL == 'S') { if ($("#hdnSearchSubLedger").val() == "") { $("#txtSearchSubLedger").focus(); return false; } }

        var fdate = $("#txtFromDate").val();
        var tdate = $("#txtToDate").val();

        if (fdate != "") {
            var rDate = fdate.split('/');
            fdate = rDate[2] + "-" + rDate[1] + "-" + rDate[0];
        }
        if (tdate != "") {
            var trDate = tdate.split('/');
            tdate = trDate[2] + "-" + trDate[1] + "-" + trDate[0];
        }

        var E = "{FromDate: '" + fdate + "', ToDate: '" + tdate + "', SectorID: '" + $('#ddlSector').val() + "', GLSL: '" + isGLSL + "', ParentAccountCode: '" + $('#hdnSearchSubLedger').val() + "'}";
        $.ajax({
            type: "POST",
            url: "/Accounts_Form/TrailBalance/Show_Details",
            data: E,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t.length > 0 && D.Status == 200) {
                    Grid_Level = parseInt(Grid_Level) + 1;

                    hide_Columns = [0, 8];
                    Populate_0(t, hide_Columns);
                }
            }
        });
    });


    $('#myDiv').on('click', 'tbody >tr', function () {
        var AccCode = $(this).closest('tr').find('.AccountCode').text(); 
        var AccountDes = $(this).closest('tr').find('.AccountDescription').text();
        var MONTH_SRL = $(this).closest('tr').find('.MONTH_SRL').text(); //alert(MONTH_SRL);
        var VoucherNumber = $(this).closest('tr').find('.VoucherNumber').text(); //alert(VoucherNumber);

     
            if (MONTH_SRL != "" && MONTH_SRL > 0)
                Populate_Exp_Main_3(AccCode, AccountDes);
            else if (VoucherNumber != "")
                window.open('/Accounts_Form/VoucherMaster/Index/' + VoucherNumber, '_blank');
            else if (MONTH_SRL == "")
                Populate_Exp_Main_2(AccCode, AccountDes);

    });
   
    //Print
    $('#btnPrint').click(function (evt) { Print(); });

    $(document).keyup(function (e) {
        if (e.keyCode === 27) {
            Back();
        }
    });

    $("#txtSearchSubLedger").autocomplete({
        source: function (request, response) {

            var V = "{AccountCode:'" + "" + "', Desc:'" + $("#txtSearchSubLedger").val() + "', FinYear:'" + $("#txtAccFinYear").val() + "',  SectorID:'" + $("#ddlSector").val() + "'}";
            $.ajax({
                type: "POST",
                url: "/Accounts_Form/AccountsVoucherOpening/Auto_SubLedger_Details",
                data: V,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        minLength: 0,
        select: function (e, i) {

            $("#hdnSearchSubLedger").val(i.item.AccountCode);
            Between_Dates();
        },
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtSearchSubLedger').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtSearchSubLedger").val('');
            $("#hdnSearchSubLedger").val('');
        }
        if (iKeyCode == 46) {
            $("#txtSearchSubLedger").val('');
            $("#hdnSearchSubLedger").val('');
        }
    });

    $("#btnBack").click(function () {
        Back();
    });
});
function Load()
{
    if (isGLSL == 'S') { if ($("#hdnSearchSubLedger").val() == "") { $("#txtSearchSubLedger").focus(); return false; } }

    var fdate = $("#txtFromDate").val();
    var tdate = $("#txtToDate").val();

    if (fdate != "") {
        var rDate = fdate.split('/');
        fdate = rDate[2] + "-" + rDate[1] + "-" + rDate[0];
    }
    if (tdate != "") {
        var trDate = tdate.split('/');
        tdate = trDate[2] + "-" + trDate[1] + "-" + trDate[0];
    }

    var E = "{FromDate: '" + fdate + "', ToDate: '" + tdate + "', SectorID: '" + $('#ddlSector').val() + "', GLSL: '" + isGLSL + "', ParentAccountCode: '" + $('#hdnSearchSubLedger').val() + "'}";
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/TrailBalance/Show_Details",
        data: E,
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data; 
            if (t.length > 0 && D.Status == 200) {
                Grid_Level = parseInt(Grid_Level) + 1;

                hide_Columns = [0, 8];
                Populate_0(t, hide_Columns);
            }
        }
    });
}
//Date Checking
 function SetFrom_Date() {
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            var a = $('#txtAccFinYear').val();
            if (a != "") {
                var res = FinYear_Validation(a);
                if (res == false) {
                    alert('Sorry ! Please Enter Valid Account Financial Year.'); $('#txtFromDate').val(''); return false;
                }
                else
                    $("#txtToDate").datepicker("option", "minDate", $('#txtFromDate').val());
            }
        }
    });
}
 function SetTo_Date() {
    $('#txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
 function Set_DefaultDate() {
    //Set From Date
    var a = $('#txtAccFinYear').val();
    var aa = FinYear_Validation(a);
    if (aa == true) {
        var fy = a.split('-');
        var validDate = '01/04/' + fy[0];
        $('#txtFromDate').val(validDate);
    }
    else {
        alert('Sorry ! Wrong Account Fin Year.'); return false;
    }

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtToDate').val(output);
}
 function FinYear_Validation(year) {

    if (year != "") {

        var str1 = $.trim(year);
        var str2 = "-";
        var a1 = ""; var a2 = "";
        //Special Character Checking
        if (str1.indexOf(str2) == -1) {
            return false;
        }
        else {
            var a = year.split('-');
            a1 = a[0];
            a2 = a[1];
            //Length Checking
            if ($.trim(a1).length != 4)
                return false;
            if ($.trim(a2).length != 4)
                return false;

            //isNumeric Checking
            var s1 = $.isNumeric(a1);
            var s2 = $.isNumeric(a2);
            if (s1 == false)
                return false;
            if (s2 == false)
                return false;

            //Max and Min Year Checking
            var g = parseInt(a1) + 1;
            if (g != a2)
                return false;
        }

        return true;
    }
    else
        return false;

}


//Show Button Click
 function MyForm_Validate() {
    $('#frmTrailBalance').validate({
        rules: {
            txtToDate: {
                required: true
            },
            txtFromDate: {
                required: true
            }
        }
    });
}
 function Between_Dates()
 {
     if ($("#txtFromDate").val() == "") { $("#txtFromDate").focus(); return false; }
     if ($("#txtToDate").val() == "") { $("#txtToDate").focus(); return false; }
     if (isGLSL == 'S') { if ($("#hdnSearchSubLedger").val() == "") { $("#txtSearchSubLedger").focus(); return false; } }

     var fdate = $('#txtFromDate').val();
     var tdate = $("#txtToDate").val();

     var fSplitDate = fdate.split('/');
     var tSplitDate = tdate.split('/');

     var a = $('#txtAccFinYear').val();
     if (a != '') {
         var fy = a.split('-');
         a1 = fy[0];
         a2 = fy[1];

         var startDate = "01/04/" + a1;
         var endDate = "31/03/" + a2;

         var newStartDate = new Date(a1, 03, 01);
         var newendDate = new Date(a2, 02, 31);

         var givenFromDate = new Date(fSplitDate[2], parseInt(fSplitDate[1]) - 1, fSplitDate[0]); 
         var givenToDate = new Date(tSplitDate[2], parseInt(tSplitDate[1]) - 1, tSplitDate[0]);

         //if ((givenFromDate >= newStartDate && givenFromDate <= newendDate)) {}
         //else
         //{
             //alert('Sorry ! Given From Date is not within Account Financial Year.'); $('#txtFromDate').focus(); return false;
         //}

         //if ((givenToDate >= newStartDate && givenToDate <= newendDate)){}
         //else {
             //alert('Sorry ! Given To Date is not within Account Financial Year.'); $('#txtToDate').focus(); return false;
         //}

         Populate_Grid(fdate, tdate);
     }
 }

//on page load
 function Populate_Grid(fdate, tdate)
 {
    
     if (fdate != "")
     {
         var rDate = fdate.split('/');
         fdate = rDate[2] + "-" + rDate[1] + "-" + rDate[0];
     }
     if (tdate != "") {
         var trDate = tdate.split('/');
         tdate = trDate[2] + "-" + trDate[1] + "-" + trDate[0];
     }
     var E = "{FromDate: '" + fdate + "', ToDate: '" + tdate + "', SectorID: '" + $('#ddlSector').val() + "', GLSL: '" + isGLSL + "', ParentAccountCode: '" + $('#hdnSearchSubLedger').val() + "'}";
	//$('#page-wrapper').toggleClass('sk-loading');
     $.ajax({
         type: "POST",
         url: "/Accounts_Form/TrailBalance/Show_Details",
         data: E,
         contentType: "application/json; charset=utf-8",
         success: function (D) {
             var t = D.Data; 
             if(t.length > 0 && D.Status == 200)
             {
                 Grid_Level = parseInt(Grid_Level) + 1;
                 
                 hide_Columns=[0,8];
                 Populate_0(t, hide_Columns);
				//$('#page-wrapper').removeClass('sk-loading');
             }
         }
     });
 }
 function Populate_0(data, hide_Columns)   //============================================================================================ 1
 {
     var html = ""; var slNo = 0; var headers = [];
     var totalOpenDR = 0; var totalOpenCR = 0; var totalTransDR = 0; var totalTransCR = 0; var totalClosingDR = 0; var totalClosingCR = 0;

     var table = $("#myTable");
     table.find('thead').empty();
     table.find('tbody').empty();
     table.find('tfoot').empty();

     html += "<tr>";
     

     //populate header
     Populate_Headers_1(data, hide_Columns);

     for (var i = 0; i < data.length; i++) {

         var isSubLedger = data[i].SubLedger;

        totalOpenDR = parseFloat(totalOpenDR) + parseFloat(data[i].OpeningBalDR);
        totalOpenCR = parseFloat(totalOpenCR) + parseFloat(data[i].OpeningBalCR);
        totalTransDR = parseFloat(totalTransDR) + parseFloat(data[i].TransactionBalDR);
        totalTransCR = parseFloat(totalTransCR) + parseFloat(data[i].TransactionBalCR);
        totalClosingDR = parseFloat(totalClosingDR) + parseFloat(data[i].ClosingBalDR);
        totalClosingCR = parseFloat(totalClosingCR) + parseFloat(data[i].ClosingBalCR);

        var color = ""; 
        if (isSubLedger == 'Y')
        {
            color = "#DFDDD9";
        }
        else
        {
            if (i % 2 == 0) color = "#FDF8EC"; else color = "white";
        }
         //onclick='Populate_1(this)'
        table.find('tbody').append("<tr class='allData' style='cursor:pointer; font-family:Verdana; font-size:12px;  background-color:" + color + "' ><td style='display:none; ' class='AccountCode'>" +
                         data[i].AccountCode + "</td><td style='text-align:left; width:30%' class='AccountDescription'>" +
                         data[i].AccountDescription + "</td><td style='text-align:right;'>" +
                         data[i].OpeningBalDR + "</td><td style='text-align:right;'>" +
                         data[i].OpeningBalCR + "</td><td style='text-align:right;'>" +
                         data[i].TransactionBalDR + "</td><td style='text-align:right;'>" +
                         data[i].TransactionBalCR + "</td><td style='text-align:right;'>" +
                         data[i].ClosingBalDR + "</td><td style='text-align:right;'>" +
                         data[i].ClosingBalCR + "</td><tr>");
     }
     var count = $("#myTable tr.allData").length;
     if (count > 0) {
         table.find('tbody').append("<tr class='footers' style='background-color:#F5F5F6; font-weight:bold'><td style='display:none; '>" +
                     "" + "</td><td style='width:30%'>" +
                     "Total" + "</td><td style='text-align:right; '>" +
                     (totalOpenDR).toFixed(2) + "</td><td style='text-align:right; '>" +
                     (totalOpenCR).toFixed(2) + "</td><td style='text-align:right'>" +
                     (totalTransDR).toFixed(2) + "</td><td style='text-align:right'>" +
                     (totalTransCR).toFixed(2) + "</td><td style='text-align:right'>" +
                     (totalClosingDR).toFixed(2) + "</td><td style='text-align:right'>" +
                     (totalClosingCR).toFixed(2) + "</td><tr>");
     }

    // Highlight_Row();

 } //============================================================================================ 1

 function Populate_Exp_Main_1(AccountCode, AccountDesc, hide_Columns) {   //============================================================================================ 2
     
     var totalDebit = 0; var totalCredit = 0; var totalCloBal = 0;
     var Z = "{AccountCode:'" + AccountCode + "'}";
    
     $.ajax({
         type: "POST",
         url: "/Accounts_Form/TrailBalance/Show_Exp_Main_1",
         data: Z,
         contentType: "application/json; charset=utf-8",
         success: function (D) {
             var data = D.Data; 
            
             if (data.length > 0 && D.Status == 200) {

                 Grid_Level = parseInt(Grid_Level) + 1;
             

                 var table = $("#myTable");
                 table.find('thead').empty();
                 table.find('tbody').empty();
                 table.find('tfoot').empty();

                 $("#lblHeader").html(AccountDesc);
                 Populate_Headers(data, hide_Columns);

                 for (var i = 0; i < data.length; i++) {

                     totalDebit = parseFloat(totalDebit) + parseFloat(data[i].Debit);
                     totalCredit = parseFloat(totalCredit) + parseFloat(data[i].Credit);
                     totalCloBal = parseFloat(totalCloBal) + parseFloat(data[i].ClosingBalance);

                     var color = "";
                     if (i % 2 == 0) color = "#FDF8EC"; else color = "white";
    
                    
                     table.find('tbody').append("<tr class='allData' style='cursor:pointer; font-family:Verdana; font-size:12px;  background-color:" + color + "' ><td style='display:none; ' class='AccountCode'>" +
                                      data[i].AccountCode + "</td><td style='text-align:left; width:30%' class='AccountDescription'>" +
                                      data[i].AccountDescription + "</td><td style='text-align:right;'>" +
                                      data[i].Debit + "</td><td style='text-align:right;'>" +
                                      data[i].Credit + "</td><td style='text-align:right;'>" +
                                      data[i].ClosingBalance + "</td><tr>");
                 }
                 var count = $("#myTable tr.allData").length;
                 if (count > 0) {
                     table.find('tbody').append("<tr class='footers' style='background-color:#F5F5F6; font-weight:bold'><td style='display:none; '>" +
                                 "" + "</td><td style='width:30%'>" +
                                 "Total" + "</td><td style='text-align:right; '>" +
                                 totalDebit + "</td><td style='text-align:right; '>" +
                                 totalCredit + "</td><td style='text-align:right'>" +
                                 totalCloBal + "</td><tr>");
                 }
                 //Highlight_Row();
             }
            
         }
     });
    
 } //================================= this of only for profit and loss ======================================== 2
 function Populate_Exp_Main_2(AccountCode, AccountDesc) //============================================================================================ 3
 {
    
     hide_Columns = [0, 1, 7];
     var totalDebit = 0; var totalCredit = 0; var totalCloBal = 0;

     var SDate = ""; var EDate = "";
     if ($.trim($("#txtFromDate").val()) != "") {
         var refDate = ($("#txtFromDate").val()).split('/');
         SDate = refDate[2] + "-" + refDate[1] + "-" + refDate[0];
     }
     if ($.trim($("#txtToDate").val()) != "") {
         var refDates = ($("#txtToDate").val()).split('/');
         EDate = refDates[2] + "-" + refDates[1] + "-" + refDates[0];
     }
     var Z = "{AccountCode:'" + AccountCode + "', StartDate:'" + SDate + "', EndDate:'" + EDate + "', SectorID:'" + $("#ddlSector").val() + "'}";
  
     $.ajax({
         type: "POST",
         url: "/Accounts_Form/TrailBalance/Show_Exp_Main_2",
         data: Z,
         contentType: "application/json; charset=utf-8",
         success: function (D) {
             var data = D.Data; 
             if (data.length > 0 && D.Status == 200) {
                 
                 ArrayOpenListID.push($('#myDiv').html());

                 Grid_Level = parseInt(Grid_Level) + 1;
                 $("#lblHeader").html(AccountDesc);

                 var table = $("#myTable");
                 table.find('thead').empty();
                 table.find('tbody').empty();
                 table.find('tfoot').empty();

                 Populate_Headers(data, hide_Columns);

               
                 for (var i = 0; i < data.length; i++) {

                     totalDebit = parseFloat(totalDebit) + parseFloat(data[i].TR_DR);
                     totalCredit = parseFloat(totalCredit) + parseFloat(data[i].TR_CR);
                     //totalCloBal = parseFloat(totalCloBal) + parseFloat(data[i].ClosingBalance);

                     var color = "";
                     if (i % 2 == 0) color = "#FDF8EC"; else color = "white";

                     table.find('tbody').append("<tr class='allData' style='cursor:pointer; font-family:Verdana; font-size:12px;  background-color:" + color + "' ><td style='display:none; ' class='AccountCode'>" +
                                      data[i].AccountCode + "</td><td style='text-align:left; display:none; width:30%' class='MONTH_SRL' >" +
                                      data[i].MONTH_SRL + "</td><td style='text-align:center;' class='AccountDescription'>" +
                                      data[i].MONTH_NAME + "</td><td style='text-align:right;' >" +
                                      (data[i].OP_DRCR).toFixed(2) + "</td><td style='text-align:right; ' >" +
                                      (data[i].TR_DR).toFixed(2) + "</td><td style='text-align:right; class='MonthClosBal' ' >" +
                                      (data[i].TR_CR).toFixed(2) + "</td><td style='text-align:right; class='MonthClosBal' ' >" +
                                      (data[i].CL_DRCR).toFixed(2) + "</td><td style='text-align:right; display:none' class='MonthClosBal' >" +
                                      data[i].YearMonth + "</td><tr>");
                 }
                 //var count = $("#myTable tr.allData").length;
                 //if (count > 0) {
                 //    table.find('tbody').append("<tr class='footer' style='background-color:aquamarine; font-weight:bold'><td style='display:none; '>" +
                 //                "" + "</td><td style='width:30%'>" +
                 //                "Total" + "</td><td style='text-align:right; '>" +
                 //                totalDebit + "</td><td style='text-align:right; '>" +
                 //                totalCredit + "</td><td style='text-align:right'>" +
                 //                totalCloBal + "</td><tr>");
                 //}
                 //Highlight_Row();
             }
             //else
             //    Grid_Level = parseInt(Grid_Level) - 1;
         }
     });
 } //============================================================================================ 3
 function Populate_Exp_Main_3(AccountCode, AccountDescription)
 {
    
     hide_Columns = [0, 1, 7];
     var totalDebit = 0; var totalCredit = 0; var totalCloBal = 0;
     var VoucherStartDate = ""; var VoucherEndDate = "";

     var FinYear = $('#txtFromDate').val();
     var t = FinYear.split('/');
     var Year = t[2]; 
    

     var FinYears = $('#txtToDate').val();
     var ts = FinYears.split('/');
     var Years = ts[2]; 

     var x = (Year % 100 === 0) ? (Year % 400 === 0) : (Year % 4 === 0); 
     var y = (Years % 100 === 0) ? (Years % 400 === 0) : (Years % 4 === 0); 

     if (AccountDescription == "JANUARY") {
         VoucherStartDate = Years + '-' + '01-' + '01'; VoucherEndDate = Years + '-' + '01-' + '31';
     }
     if (AccountDescription == "FEBRUARY") {
         if (y == false) {
             VoucherStartDate = Years + '-' + '02-' + '01'; VoucherEndDate = Years + '-' + '02-' + '28';
         }
         else {
             VoucherStartDate = Years + '-' + '02-' + '01'; VoucherEndDate = Years + '-' + '02-' + '29';
         }

     }
     if (AccountDescription == "MARCH") {
         VoucherStartDate = Years + '-' + '03-' + '01'; VoucherEndDate = Years + '-' + '03-' + '31';
     }
     if (AccountDescription == "APRIL") {

         VoucherStartDate = Year + '-' + '04-' + '01'; VoucherEndDate = Year + '-' + '04-' + '30';
     }
     if (AccountDescription == "MAY") {
         VoucherStartDate = Year + '-' + '05-' + '01'; VoucherEndDate = Year + '-' + '05-' + '31';
     }
     if (AccountDescription == "JUNE") {
         VoucherStartDate = Year + '-' + '06-' + '01'; VoucherEndDate = Year + '-' + '06-' + '30';
     }
     if (AccountDescription == "JULY") {
         VoucherStartDate = Year + '-' + '07-' + '01'; VoucherEndDate = Year + '-' + '07-' + '31';
     }
     if (AccountDescription == "AUGUST") {
         VoucherStartDate = Year + '-' + '08-' + '01'; VoucherEndDate = Year + '-' + '08-' + '31';
     }
     if (AccountDescription == "SEPTEMBER") {
         VoucherStartDate = Year + '-' + '09-' + '01'; VoucherEndDate = Year + '-' + '09-' + '30';
     }
     if (AccountDescription == "OCTOBER") {
         VoucherStartDate = Year + '-' + '10-' + '01'; VoucherEndDate = Year + '-' + '10-' + '31';
     }
     if (AccountDescription == "NOVEMBER") {
         VoucherStartDate = Year + '-' + '11-' + '01'; VoucherEndDate = Year + '-' + '11-' + '30';
     }
     if (AccountDescription == "DECEMBER") {
         VoucherStartDate = Year + '-' + '12-' + '01'; VoucherEndDate = Year + '-' + '12-' + '31';
     }

     var Z = "{AccountCode:'" + AccountCode + "', VoucherStartDate:'" + VoucherStartDate + "', VoucherEndDate:'" + VoucherEndDate + "'}"; 

     $.ajax({
         type: "POST",
         url: "/Accounts_Form/TrailBalance/Show_Exp_Main_3",
         data: Z,
         contentType: "application/json; charset=utf-8",
         success: function (D) {
             var data = D.Data; //alert(JSON.stringify(data));

             if (data.length > 0 && D.Status == 200) {
                
                 ArrayOpenListID.push($('#myDiv').html());

                 Grid_Level = parseInt(Grid_Level) + 1;

                 var table = $("#myTable");
                 table.find('thead').empty();
                 table.find('tbody').empty();
                 table.find('tfoot').empty();

                 Populate_Headers(data, hide_Columns);

                 var lblHeader = 'From : ' + VoucherStartDate + '  To : ' + VoucherEndDate;
                 $("#lblHeader").html(lblHeader);

                 for (var i = 0; i < data.length; i++) {

                     Grid_Level = parseInt(Grid_Level) - 1;

                     totalDebit = parseFloat(totalDebit) + parseFloat(data[i].Debit);
                     totalCredit = parseFloat(totalCredit) + parseFloat(data[i].Credit);
                     totalCloBal = parseFloat(totalCloBal) + parseFloat(data[i].ClosingBalance);

                     var color = "";
                     if (i % 2 == 0) color = "#FDF8EC"; else color = "white";

                     table.find('tbody').append("<tr class='allData' style='cursor:pointer; font-family:Verdana; font-size:12px;  background-color:" + color + "'  ><td style='display:none; ' class='AccountCode'>" +
                                      data[i].AccountCode + "</td><td style='text-align:left; display:none;' class='VoucherNumber'>" +
                                      data[i].VoucherNumber + "</td> <td style='text-align:center; width:20%' class='AccountDescription'>" +
                                      data[i].RefVoucherSlNo + "</td> <td style='text-align:center; width:10%' >" +
                                      data[i].VoucherDate + "</td> <td style='text-align:left; width:50%'' >" +
                                      data[i].Narration + "</td> <td style='text-align:right; width:40%' >" +
                                      (data[i].Debit).toFixed(2) + "</td> <td style='text-align:right; width:40%' >" +
                                      (data[i].Credit).toFixed(2) + "</td> <td style='text-align:right; width:40%; display:none;' >" +
                                      (data[i].ClosingBalance).toFixed(2) + "</td></tr>");
                                     // "<td style='text-align:center;'><a target='_blank' href='/Accounts_Form/VoucherMaster/Index/" + data[i].VoucherNumber + "'><img style='cursor:pointer;' src='/Content/Images/Edit.png' alt='cross' height=20 width=20 /></a></td><tr>");

                 }
                 var count = $("#myTable tr.allData").length;
                 if (count > 0) {
                     table.find('tbody').append("<tr class='footers' style='background-color:#F5F5F6; font-weight:bold'><td style='display:none; '>" +
                                 "" + "</td><td style='width:30%; display:none;'>" +
                                 "" + "</td><td style='width:20%'>" +
                                 "" + "</td><td style='width:10%'>" +
                                 "" + "</td><td style='width:50%'>" +
                                 "Total" + "</td><td style='text-align:right; width:40%''>" +
                                 (totalDebit).toFixed(2) + "</td><td style='text-align:right; width:40%''>" +
                                 (totalCredit).toFixed(2) + "</td><td style='text-align:right; width:40%; display:none;'>" +
                                 (totalCloBal).toFixed(2) + "</td><tr>");
                 }
                 //Highlight_Row();
             }
             else
             {
              
                Grid_Level = parseInt(Grid_Level) - 1; 
                 //var lblHeader = 'From : ' + VoucherStartDate + '  To : ' + VoucherEndDate;
                 //$("#lblHeader_3").html(lblHeader);

                 //table.find('tbody').append("<tr><td colspan='7' style='text-align:center; color:red;'>" +
                 //                      'No Data Found !!' + "</td></tr>");
             }
         }
     });
 }

 function SearchTable_0() {
     var forSearchprefix = $("#txtSearch_0").val().trim().toUpperCase();
     var tablerow = $('#myTable').find('.allData');
     $.each(tablerow, function (index, value) {
         var AccountDescription = $(this).find('.AccountDescription').text().toUpperCase();
         if (AccountDescription.indexOf(forSearchprefix) > -1 ) {
             $(this).show();
         } else {
             $(this).hide();
         }
     });
 }

 function MyFunctions(AccountCode, AccountDesc, Grid_Level)
 {
    // alert(Grid_Level);
     if (Grid_Level == 0) {
         $("#lblHeader").html('');
         $("#hdnAccountCode").val(''); $("#hdnAccountDesc").val(''); $("#hdnAccountHidden").val('');
         Populate_Grid($('#txtFromDate').val(), $('#txtToDate').val());
     }
     if (Grid_Level == 1) {
         hide_Columns = [0,1,7];
         $("#hdnAccountCode").val(AccountCode); $("#hdnAccountDesc").val(AccountDesc); $("#hdnAccountHidden").val(hide_Columns);
         Populate_Exp_Main_2(AccountCode, AccountDesc, hide_Columns);
     }
     if (Grid_Level == 2) {
         hide_Columns = [0, 1];
         $("#hdnAccountCode").val(AccountCode); $("#hdnAccountDesc").val(AccountDesc); $("#hdnAccountHidden").val(hide_Columns);
         Populate_Exp_Main_3(AccountCode, AccountDesc, hide_Columns);
     }
     //if (Grid_Level == 3) {
     //    hide_Columns = [0,1];
     //    $("#hdnAccountCode").val(AccountCode); $("#hdnAccountDesc").val(AccountDesc); $("#hdnAccountHidden").val(hide_Columns);
     //    Populate_Exp_Main_3(AccountCode, AccountDesc, hide_Columns);
        
     //}
 }
 function Check_Array(value, hide_list) {

     if (jQuery.inArray(value, hide_list) != '-1') {
         return true;
     } else {
         return false;
     }
 }
 function Populate_Headers(data, hide_Columns)
 {
     var html = "";
     html += "<tr>";

     var table = $("#myTable");

     //populate header
     for (var j = 0; j < (Object.keys(data[0])).length; j++) {
         var Columns = Object.keys(data[0])[j];

         var res = Check_Array(j, hide_Columns);

         var desc = "";
         if (Object.keys(data[0])[j] == "AccountHead")
             desc = "Account Head";
         else if (Object.keys(data[0])[j] == "AccountDescription")
             desc = "Account Description";
         else if (Object.keys(data[0])[j] == "MONTH_NAME")
             desc = "Months";
         else if (Object.keys(data[0])[j] == "OP_DRCR")
             desc = "Opening Balance";
         else if (Object.keys(data[0])[j] == "TR_DR")
             desc = "Debit";
         else if (Object.keys(data[0])[j] == "TR_CR")
             desc = "Credit";
         else if (Object.keys(data[0])[j] == "CL_DRCR")
             desc = "Closing Balance";
         else if (Object.keys(data[0])[j] == "RefVoucherSlNo")
             desc = "Voucher No.";
         else if (Object.keys(data[0])[j] == "VoucherDate")
             desc = "Voucher Date";
         else
             desc = Object.keys(data[0])[j];

         if (res == true) {
             html += "<th data-field-name='" + desc + "' data-col-rank='" + j + "' style='display:none'>" + desc + "</th>";
         }
         else {
             html += "<th data-field-name='" + desc + "' data-col-rank='" + j + "'>" + desc + "</th>";
         }
     }
     html += "</tr>";
     table.find('thead').append(html);
 }
 function Populate_Headers_1(data, hide_Columns) {
     var html = "";

     var table = $("#myTable");

     //populate header
     html += "<tr>";
     html += "<th></th>";
     html += "<th colspan=2 >Opening Balance</th>";
     html += "<th colspan=2 >Transaction Balance</th>";
     html += "<th colspan=2 >Closing Balance</th>";
     html += "</tr>";

     html += "<tr>";
     html += "<th>Account Description</th>";
     html += "<th>Dr.</th>";
     html += "<th>Cr.</th>";
     html += "<th>Dr.</th>";
     html += "<th>Cr.</th>";
     html += "<th>Dr.</th>";
     html += "<th>Cr.</th>";
     html += "</tr>";

     table.find('thead').append(html);
 }
 function Highlight_Row() {

     $("[id*=myTable] tr").not(':first').hover(
        function () { $(this).css("background", "#D4E6F1");},
        function () { $(this).css("background", "");});
 }

 function Print()
 {
     var table = $("#myTable");
     var tblCount = (table.find('tbody tr.allData')).length;
     if (tblCount > 0) {
         var res = confirm('Are You Sure Want to Print Report ??');
         if (res == true) {
             var contents = $("#myDiv").html();
             var frame1 = $('<iframe />');
             frame1[0].name = "frame1";
             frame1.css({ "position": "absolute", "top": "-1000000px" });
             $("body").append(frame1);
             var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
             frameDoc.document.open();
             //Create a new HTML document.
             frameDoc.document.write("<html><head><title></title>");
             frameDoc.document.write('</head><body>');
             //Append the external CSS file.
             frameDoc.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
             //Append the DIV contents.
             frameDoc.document.write("<div style='width:100%'>" +
                 "<div style='width:20%; float:left; text-align:right; padding-bottom:5px;'><img src='" + $("#hdnLogo").val() + "' style='width:300px;height:80px;cursor:pointer;' /></div>");
             frameDoc.document.write("<div style='width:90%; height:80px; text-align:center; '>" +
                 "<span>" + $("#hdnHeader").val() + "</span><br/>" +
                 "<span>" + $("#hdnAddress").val() + "</span></div>" +
                 "<h3 style='font-family:verdana;'>" + (isGLSL == "L" ? ($('#lblHeader').text().trim()) : ($('#txtSearchSubLedger').val().trim())) + "</h3></div>");
            
           
             frameDoc.document.write(contents);
             frameDoc.document.write('</body></html>');
             frameDoc.document.close();
             setTimeout(function () {
                 window.frames["frame1"].focus();
                 window.frames["frame1"].print();
                 frame1.remove();
             }, 500);
         }
     }
     else {
         //
         alert('Sorry ! Please Select Ledger Details.'); return false;
     }
 }
 //**********************************************************************************************************************************
 //                                                                 OTHER
 //**********************************************************************************************************************************

 function Back() {
     if (ArrayOpenListID.length > 0) {
         var t = ArrayOpenListID.pop();
         if (t != "") {
             $('#myDiv').html(t);
         }
     }
 }
 
  function Excel() {

     var fromDate = $("#txtFromDate").val();
     var toDate = $("#txtToDate").val();
     if (fromDate == "")
     {
         $("#txtFromDate").focus(); return false;
     }
     if (toDate == "") {
         $("#txtToDate").focus(); return false;
     }

         var res = confirm('Are You Sure Want to download Excel Report ??');
         if (res == true) {

             var fDate = ""; var tDate = "";
             if ($("#txtFromDate").val() != "") {
                 var a = ($("#txtFromDate").val()).split('/');
                 fDate = a[2] + '-' + a[1] + '-' + a[0];
             }
             if ($("#txtToDate").val() != "") {
                 var b = ($("#txtToDate").val()).split('/');
                 tDate = b[2] + '-' + b[1] + '-' + b[0];
             }

             var master = []; var final = {};

             master.push({
                 FromDate: fDate,
                 ToDate: tDate,
                 SectorID: $("#ddlSector").val(),
                 GLSL: isGLSL,
                 ParentAccountCode: $('#hdnSearchSubLedger').val()
             });
             final = {
                 Master: master
             }

             location.href = "/Accounts_Form/TrailBalance/Excel?ReportName=" + JSON.stringify(final);
         }
 }



 
    
   
    
    
    




