﻿var pathString = "";

$(document).ready(function () {

    Set_PurchaseOrderDate();
    Set_DefaultDate();
    Load();

    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $(".allownumericwithoutdecimal").on("keypress keyup blur, onkeydown", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    //    ITEM AUTOCOMPLETE
    $('#txtExpItemName').autocomplete({
        source: function (request, response) {

            var S = "{Desc:'" + $('#txtExpItemName').val() + "', MND:'" + "I" + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/AccountMaster/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnExpItemCode').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtExpItemName').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtExpItemName").val('');
            $("#hdnExpItemCode").val('');
        }
        if (iKeyCode == 46) {
            $("#txtExpItemName").val('');
            $("#hdnExpItemCode").val('');
        }
    });

    //    VENDOR AUTOCOMPLETE
    $('#txtExpVendorName').autocomplete({
        source: function (request, response) {

            var S = "{Desc:'" + $('#txtExpVendorName').val() + "', MND:'" + "S" + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/AccountMaster/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdntxtExpVendorID').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtExpVendorName').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#hdntxtExpVendorID").val('');
            $("#txtExpVendorName").val('');
        }
        if (iKeyCode == 46) {
            $("#hdntxtExpVendorID").val('');
            $("#txtExpVendorName").val('');
        }
    });

    //    SHIP TO AUTOCOMPLETE
    $('#txtShipTo').autocomplete({
        source: function (request, response) {

            var S = "{Desc:'" + $('#txtShipTo').val() + "', MND:'" + "S" + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/AccountMaster/Account_Description',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnShipTo').val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtShipTo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtShipTo").val('');
            $("#hdnShipTo").val('');
        }
        if (iKeyCode == 46) {
            $("#txtShipTo").val('');
            $("#hdnShipTo").val('');
        }
    });

    $('#btnItemAdd').click(Add_Item);
    $('#btnExpSave').click(Save_PO);
    
});

function Add_Item() {
    if ($('#hdnExpItemCode').val() == "") { $('#txtExpItemName').focus(); return false; }
    if ($('#txtExpItemQty').val() == "" ||  $('#txtExpItemQty').val() == 0) { $('#txtExpItemQty').focus(); return false; }
    if ($('#txtExpItemRate').val() == "" || $('#txtExpItemRate').val() == 0) { $('#txtExpItemRate').focus(); return false; }
    if ($('#txtExpItemAmount').val() == "" || $('#txtExpItemAmount').val() == 0) { $('#txtExpItemAmount').focus(); return false; }
    if ($('#ddlExpenseTax').val() == "" || $('#ddlExpenseTax').val() == 0) { $('#ddlExpenseTax').focus(); return false; }
    //$("#hdnExpenseTax").val($('#ddlExpenseTax').val());

    var chkAccCode = $("#hdnExpItemCode").val();

    var l = $('#tbl_Expense tr.myData').find("td[chk-data='" + chkAccCode + "']").length;

    if (l > 0) {
        alert('Sorry ! This Item is Already Available.'); $("#txtExpItemName").val(''); $("#hdnExpItemCode").val(''); $('#txtExpItemName').focus(); return false;
    }

    var html = ""

    var $this = $('#tbl_Expense .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData'>"
         + '<td colspan="7" chk-data=' + $("#hdnExpItemCode").val() + ' chk-tax=' + $("#ddlExpenseTax").val() + '>'
         + '<div class="forum-item cls_1">'
         + '<div class="row">'
         + '<div class="col-md-5">'
         + '<div class="forum-icon">' 
         + '<i class="fa fa-trash-o del_img"  onclick="Delete(this, \'' + $("#ddlExpenseTax").val() + '\')" style="cursor:pointer; color:red" title="Delete"></i>'
         + '</div>'
         + '<a class="forum-item-title">' + $("#txtExpItemName").val() + '</a>'
         + '<div class="forum-sub-title cls_ExpItemDesc">' + $("#txtExpItemDesc").val() + '</div>'
         + '</div>'

          + '<div class="col-md-1 forum-info" style="display:none">'
         + '<span class="views-number cls_ItemID" >'
         + $("#hdnExpItemCode").val()
         + '</span>'
         + '<div>'
         + '<small>ItemID</small>'
         + '</div>'
         + '</div>'

         + '<div class="col-md-1 forum-info">'
         + '<span class="views-number cls_ExpItemQty">'
         + $("#txtExpItemQty").val()
         + '</span>'
         + '<div>'
         + '<small>Qty</small>'
         + '</div>'
         + '</div>'

         + '<div class="col-md-2 forum-info">'
         + '<span class="views-number cls_ExpItemRate">'
         + $("#txtExpItemRate").val()
         + '</span>'
         + '<div>'
         +'<small>Rate</small>'
         + '</div>'
         + '</div>'

         +'<div class="col-md-2 forum-info">'
         +'<span class="views-number cls_Amount">'
         +  $("#txtExpItemAmount").val()  
         + '</span>'
         +'<div>'
         +'<small>Amount</small>'
         +'</div>'
         + '</div>'

         + '<div class="col-md-2 forum-info" style="display:none">'
         + '<span class="views-number cls_ExpenseTaxAmount" >'
         + $("#hdnExpenseTax").val()
         + '</span>'
         + '<div>'
         + '<small>CalTaxAmount</small>'
         + '</div>'
         + '</div>'

         +'<div class="col-md-2 forum-info">'
         +'<span class="views-number">'
         + $("#ddlExpenseTax option:selected").text()
         +'</span>'
         +'<div>'
         +'<small>Tax</small>'
         +'</div>'
         + '</div>'

         + '<div class="col-md-2 forum-info" style="display:none">'
         + '<span class="views-number cls_ExpenseTax" >'
         + $("#ddlExpenseTax").val()
         + '</span>'
         + '<div>'
         + '<small>TaxID</small>'
         + '</div>'
         + '</div>'

         +'</div>'
         +'</div>'
         +'</td>'
    html + "</tr>";

    $parentTR.after(html);

    Calculate_Tax($("#ddlExpenseTax").val());
    $("#txtExpItemName").val(''); $("#hdnExpItemCode").val(''); $("#txtExpItemDesc").val('');
    $("#txtExpItemQty").val(''); $("#txtExpItemRate").val(''); $("#txtExpItemAmount").val('');
    $("#ddlExpenseTax").val(''); $("#hdnExpenseTax").val('');
    Calculate_Total();
    
}
function Set_DefaultDate() {
    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtExpPurOrderDate').val(output);
}
function Set_PurchaseOrderDate() {
    $('#txtExpPurOrderDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function Delete(ID, TaxID)
{
    $(ID).closest('tr').remove(); Calculate_Total(); Calculate_Tax(TaxID); Calculate_FinalTotal();
}
function Cal_Amount() {
    var qty = $('#txtExpItemQty').val();
    var rate = $('#txtExpItemRate').val();

    var amt = qty * rate;
    $('#txtExpItemAmount').val(amt.toFixed(2));
}
function Calculate_Total() {
    var Total = 0;
    var html = ""

    if ($("#tbl_Expense tbody tr.myData").length > 0) {
        $("#tbl_Expense tbody tr.myData").each(function (index, value) {
            var Amount = $(this).find('.cls_Amount').text();
            Total = parseFloat(Total) + parseFloat(Amount);
            $('#hdnExpGrossAmt').val(Total);
        });
    }

    var $this = $('#tbl_Expense .myData');
    $parentTR = $this.closest('tr');

    if ($('#tbl_Expense .trfooter').length > 0) {
        $('#tbl_Expense .trfooter').find('.clsSubTotal').text((Total).toFixed(2)); return;
    }


    html += "<tr class='trfooter' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='5'>SubTotal : </td>"
        + "<td style='text-align:right; font-weight:bold; width:20%' colspan='2'><span class='clsSubTotal' > &#x20b9; " + (Total).toFixed(2) + "</span></td>"
    html + "</tr>";

    $parentTR.after(html);

}

function Calculate_FinalTotal() {
    var Total = 0;
    var html = ""
   
    if ($("#tbl_SubExpense tbody tr.mysubData").length > 0) {
        $("#tbl_SubExpense tbody tr.mysubData").each(function (index, value) {
            var Amount = $(this).find('.clsTaxNetAmount').text();
            Total = parseFloat(Total) + parseFloat(Amount);
            $('#hdnExpSubGrossAmt').val(Total);
        });
    }
    
    $('#tbl_SubExpense .trfooter').remove();

    html += "<tr class='trfooter' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='1'>Total : </td>"
        + "<td style='text-align:right; font-weight:bold;'><span class='clsSubTotalTax' >&#x20b9; " + (Total).toFixed(2) + "</span></td>"
    html + "</tr>";

    $('#tbl_SubExpense tr:last').after(html)

}
function Calculate_Tax(TaxID)
{
    var Total = 0;
    if (TaxID != "") {

        if ($("#tbl_Expense tbody tr.myData").length > 0) {
            $("#tbl_Expense tbody tr.myData").each(function (index, value) {
                var TaxIDs = $(this).find('.cls_ExpenseTax').text(); 
                if (TaxID == TaxIDs) {
                    var Amount = $(this).find('.cls_Amount').text();
                    Total = parseFloat(Total) + parseFloat(Amount);
                }
            });

            var l = $('#tbl_Expense tr.myData').find("td[chk-tax='" + TaxID + "']").length;
            if (l > 0) {
                var E = "{TaxTypeID: '" + TaxID + "'}";
                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/Item/Load_TaxTypeDetail',
                    data: E,
                    contentType: 'application/json; charset=utf-8',
                    success: function (response) {
                        var data = response.Data;
                        if (response.Status == 200) {
                            for (var i = 0; i < data.length; i++) {
                                var taxPert = data[i].TaxPer;
                                var taxTypeDet = data[i].TaxTypeDet;
                                var taxTypeDetID = data[i].TaxTypeDetId;
                                var calAmount = parseFloat((parseFloat(Total) * parseFloat(taxPert)) / 100);

                                Bind_TAXCal(TaxID, taxTypeDetID, taxTypeDet, Total, calAmount);
                            }
                            Calculate_FinalTotal();
                        }
                    }
                });
               
            }
            else {
                $('#tbl_SubExpense tbody').find("tr[chk-tax='" + TaxID + "']").remove();
            }
        }
        else
        {
            //$("#tbl_SubExpense tbody tr.mysubData").remove();
            $("#tbl_SubExpense tbody tr").remove();
        }
    }
}
function Bind_TAXCal(TaxID, taxTypeDetID, TaxTypeDet, Total, calAmount) {
    
    var html = ""

    var table = $('#tbl_SubExpense tbody');

    var ee = 0;
    $("#tbl_SubExpense tbody tr.mysubData").each(function (index, value) {
        var taxid = $(this).find('.aa').text();
        var taxtypeid = $(this).find('.bb').text();
        if (TaxID == taxid && taxTypeDetID == taxtypeid) {
            $(this).find('.clsTaxDes').text(TaxTypeDet + Total);
            $(this).find('.clsTaxNetAmount').text((calAmount).toFixed(2));
            ee++;
        }
    });
    if (ee > 0)
        return false;
      
    html += "<tr class='mysubData cls_" + TaxID + "'  chk-tax=" + TaxID + ">"
        + "<td style='text-align:right; display:none;' class='aa' >" + TaxID + "</td>"
        + "<td style='text-align:right; display:none;' class='bb'>" + taxTypeDetID + "</td>"
        + "<td style='text-align:right' class='clsTaxDes' >" + (TaxTypeDet + Total) + "</td>"
        + "<td style='text-align:right' class='clsTaxNetAmount' >" + (calAmount).toFixed(2) + "</td>"
    html + "</tr>";

   table.append(html);

   
}
function Blank()
{
    $('.expense').val(''); $('#lblPurchaseOrederNo').text(''); Set_DefaultDate();
    $("#tbl_Expense tbody tr.myData").remove(); $('#tbl_Expense tbody tr.trfooter').remove();
    $("#tbl_SubExpense tbody tr").remove();
    $('#ExpIframe').attr('src', '');
}
function Calculate_TaxAmount(ID)
{
    var taxamount = $('#txtExpItemAmount').val();
    var taxid = $('#ddlExpenseTax').val();
    if(taxid !="")
    {
        var E = "{TaxTypeID: '" + taxid + "'}"; 
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/Item/Load_TaxTypeDetail',
            data: E,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                var data = response.Data;
                if (response.Status == 200 && data.length >0) {
                    var taxPert = data[0].TotalTaxPer;

                    var a = (parseFloat(taxamount) * parseFloat(taxPert)) / 100;
                    $('#hdnExpenseTax').val(a);
                }
            }
        });
    }
}
function Expense_Preview(input) {

    var a = input.files[0].name;
    if (a != "") {
        var validExtensions = ['pdf']; //array of valid extensions
        var fileName = a;
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
        if ($.inArray(fileNameExt, validExtensions) == -1) {
            alert("Invalid file type. Please Select PDF File only.");
            $("#ExpenseflPic").val('');
            return false;
        }
    }

    if (input.files && input.files[0]) {
        path = $('#ExpenseflPic').val().substring(12);
        pathString = '/UploadItemImage/PO/' + path;
    }
}

function Save_PO()
{
    if ($("#hdntxtExpVendorID").val() == "") {
        toastr.error('Please, Select Vendor Name !!', 'Warning')
        $("#txtExpVendorName").focus(); return false;
    }
    if ($("#txtExpPurOrderDate").val() == "") {
        toastr.error('Please, Enter Purchase Order Date !!', 'Warning')
        $("#txtExpPurOrderDate").focus(); return false;
    }

    var master = {};
    master = {
        PoId: $('#hdnPOID').val(),
        VendorCode: $('#hdntxtExpVendorID').val(),    //Vendor Code
        VendEmail: $('#txtEmail').val(),
        PoStatus: 'O',
        ShipAddr: $('#txtShippingAddress').val(),
        ShipTo: $('#hdnShipTo').val(),
        ShipVia: $('#txtShipVia').val(),
        PoNo: $('#lblPurchaseOrederNo').text(),
        PoDate: $('#txtExpPurOrderDate').val(),
        CreditTerms: $('#txtExpCreditTerm').val(),
        TaxType: 'E',
        GrossAmt: $('#hdnExpGrossAmt').val(),
        AdjType: $('#ddlExpAdjType').val(),
        AdjAmt: $('#txtExpAdjAmount').val(),
        RoundOff: $('#txtExpRoundOff').val(),
        NetAmt: $('#txtExpNetAmount').val(),
        Remarks: $('#txtExpRemark').val(),
        DocFilePath: pathString,
        Sectorid: $('#ddlSector').val()
    }

    var grdLen = $('#tbl_Expense tbody tr.myData').length; var ArrList = [];
    if (grdLen > 0) {
        $('#tbl_Expense tbody tr.myData').each(function () {

            var ItemID = $(this).find('.cls_ItemID').text(); 
            var ItemDesc = $(this).find('.cls_ExpItemDesc').text();
            var ItemQty = $(this).find('.cls_ExpItemQty').text();
            var ItemRate = $(this).find('.cls_ExpItemRate').text();
            var ItemAmount = $(this).find('.cls_Amount').text();
            var ItemCalTaxAmount = $(this).find('.cls_ExpenseTaxAmount').text();
            var ItemTaxId = $(this).find('.cls_ExpenseTax').text();

            var NetAmount = parseFloat(ItemAmount) + parseFloat(ItemCalTaxAmount);

            ArrList.push({
                'ItemId': ItemID, 'ProductServiseDesc': ItemDesc, 'Qty': ItemQty, 'Rate': ItemRate, 'GrossAmt': ItemAmount,
                'TaxTypeId': ItemTaxId, 'TaxAmt': ItemCalTaxAmount, 'NetAmt': NetAmount, 'sectorid': $('#ddlSector').val()
            });
        });
    }
    
    var grdsubLen = $('#tbl_SubExpense tbody tr.mysubData').length; var ArrSubList = [];
    if (grdsubLen > 0) {
        $('#tbl_SubExpense tbody tr.mysubData').each(function () {

            var TaxID = $(this).find('.aa').text();
            var taxTypeDetID = $(this).find('.bb').text();
            var TaxDes = $(this).find('.clsTaxDes').text();
            var TaxNetAmount = $(this).find('.clsTaxNetAmount').text();

            ArrSubList.push({
                'TaxID': TaxID, 'taxTypeDetID': taxTypeDetID, 'TaxDes': TaxDes, 'TaxNetAmount': TaxNetAmount, 'GrossAmt': $('#hdnExpSubGrossAmt').val()
            });
        });
    }

    var ItemDesc = JSON.stringify(ArrList);
    var ItemSubDesc = JSON.stringify(ArrSubList);
    var E = "{POMaster: " + JSON.stringify(master) + ", PODetail: " + ItemDesc + ", POSub: " + ItemSubDesc + "}"; 

   
    $.ajax({
        url: '/Accounts_Form/Item/PO_InsertUpdate',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: E,
        dataType: 'json',
        success: function (data) {
            var t = data.Data;
            if (data.Status == 200 && t != "") {
                Upload('ExpenseflPic', t);

                swal({
                    title: "Success",
                    text: 'PO Details are Saved Successfully !!',
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        location.reload();
                        //Load_Item();
                        return false;
                    }
                });
            }
        }
    });

}
function Upload(flPic, PONO) {

    var formData = new FormData();
    var totalFiles = document.getElementById(flPic).files.length;
    for (var i = 0; i < totalFiles; i++) {
        var file = document.getElementById(flPic).files[i];
        fileName = $('#ExpenseflPic').val().substring(12);
        var fileNameExt = fileName.substr(fileName.lastIndexOf('.'));
        formData.append(flPic, file, PONO + fileNameExt);
    }

    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/UploadPicture_Expense',
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        async: false,
        success: function (response) {
        }
    });
}

function Load()
{
    var E = "{POID: '" + "" + "', TransactionType: '" + "Select" + "'}"; 
   
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_PO',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {

            var tt = data.Data; //alert(JSON.stringify(t));
            var t = tt["Table"]; 

            var table = $("#tbl_ExpenseMain");
            table.find('tbody').empty();
            table.find('tfoot').empty();

            var color = '';

            for (var i = 0; i < t.length; i++) {

                if (i % 2 == 0) color = "#FDF8EC"; else color = "white";
                table.find('tbody').append("<tr class='allData' style='font-family:Verdana; font-size:12px;  background-color:" + color + "' ><td style='display:none; ' class='PoId'>" +
                     t[i].PoId + "</td><td style='text-align:center; vertical-align:middle !important;' class='PoNo'>" +
                     t[i].PoNo + "</td><td style='text-align:center; vertical-align:middle !important;' class='PoDate'>" +
                     t[i].PoDate + "</td><td style='text-align:left; vertical-align:middle !important;' class='VendorName'>" +
                     t[i].VendorName + "</td><td style='text-align:right; vertical-align:middle !important;' class='GrossAmt'>" +
                     (t[i].GrossAmt).toFixed(2) + "</td><td style='text-align:center; width:3%' class='TransType'>" +
                     t[i].TransType + "</td><td style='text-align:center; vertical-align:middle !important;'>"
                     + "<select id='po_" + t[i].PoId + "' class='cls_Action form-control' onchange='Execute_Action_" + t[i].TransType + "(this)' style='color:blue;'>"
                     + "<option value=''>Action</option>"
                     + "<option value='B'>Copy to Bill</option>"
                     + "<option value='P'>Print</option>"
                     + "<option value='E'>View/Edit</option>"
                     + "<option value='C'>Copy</option>"
                     + "<option value='D'>Delete</option>"
                     + "</select></td>"
                     + "<tr>");
            }
        }
    });
}
function Execute_Action_P(ID)
{
   
    var POID = $(ID).closest('tr').find('.PoId').text(); 
    var ActionName = $(ID).closest('tr').find('.cls_Action').val();
    var TransType = $(ID).closest('tr').find('.TransType').text(); 
    $('.cls_Action').val('');

   
    if(ActionName != "")
    {
        if (ActionName == "E")  //Edit
            Action_Edit(POID, TransType);
        if (ActionName == "D")  //Delete
            Action_Delete(POID, TransType);
        
    }

    $(ID).closest('tr').find('.cls_Action').val(ActionName);
}
function Populate_Expense(detail)
{
   
    if (detail.length > 0) {
       
        $('#tbl_Expense tbody tr.myData').remove();
        $('#tbl_Expense tbody tr.trfooter').remove();

        if (detail.length > 0) {
            var html = ""

            var $this = $('#tbl_Expense .test_0');
            $parentTR = $this.closest('tr');
            var totalGross = 0;

          
            for (var i = 0 ; i < detail.length; i++) {

                html += "<tr class='myData'>"
                     + '<td colspan="7" chk-data=' + detail[i].ItemId + ' chk-tax=' + detail[i].TaxTypeId + '>'
                     + '<div class="forum-item cls_1">'
                     + '<div class="row">'
                     + '<div class="col-md-5">'
                     + '<div class="forum-icon">'
                     + '<i class="fa fa-trash-o del_img"  onclick="Delete(this, \'' + detail[i].TaxTypeId + '\')" style="cursor:pointer; color:red" title="Delete"></i>'
                     + '</div>'
                     + '<a class="forum-item-title">' + detail[i].ItemDescription + '</a>'
                     + '<div class="forum-sub-title cls_ExpItemDesc">' + detail[i].ProductServiseDesc + '</div>'
                     + '</div>'

                      + '<div class="col-md-1 forum-info" style="display:none">'
                     + '<span class="views-number cls_ItemID" >'
                     + detail[i].ItemId
                     + '</span>'
                     + '<div>'
                     + '<small>ItemID</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-1 forum-info">'
                     + '<span class="views-number cls_ExpItemQty">'
                     + detail[i].Qty
                     + '</span>'
                     + '<div>'
                     + '<small>Qty</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-2 forum-info">'
                     + '<span class="views-number cls_ExpItemRate">'
                     + detail[i].Rate
                     + '</span>'
                     + '<div>'
                     + '<small>Rate</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-2 forum-info">'
                     + '<span class="views-number cls_Amount">'
                     + detail[i].GrossAmt
                     + '</span>'
                     + '<div>'
                     + '<small>Amount</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-2 forum-info" style="display:none">'
                     + '<span class="views-number cls_ExpenseTaxAmount" >'
                     + detail[i].TaxAmt
                     + '</span>'
                     + '<div>'
                     + '<small>CalTaxAmount</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-2 forum-info">'
                     + '<span class="views-number">'
                     + detail[i].TaxTypeDesc
                     + '</span>'
                     + '<div>'
                     + '<small>Tax</small>'
                     + '</div>'
                     + '</div>'

                     + '<div class="col-md-2 forum-info" style="display:none">'
                     + '<span class="views-number cls_ExpenseTax" >'
                     + detail[i].TaxTypeId
                     + '</span>'
                     + '<div>'
                     + '<small>TaxID</small>'
                     + '</div>'
                     + '</div>'

                     + '</div>'
                     + '</div>'
                     + '</td>'
                html + "</tr>";

                totalGross = parseFloat(totalGross) + parseFloat(detail[i].TaxAmt);

            }

            html += "<tr class='trfooter' style='background-color:#F5F5F6' >"
               + "<td style='text-align:right; font-weight:bold;' colspan='5'>SubTotal : </td>"
               + "<td style='text-align:right; font-weight:bold; width:20%' colspan='2'><span class='clsSubTotal' > &#x20b9; " + (parseFloat($('#hdnExpGrossAmt').val()).toFixed(2)) + "</span></td>"
            html + "</tr>";

            $parentTR.after(html);
          
        }
    }
}
function Populate_SubExpense(detail) {

    if (detail.length > 0) {
       
        $('#tbl_SubExpense tbody tr.mysubData').remove();
        $('#tbl_SubExpense tbody tr.trfooter').remove();

        var html = "";  var totalGross = 0;

        var table = $('#tbl_SubExpense tbody');

        for (var i = 0 ; i < detail.length; i++) {
            html += "<tr class='mysubData cls_" + detail[i].TaxID + "'  chk-tax=" + detail[i].TaxID + ">"
                + "<td style='text-align:right; display:none;' class='aa' >" + detail[i].TaxID + "</td>"
                + "<td style='text-align:right; display:none;' class='bb'>" + detail[i].taxTypeDetID + "</td>"
                + "<td style='text-align:right' class='clsTaxDes' >" + detail[i].TaxDes + "</td>"
                + "<td style='text-align:right' class='clsTaxNetAmount' >" + detail[i].TaxNetAmount + "</td>"
            html + "</tr>";

            totalGross = parseFloat(totalGross) + parseFloat(detail[i].TaxNetAmount);
        }
        html += "<tr class='trfooter' style='background-color:#F5F5F6' >"
        + "<td style='text-align:right; font-weight:bold;' colspan='1'>Total : </td>"
        + "<td style='text-align:right; font-weight:bold;'><span class='clsSubTotalTax' >&#x20b9; " + (totalGross).toFixed(2) + "</span></td>"
        html + "</tr>";

        table.append(html);
        $('#hdnExpSubGrossAmt').val(totalGross);
    }
}
function SearchTable() {
    var forSearchprefix = $("#searchInput").val().trim().toUpperCase();
    var tablerow = $('#tbl_ExpenseMain').find('.allData');
    $.each(tablerow, function (index, value) {
        var PoNo = $(this).find('.PoNo').text().toUpperCase();
        var PoDate = $(this).find('.PoDate').text().toUpperCase();
        var VendorName = $(this).find('.VendorName').text().toUpperCase();
        var GrossAmt = $(this).find('.GrossAmt').text().toUpperCase();

        if (PoNo.indexOf(forSearchprefix) > -1 || PoDate.indexOf(forSearchprefix) > -1 || VendorName.indexOf(forSearchprefix) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}

function Action_Edit(POID, TransType)
{
    var E = "{POID: '" + POID + "', TransactionType: '" + "Select" + "', TransType: '" + TransType + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_PO',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var tt = data.Data; //alert(JSON.stringify(t));

            var t1 = tt["Table"];
            var t2 = tt["Table1"];
            var t3 = tt["Table2"];

            $('#lblPurchaseOrederNo').text(t1[0].PoNo);
            $('#txtExpVendorName').val(t1[0].VendorName); $('#hdntxtExpVendorID').val(t1[0].VendorCode); $('#txtEmail').val(t1[0].VendEmail);
            $('#txtMailingAddress').val(t1[0].Address); $('#txtShippingAddress').val(t1[0].ShipAddr); $('#txtShipTo').val(t1[0].ShipToName); $('#hdnShipTo').val(t1[0].ShipTo);
            $('#txtShipVia').val(t1[0].ShipVia); $('#txtExpPurOrderDate').val(t1[0].PoDate); $('#txtExpCreditTerm').val(t1[0].CreditTerms);
            $('#txtExpRemark').val(t1[0].Remarks);
            $('#ddlExpAdjType').val(t1[0].AdjType); $('#txtExpAdjAmount').val(t1[0].AdjAmt); $('#txtExpRoundOff').val(t1[0].RoundOff); $('#txtExpNetAmount').val(t1[0].NetAmt);

            $('#hdnExpGrossAmt').val(t1[0].GrossAmt);
            $('#hdnPOID').val(POID);

            $('#ExpIframe').attr('src', '');
            $('#ExpIframe').attr('src',  t1[0].DocFilePath);

            Populate_Expense(t2);
            Populate_SubExpense(t3);

            $('.po').toggleClass('toggled');
        }
    });
}
function  Action_Delete(POID)
{
        swal({
            title: "Warning",
            text: "Are You Sure want to Delete this Purchase Order ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                var E = "{POID: '" + POID + "', TransactionType: '" + "Delete" + "'}";
                $.ajax({
                    type: "POST",
                    url: '/Accounts_Form/Item/Load_PO',
                    contentType: "application/json; charset=utf-8",
                    data: E,
                    dataType: "json",
                    success: function (data, status) {
                        var t = data.Data; 
                        var table = t['Table']; 
                        var result = table[0].Result;
                        if (data.Status == 200 && result == "success")
                        {
                            swal("Deleted!", "Your Purchase Order has been deleted successfully.", "success");
                            Load();
                        }
                        else
                            swal("Cancelled", "There is Some Problem. Your Purchase Order is Not Deleted.", "error");
                    }
                });
            } else {
                swal("Cancelled", "Your Purchase Order is Safe.", "error");
            }
        });
}