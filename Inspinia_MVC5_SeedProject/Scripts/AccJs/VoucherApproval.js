﻿var minDATE = "", maxDATE = "";

$(document).ready(function () {

   


    var frm = $('#frmApproval');
    appVoucher.validation();
    appVoucher.currentdate();
    appVoucher.approveBy();
    appVoucher.getVoucherType();
    $('#btnsearch').click(function () {
        if (!frm.valid()) {
            return false;
        } else {
            appVoucher.getVoucherApproval();
        }
        
    });

    $(document).on('click', '.btnShow', function () {
        appVoucher.ShowDetail(this);
    })

    $(document).on('click', '#btnEdit', function () {
        $(this).prop('href', '/Accounts_Form/VoucherMaster/Index/' + appVoucher.Voucher); 
        $(this).prop('target','_blank')
    })

    $('#btncancel').click(function () {
        appVoucher.reset();
    })

    $('#ddlGlobalFinYear').change(function () {
        Set_DateBetween_FinYear();
    });

    Set_DateBetween_FinYear();
   
});


var appVoucher = {
    Voucher:'',
    getVoucherType: function () {
        $.ajax({
            url: '/Accounts_Form/VoucherApproval/GetAllVoucherType',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (response) {
                appVoucher.popVoucher(response.Data)
                //console.log(response.Data);
            },
            error: function () { }
        });
    },
    popVoucher: function (data) {
        var ctlr_ddlVoucher = $('#ddlVoucherType');
        ctlr_ddlVoucher.empty();
        ctlr_ddlVoucher.append('<option value=0>Select</option');
        $(data).each(function (index, item) {
            //ctlr_ddlYear.append('<option value=' + item.Year + '>' + item.YearDesc + '</option>');
            ctlr_ddlVoucher.append('<option value=' + item.VoucherTypeID + '>' + item.VoucherDescription + '</option>');
        });
       
    },

    getVoucherApproval: function () {
        var VCH_TYPE = $('#ddlVoucherType').val();
        var fromDate = $('#dtFrom').val();
        var toDate=$('#dtTo').val();
        var vaapSearch = { "VCH_NO": '', "VCH_TYPE": VCH_TYPE, "fromDate": fromDate, "toDate": toDate}
        $.ajax({
            url: '/Accounts_Form/VoucherApproval/GetAllVoucherApproval',
            type: 'POST',
            data: JSON.stringify(vaapSearch),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {
                appVoucher.popVoucherApproval(data.Data);
                console.log(data.Data);
            },
            error: function () { }
        });
    },
    popVoucherApproval: function (data) {
        $('#tblVoucherApproval tbody').empty();

        $.each(data, function (index, element) {
            $('#tblVoucherApproval tbody').append("<tr style=width:100 %; font-family:Verdana;>"
                + "<td class='vId' style='padding-top:4px;display:none;'>" + element.VoucherNumber+"</td>"
                + "<td style='text-align:center; padding-top:4px;'>" + element.VCH_DATE + "</td>"
                + "<td style='text-align:center; padding-top:4px;'>" + element.RefVoucherSlNo + "</td>"
                + "<td style='text-align:right;'>" + (element.VCH_AMOUNT).toFixed(2) + "</td>"
                + "<td style='text-align:center;'>"
                + "<div class='checkbox checkbox-info checkbox-inline' >" 
                + "<input type='checkbox' value='option2' class='chk'  id='chk_" + element.VoucherNumber + "' onClick='appVoucher.approve(this)'><label for='chk'> </label>"
                + "</div></td>"
                + "<td style='text-align:center;'><button type='button' class='btnShow' id='btn_" + element.VoucherNumber +"'>Detail</button></td>"
            );
        });
    },
    approve: function (ID)
    {
        $(ID).attr("checked", true);
        swal({
            title: "Warning",
            text: "Are You Sure want to Approve this Voucher ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    appVoucher.update(ID);
                } else {
                    swal("Cancelled", "Your Voucher is Safe.", "error");
                    $(ID).removeAttr("checked");
                }
            });
    },
    update: function (ref) {
        var voucherId = $(ref).closest('tr').find('.vId').html();
        var vaapSearch = { "VCH_NO": voucherId}
        $.ajax({
            url: '/Accounts_Form/VoucherApproval/UpdateVoucherApproval',
            type: 'POST',
            data: JSON.stringify(vaapSearch),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (D) {
                var t = D.Data;

                    swal({
                        title: "Success",
                        text: D.Message,
                        type: "success",
                        confirmButtonColor: "#AEDEF4",
                        confirmButtonText: "OK",
                        closeOnConfirm: true,
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                appVoucher.getVoucherApproval();
                            }
                        });
            },
            error: function () { }
        });


    },
    ShowDetail: function (ref) {
        var VoucherNumber = $(ref).closest('tr').find('.vId').html();
        appVoucher.Voucher = VoucherNumber;
        var vDetails = { "VCH_NO": VoucherNumber}
        $.ajax({
            url: '/Accounts_Form/VoucherApproval/GetAllVoucherApprovalDetails',
            type: 'POST',
            data: JSON.stringify(vDetails),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {
                //appVoucher.popVoucherApproval(data.Data);
                console.log(data.Data);
                appVoucher.popup(data.Data);
            },
            error: function () { }
        });


    },
    popup: function (data) {
        $("#tbl tr.trclass").empty();
        $.each(data, function (index, value) {
            $("#tbl").append("<tr class='trclass'><td  style='text-align:center; display:none;' >" +   // style='display:none;'
                value.VoucherNumber + "</td><td style='text-align:center; display:none;'>" +
                value.VoucherSerial + "</td><td style='text-align:center; display:none;'>" +
                value.AccountCode + "</td><td style='text-align:center;'>" +
                value.AccountDescription + "</td><td style='text-align:center;'>" +
                value.DRCR + "</td><td style='text-align:center;'>" +
                value.Amount + "</td></tr>");
        });
        appVoucher.OpenPopUpGrid();
    },
    OpenPopUpGrid: function () {
        //jQuery.noConflict();
        $('#Div1').modal('show');
    },
    currentdate: function () {
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        $('#txtapprovedon').val(output);
    },
    approveBy: function () {
        $.ajax({
            url: '/Accounts_Form/VoucherApproval/getSessionData',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success: function (data) {

                $('#txtapprovedby').val(data.Data);
            },
            error: function () { }
        });

       

        
    },
    reset: function () {
        $('#ddlVoucherType').val('0');
        $('#dtFrom').val('');
        $('#dtTo').val('');
        //$('#tblVoucherApproval tbody').empty();
    },
    validation: function () {
        jQuery.validator.addMethod("validDate", function (value, element) {
            //return this.optional(element) || moment(value, "DD/MM/YYYY").isValid();
            return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
        });

        $.validator.addMethod('ddlVoucherType', function (value, element) {
            //return parseInt(value) % 5 == 0
            //return value != '(G)NA(0)' || value != 'NA'
            return value != "0";
        });

        $('#frmApproval').validate({
            rules: {
                ddlVoucherType: {
                    ddlVoucherType: true
                },
                dtFrom: {
                    required: true,
                    validDate:true
                },
                dtTo: {
                    required: true,
                    validDate: true
                }
            },
            messages: {
                ddlVoucherType: {
                    ddlVoucherType: "This field is required."
                },
                dtFrom: {
                    required: "Please Enter From date",
                    validDate: "Please enter a valid date in the format DD/MM/YYYY"
                },
                dtTo: {
                    required: "Please Enter To date",
                    validDate:"Please enter a valid date in the format DD/MM/YYYY"
                }

            }
        });
    }

   
}


function Set_DateBetween_FinYear() {

    var finyear = $("#ddlGlobalFinYear").val();
    var FinYear = $("#ddlGlobalFinYear option:selected").text();

    if (finyear == "O") {
        var dateFrom = "", dateTo = "";
        if (FinYear != "") {
            var sFinYear = FinYear.split('-');
            var s = sFinYear[0];
            var t = sFinYear[1];

            minDATE = "01/04/" + s;
            maxDATE = "31/03/" + t;
        }
        //$('.clsFinDate').prop('readonly', false);
        $('.clsFinDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow',
            dateFormat: 'dd/mm/yy',
            minDate: minDATE,
            maxDate: maxDATE
        });
    }
    else {
        $('.clsFinDate').datepicker('destroy');
        $('.clsFinDate').prop('readonly', true);
    }
}