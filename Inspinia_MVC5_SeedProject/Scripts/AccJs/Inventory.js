﻿$(document).ready(function () {
   
    var frm = $('#frmInv');
    Inventory.validation();
    Inventory.stockdate();
    Inventory.accountDesAutoComplete();
    

    $('#btnsearch').click(function () {
       
        if (!frm.valid()) {
            return false;
        } else {
            
            Inventory.getAllInvDetails();
        }

       
    });

    $('#btnAdd').click(function () {
         Inventory.Buttonval = $(this).attr('data-attrval');
        if (!frm.valid()) {
            return false;
        } else {
            Inventory.save(function (res) {
                Inventory.getAllInvDetails();
                Inventory.reset();
            });
        }
    });

    $(document).on('click', '#btnedit', function () {
        Inventory.rowEdit(this);
    });

    $(document).on('click', '#btnDel', function () {
        Inventory.rowDel(this, function (response) {
            Inventory.getAllInvDetails();
        });
    });
});


var Inventory = {
    Id: '',
    Buttonval:'',
    stockdate: function () {
        $('#dtStockDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow',
            dateFormat: 'dd/mm/yy'
        });
        
        
    },

    accountDesAutoComplete: function () {

        var obj = {};
        obj.AccountDescription = $('#txtAccountDesc').val();
        obj.SectorID = $('#ddlSector').val();

        $('#txtAccountDesc').autocomplete({

            source: function (request, response) {
                //var V = "{ AccountDescription :'" + $('#' + ref).val() + "', SectorID: '" + $('#ddlSector').val() + "'}";
                $.ajax({
                    url: '/Accounts_Form/Inventory/AccontHeadAuto',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(obj),
                    dataType: 'json',
                    success: function (serverResponse) {
                        var SubledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {
                            $.each(serverResponse.Data, function (index, item) {
                                SubledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });
                            //console.log(SubledgerAutoComplete);
                            response(SubledgerAutoComplete);
                        }
                    },
                    error: function () { }
                });
            },
            select: function (e, i) {
                $("#hdnAuto").val(i.item.AccountCode);
            },
            minLength: 0
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });
        
    },
    getFormdata: function () {
        var ctrl_InvDate = $("#dtStockDate");
        var ctrl_txtAccountDesc = $('#txtAccountDesc');
        var ctrl_hdnAuto = $('#hdnAuto');
        var ctrl_txtAmount = $('#txtAmount');
        

        return {
            Id: Inventory.Id,
            InvDate: ctrl_InvDate.val(),
            AccountDescription: ctrl_txtAccountDesc.val(),
            AccountCode: ctrl_hdnAuto.val(),
            Amount: ctrl_txtAmount.val()
        }
    },
    save: function (callback) {
        //var obj = {};
        //obj.Id = '0';
        //obj.AccountDescription = $('#txtAccountDesc').val();
        //obj.AccountCode = $('#hdnAuto').val();
        //obj.Amount = $('#txtAmount').val();
        //obj.InvDate = $('#dtStockDate').val();

        $.ajax({
            url: '/Accounts_Form/Inventory/InsUpdInventorydetails',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(Inventory.getFormdata()),
            dataType: "json",
            success: function (response) {
                alert(response.Message);
                if (callback) {
                    callback(response);
                }
            },
            error: function () { }
        });


    },
    rowEdit: function (ref) {
        
        var ctrl_InvDate = $("#dtStockDate");
        var ctrl_hdnAccountCode = $('#hdnAuto');
        var ctrl_txtAccountDesc = $('#txtAccountDesc');
        var ctrl_txtAmount = $('#txtAmount');
      

        //app.MunicipalityID = $(ref).closest('tr').find('.MunicipalityID').html();
        Inventory.Id = $(ref).closest('tr').find('.Id').html();

        ctrl_InvDate.val($(ref).closest('tr').find('.InvDate').html());
        ctrl_txtAccountDesc.val($(ref).closest('tr').find('.AccountDescription').html());
        //var dectext = $(ref).closest('tr').find('.AccountDescription').html();
        //alert(dectext);
        //$("#txtAccountDesc").val(dectext);
        ctrl_hdnAccountCode.val($(ref).closest('tr').find('.AccountCode').html());
        ctrl_txtAmount.val($(ref).closest('tr').find('.Amount').html());

        

        $('#btnAdd').val('Update');

        //var obj = { "MunicipalityID": MunicipalityID, "YearId": YearId }

        //$.ajax({
        //    url: '/Municipality/BlockPeriod/GetAllBlockPeriodById',
        //    type: 'POST',
        //    contentType: 'application/json; charset=utf-8',
        //    data: JSON.stringify(obj),
        //    dataType: 'json',
        //    success: function (response) {

        //    },
        //    error: function () { }
        //});

    },
    rowDel: function (ref,callback) {
        var obj = {};
        
        Inventory.Id = $(ref).closest('tr').find('.Id').html();
        obj.Id = Inventory.Id;
        $.ajax({
            url: '/Accounts_Form/Inventory/DeleteInvDetailsById',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(obj),
            dataType: 'json',
            success: function (response) {
                alert(response.Message);
                if (callback) {
                    callback(response);
                }
            },
            error: function () { }
        });

    },

    getAllInvDetails: function () {
        var obj = {};
        obj.stockdate = $('#dtStockDate').val();

        $.ajax({
            url: '/Accounts_Form/Inventory/GetAllInventoryDetails',
            type: 'POST',
            contentType:'application/json; charset=utf-8',
            data: JSON.stringify(obj),
            dataType: 'json',
            success: function (response) {
                Inventory.popAllInvDetails(response.Data);
            },
            error: function () { }
        });


    },
    popAllInvDetails: function (data) {
      
        var DataHtml = [];
        var sum = 0;
        $(data).each(function (i, element) {
            DataHtml.push('<tr>');
            DataHtml.push('<td class="Id" style=display:none;>' + element.Id + '</td>');
            DataHtml.push('<td class="InvDate">' + element.InvDate + '</td>');
            DataHtml.push('<td class="AccountDescription">' + element.AccountDescription + '</td>');
            DataHtml.push('<td class="AccountCode" style=display:none;>' + element.AccountCode + '</td>');
            DataHtml.push('<td class="Amount">' + element.Amount + '</td>');
            DataHtml.push('<td><input type="button" id="btnedit" class="ibtnDel btn btn-primary"  value="Edit"></td>');
            DataHtml.push('<td><input type="button" id="btnDel" class="ibtnDel btn btn-md btn-danger"  value="Delete"></td>');
            DataHtml.push('</tr>');
            sum +=element.Amount;
        });
        $('#totalval').text(sum);
        //$('#tbodyBeltSheet').append(DataHtml.join(''));
        $('#tbodyInvDetails').html(DataHtml.join(''));
    },
    validation: function () {
        jQuery.validator.addMethod("validDate", function (value, element) {
            //return this.optional(element) || moment(value, "DD/MM/YYYY").isValid();
            return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
        });

       

        $('#frmInv').validate({
            rules: {
               
                dtStockDate: {
                    required: true,
                    validDate: function () {
                        if (Inventory.Buttonval == "A") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtAccountDesc: {
                    required: function () {
                        if (Inventory.Buttonval == "A") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                txtAmount: {
                    required: function () {
                        if (Inventory.Buttonval == "A") {
                            return true;
                        } else {
                            return false
                        }
                    }
                }
                
                
            },
            messages: {
                
                dtStockDate: {
                    required: "Please Enter From date",
                    validDate: "Please enter a valid date in the format DD/MM/YYYY"
                },
                txtAccountDesc: {
                    required: "Please Select Inventory Description",
                },
                txtAmount: {
                    required: "Please Enter Amount",
                }
            }
        });
    },
    reset: function () {
        $('#btnAdd').val('Add');
        $('#txtAccountDesc').val('');
        $('#hdnAuto').val('');
        $('#txtAmount').val('');
        Inventory.Id = '';
    }


}