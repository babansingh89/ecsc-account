﻿var minDATE = "", maxDATE = "";

$(document).ready(function () {
    
    Set_DateBetween_FinYear();
    Bind_Form();

    $('#ddlGlobalFinYear').change(function () {
        Set_DateBetween_FinYear();
    });

});

function Bind_Form()
{
    $.ajax({
        type: "POST",
        url: "/Accounts_Form/SetAccFinYear/Bind_Form",
        data: {},
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (D) {
            var t = D.Data; 
            if (t.length > 0)
            {
                var html = "";
                $('#tbl tbody tr.trClass').remove();

                for (var i = 0; i < t.length; i++)
                {
                    html += "<tr class='trClass'>";
                    html += "<td style='display:none;' class='clsMenuID'>" + t[i].MenuID + "</td>";
                    html += "<td>" + t[i].MenuName + "</td>";

                    html += "<td><input id='txtFinDate_" + t[i].MenuID + "' class='clsFinDate form-control' readonly autocomplete='off' type='text'  value='" + t[i].Acc_OpenDate + "'  placeholder='DD/MM/YYYY'  />" +   //onblur='Save(this)'
                        "<script type='text/javascript'> $('#txtFinDate_" + t[i].MenuID + "').datepicker({" +
                        "showOtherMonths: true," +
                        "selectOtherMonths: true," +
                        "closeText: 'X'," +
                        "showAnim: 'drop'," +
                        "changeYear: true," +
                        "changeMonth: true," +
                        "duration: 'slow'," +
                        "dateFormat: 'dd/mm/yy'," +
                        "minDate: '" + minDATE + "', " +
                        "maxDate: '" + maxDATE + "', " +
                        "onSelect: function () {" +
                         "Save(this);}" +
                        "});</script></td>";


                    html += "</tr>";
                }
                $('#tbl tbody tr.trClass').remove();
                $('#tbl tbody').append(html);

            }
        }
    });
}
function Save(ID)
{
    var accDate = $(ID).closest('tr').find('.clsFinDate').val(); 

        var menuID = $(ID).closest('tr').find('.clsMenuID').text();

        var Z = "{accDate:'" + accDate + "', menuID:'" + menuID + "'}";
       // return false;
        $.ajax({
            type: "POST",
            url: "/Accounts_Form/SetAccFinYear/Save",
            data: Z,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (D) {
                var t = D.Data;
                if (t == 1) {
                    $(ID).closest('tr').find('.clsFinDate').css('background-color', '#DDFEDD');
                }
                else {
                    $(ID).closest('tr').find('.clsFinDate').css('background-color', '#FDEEEA');
                }
            }
        });

}

function CheckDate(dateCheck)
{
    var finyear = $("#ddlGlobalFinYear").val();
    if (finyear != "")
    {
        var sFinYear = finyear.split('-');
        var s = sFinYear[0];
        var t = sFinYear[1];

        var dateFrom = "01/04/" + s;
        var dateTo = "31/03/" + t;

        var d1 = dateFrom.split("/");
        var d2 = dateTo.split("/");
        var c = dateCheck.split("/");

        var from = new Date(d1[2], parseInt(d1[1]) - 1, d1[0]);  // -1 because months are from 0 to 11
        var to = new Date(d2[2], parseInt(d2[1]) - 1, d2[0]);
        var check = new Date(c[2], parseInt(c[1]) - 1, c[0]);

        if (check > from && check < to)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
function Set_DateBetween_FinYear()
{
    var finyear = $("#ddlGlobalFinYear").val();
    var FinYear = $("#ddlGlobalFinYear option:selected").text();

    if (finyear == "O") {
        var dateFrom = "", dateTo = "";
        if (FinYear != "") {
            var sFinYear = FinYear.split('-');
            var s = sFinYear[0];
            var t = sFinYear[1];

            minDATE = "01/04/" + s;
            maxDATE = "31/03/" + t;
        }
        $('.clsFinDate').datepicker('destroy');
        $('.clsFinDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow',
            dateFormat: 'dd/mm/yy',
            minDate: minDATE,
            maxDate: maxDATE,
            onSelect: function () {
                Save(this);
            }
        });
    }
    else
    {
        $('.clsFinDate').datepicker('destroy');
    }
}


