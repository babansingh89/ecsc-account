﻿$(function () {
    $("#ddlVoucherType").change(function () {
        if ($('#ddlVoucherType').val() == 0) {
            $("#tblVoucheropening tbody").empty(); $("#txtAutoComp").val('');
            $('#ddlVoucherType').focus();
            $('#txtLedger').hide();
            $('#txtLedger').val(""); return false;

         
        }
        else if ($('#ddlVoucherType').val() == "S") {
            $("#tblVoucheropening tbody").empty();
            $('#txtLedger').show(); $('#txtLedger').focus(); return false;
       }
       else {
           $('#txtLedger').hide();
           $('#txtLedger').val("");
           $("#txtAutoComp").val('');
           voucher.Populate();
       }
       
       
    });

    $('#txtLedger').hide();
    $('#txtLedger').val("");
    voucher.autoComplete();
    voucher.Ledger_autoComplete();

    $(document).on("change", ".clsDDLdrcr", function () {
        var AccountCode = $(this).closest('tr').find('.clsAccountCode').html();
        var drcr = $(this).closest('tr').find('.clsDDLdrcr').val();
        var Amount = $(this).closest('tr').find('.clsAccountAmount').val();
        var ids = this;
        if (Amount == "")
            return false;
        voucher.update(AccountCode, drcr, Amount, ids);
    });

    $(document).on("blur", ".clsAccountAmount", function () {
        
        var AccountCode = $(this).closest('tr').find('.clsAccountCode').html();
        var drcr = $(this).closest('tr').find('.clsDDLdrcr').val();
        var Amount = $(this).closest('tr').find('.clsAccountAmount').val();
        var ids = this;
        if (drcr == 0 && Amount != 0) {
            alert('Please Select DRCR');
            return false;
        } else if (Amount == "") { 
            return false;

        }
        voucher.update(AccountCode, drcr, Amount, ids);
    });

    $(document).on("keydown", "#txtLedger", function (evt) {
        voucher.Ledger_Blank(evt);
    });
});





voucher = {
    Populate: function () {

        var V = "{FinYear:'" + $.trim($("#ddlGlobalFinYear option:selected").text()) + "', VoucherType:'" + $('#ddlVoucherType').val() + "',  SectorID:'" + $('#ddlSector').val() + "'}";
        $.ajax({
            type: "POST",
            url: "/Accounts_Form/AccountsVoucherOpening/GET_AccountsDetails",
            data: V,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                
                var detail = data.Data;
                if (detail.length > 0) {

                    var table = $("#tblVoucheropening");
                    table.find('thead').empty();
                    table.find('tbody').empty();

                    var hide_Columns = [0,1];
                    voucher.Populate_Headers(detail, hide_Columns);

                    for (var i = 0; i < detail.length; i++) {
                        table.find('tbody').append("<tr><td class='clsAccountCode' style='display:" + ((voucher.Check_Array(0, hide_Columns)) == true ? 'none' : 'block') + "'>" + detail[i].AccountCode + "</td>"
                                              + "<td style='display:" + ((voucher.Check_Array(1, hide_Columns)) == true ? 'none' : 'block') + "'>" + detail[i].OldAccountHead + "</td>"
                                              + "<td>" + detail[i].AccountDescription + "</td>"
                                              + "<td style='text-align:center'>"
                                              + "<select id='op_" + detail[i].AccountCode + "' class='clsDDLdrcr' >"
                                              + "<option value='0'>DR/CR</option>"
                                              + "<option value='D'>DR</option>"
                                              + "<option value='C'>CR</option>"
                                              + "</select></td>"
                                              + "<td style='text-align:center'><input type='text' style='width:50%; text-align:right' class='clsAccountAmount' value='" + detail[i].OpBalance + "'/></td></tr>");
                        $('#op_' + detail[i].AccountCode).val(detail[i].DRCR == '' ? 0 : detail[i].DRCR);
                    }
                }
            }
        });
    },
    Ledger_autoComplete: function () {
        $("#txtLedger").autocomplete({
            source: function (request, response) {

                var V = "{AccountCode:'" + "" + "', Desc:'" + $("#txtLedger").val() + "', FinYear:'" + $("#ddlGlobalFinYear option:selected").text() + "',  SectorID:'" + $("#ddlSector").val() + "'}";
                $.ajax({
                    type: "POST",
                    url: "/Accounts_Form/AccountsVoucherOpening/Auto_SubLedger_Details",
                    data: V,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var t = D.Data;
                        var DataAutoComplete = [];
                        if (t.length > 0) {
                            $.each(t, function (index, item) {
                                DataAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });
                            response(DataAutoComplete);
                        }
                    }
                });
            },
            minLength: 0,
            select: function (e, i) {

                $("#hdnLedger").val(i.item.AccountCode);

                E = "{AccountCode:'" + i.item.AccountCode + "', Desc:'" + $("#txtLedger").val() + "', FinYear:'" + $("#ddlGlobalFinYear option:selected").text() + "',  SectorID:'" + $("#ddlSector").val() + "'}";
                $.ajax({
                    type: "POST",
                    url: "/Accounts_Form/AccountsVoucherOpening/Auto_SubLedger_Details",
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var t = D.Data;
                        if (t.length > 0 && D.Status == 200) {
                           
                            voucher.ShowDetail(D);
                        }
                    }
                });

            },
        }).click(function () {
            if ($("#ddlVoucherType").val() == 0) {
                alert('Please Select Account Type.'); $("#ddlVoucherType").focus(); return false;
            }
            $(this).autocomplete('search', ($(this).val()));
        });
    },
    Ledger_Blank: function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode; 
        if (iKeyCode == 8) {
            $("#txtLedger").val('');
            $("#hdnLedger").val('');
        }
        if (iKeyCode == 46) {
            $("#txtLedger").val('');
            $("#hdnLedger").val('');
        }
    },
    autoComplete: function () {
        $("#txtAutoComp").autocomplete({
            source: function (request, response) {

                var Vals = ($('#radioName').is(':checked') ? 'Name' : 'Code');

                var URL = ""; var E = "";
                if ($("#ddlVoucherType").val() == 'S') {
                    URL = "/Accounts_Form/AccountsVoucherOpening/Auto_SubLedger_Details";
                    E = "{AccountCode:'" + $("#hdnLedger").val() + "', Desc:'" + $("#txtAutoComp").val() + "', FinYear:'" + $("#ddlGlobalFinYear option:selected").text() + "',  SectorID:'" + $("#ddlSector").val() + "'}";
                }
                else {
                    URL = "/Accounts_Form/AccountsVoucherOpening/Search_AutoComplete";
                    E = "{AutoText: '" + $("#txtAutoComp").val() + "', SearchType: '" + Vals + "', VoucherType: '" + $("#ddlVoucherType").val() + "', FinYear:'" + $("#ddlGlobalFinYear option:selected").text() + "',  SectorID:'" + $("#ddlSector").val() + "'}";
                }
                 $.ajax({
                    type: "POST",
                    url: URL,
                    data: E,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        voucher.ShowDetail(data);
                    }
                });
            },
            minLength: 0
        }).click(function () {
            if ($("#ddlVoucherType").val() == 0) {
                alert('Please Select Account Type.'); $("#ddlVoucherType").focus(); return false;
            }
            if ($("#ddlVoucherType").val() == 'S' && $("#hdnLedger").val() == '') {
                alert('Please Select Ledger.'); $("#txtLedger").focus(); return false;
            }
            $(this).autocomplete('search', ($(this).val()));
        });
    },
    ShowDetail: function (data) {
      
        var detail = data.Data; 
        if (detail.length > 0) {

            var table = $("#tblVoucheropening");
            table.find('thead').empty();
            table.find('tbody').empty();

            var hide_Columns = [0,1];
            voucher.Populate_Headers(detail, hide_Columns);



            //for (var i = 0; i < detail.length; i++) {
            //    $("#tblVoucheropening tbody").append("<tr>"
            //        + "<td  class='clsAccountCode' style='display:" + ((voucher.Check_Array(0, hide_Columns)) == true ? 'none' : 'block' ) + "' >" + detail[i].AccountCode + "</td>"
            //        + "<td  style='display:" + ((voucher.Check_Array(1, hide_Columns)) == true ? 'none' : 'block') + "'>" + detail[i].OldAccountHead + "</td>"
            //        + "<td>" + detail[i].AccountDescription + "</td>"
            //        + "<td>"
            //        + "<select id='op_" + detail[i].AccountCode + "' class='clsDDLdrcr' >"
            //        + "<option value='0'>DR/CR</option>"
            //        + "<option value='D'>DR</option>"
            //        + "<option value='C'>CR</option>"
            //        + "</select></td>"
            //        + "<td><input type='text' style='width:50%;' class='clsAccountAmount textbox' value='" + detail[i].OpBalance + "'/></td></tr>")
            //        $('#op_' + detail[i].AccountCode).val(detail[i].DRCR == '' ? 0 : detail[i].DRCR);
            //}
            for (var i = 0; i < detail.length; i++) {
                table.find('tbody').append("<tr><td class='clsAccountCode' style='display:" + ((voucher.Check_Array(0, hide_Columns)) == true ? 'none' : 'block') + "'>" + detail[i].AccountCode + "</td>"
                                      + "<td style='display:" + ((voucher.Check_Array(1, hide_Columns)) == true ? 'none' : 'block') + "'>" + detail[i].OldAccountHead + "</td>"
                                      + "<td>" + detail[i].AccountDescription + "</td>"
                                      + "<td style='text-align:center'>"
                                      + "<select id='op_" + detail[i].AccountCode + "' class='clsDDLdrcr' >"
                                      + "<option value='0'>DR/CR</option>"
                                      + "<option value='D'>DR</option>"
                                      + "<option value='C'>CR</option>"
                                      + "</select></td>"
                                      + "<td style='text-align:center'><input type='text' style='width:50%; text-align:right' class='clsAccountAmount' value='" + detail[i].OpBalance + "'/></td></tr>");
                $('#op_' + detail[i].AccountCode).val(detail[i].DRCR == '' ? 0 : detail[i].DRCR);
            }
        }
        else {
            $("#tblVoucheropening tbody").empty();
        }
    },
    Populate_Headers: function (data, hide_Columns) {
        var html = "";
        html += "<tr>";

        var table = $("#tblVoucheropening");

        //populate header

        for (var j = 0; j < (Object.keys(data[0])).length; j++) {
            var Columns = Object.keys(data[0])[j];

            var res = voucher.Check_Array(j, hide_Columns);

            if (res == true) {
                html += "<th data-field-name='" + Object.keys(data[0])[j] + "' data-col-rank='" + j + "' style='text-align:center; border: 1px solid; display:none'>" + Object.keys(data[0])[j] + "</th>";
            }
            else {
                html += "<th data-field-name='" + Object.keys(data[0])[j] + "' data-col-rank='" + j + "' style='text-align:center; border: 1px solid;'>" + Object.keys(data[0])[j] + "</th>";
            }
        }
        html += "</tr>";
        table.find('thead').append(html);
    },
    Check_Array: function (value, hide_list) {

        if (jQuery.inArray(value, hide_list) != '-1') {
            return true;
        } else {
            return false;
        }
    },
    update: function (AccountCode, drcr, Amount, id) {

        var ParetnAccCode = "";
        var SectorID = $("#ddlSector").val();
        var AccType = $("#ddlVoucherType").val();
        if (AccType == "A" || AccType == "L")
            ParetnAccCode = AccountCode;
        else
            ParetnAccCode = $("#hdnLedger").val();

        var C = "{AccountCode: '" + AccountCode + "', ParentAccountCode: '" + ParetnAccCode + "', DRCR: '" + drcr + "', Amount : '" + Amount + "', FinYear:'" + $("#ddlGlobalFinYear option:selected").text() + "', SectorID:'" + SectorID + "'}";
        $.ajax({
            type: "POST",
            url: '/Accounts_Form/AccountsVoucherOpening/Update_Details',
            data: C,
            contentType: "application/json; charset=utf-8",
            success: function (D) {
              
                var result = D.Message;
                if (result == "Success") {
                    if ($(id).attr('type') == 'text') {
                        $(id).css("background-color", "lightgreen");
                    }
                }
                else {
                    alert('There is some Problem.'); return false;
                }
            },
            error: function (response) {
                alert('');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}