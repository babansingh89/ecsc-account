﻿
var ServerResponse = "";

$(function () {

    Bank.FromandToDate();
    Bank.DefaultDate();

    Bank.Populate_Grid();
    Bank.VoucherAuto();

    $(document).on('keydown', '.cls_Bank', function (evt) {

        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $(".cls_Bank").val('');
            $(".hdn").val('0000000000');
        }
        if (iKeyCode == 46) {
            $(".cls_Bank").val('');
            $(".hdn").val('0000000000');
        }

        var ths = $(this);
        ths.removeClass('selected');
        ths.addClass('selected');
        var t = ths.attr('id'); 
        Bank.auto(t);
    });

    $(document).on('click', ".cls_Save", function () {
        var currentrow = $(this).parent().parent();
        var BillID = currentrow.find(".BillID").text();
        var VoucherNumber = currentrow.find(".VoucherNumber").text();
        var VoucherDetailID = currentrow.find(".VoucherDetailID").text();
        var AccountCode = currentrow.find(".hdn").val();
        var InstTypeID = currentrow.find(".cls_InstType").val();
        var InstNo = currentrow.find(".cls_InstNo").val(); 
        var InstDate = currentrow.find(".cls_InstDate").val();
        var PaymentDate = currentrow.find(".cls_PaymentDate").val(); //New Field Added

        if (PaymentDate == "") { currentrow.find(".cls_PaymentDate").focus(); return false; }
        if (AccountCode == "0000000000") { currentrow.find(".cls_Bank").focus(); return false; }
        if (InstTypeID == "") { currentrow.find(".cls_InstType").focus(); return false; }
        if (InstNo == "") { currentrow.find(".cls_InstNo").focus(); return false; }
        if (InstDate == "") { currentrow.find(".cls_InstDate").focus(); return false; }

        Bank.Save(BillID, VoucherNumber, VoucherDetailID, AccountCode, InstTypeID, InstNo, InstDate, PaymentDate);
    });


});

var Bank = {
    FromandToDate: function () {
        $('#txtFromDate, #txtToDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow',
            dateFormat: 'dd/mm/yy'
        });
    },
    DefaultDate: function ()
    {
        //Set From date
        var finyear = $("#ddlGlobalFinYear option:selected").text(); 
        var fy = finyear.split('-');
        var validDate = '01/04/' + fy[0];
        $('#txtFromDate').val(validDate);

        //Set To Date
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
        $('#txtToDate').val(output);
    },

    Load: function ()
    {
        if ($("#txtFromDate").val() == "") { $("#txtFromDate").focus(); return false; }
        if ($("#txtToDate").val() == "") { $("#txtToDate").focus(); return false; }

        Bank.Populate_Grid();
    },
    VoucherAuto: function ()
    {
        $('#txtVoucherNo').autocomplete({
            source: function (request, response) {

                Bank.Populate_Grid();
            },
            select: function (e, i) {
                //$('#hdnBankName').val(i.item.AccountCode);
            },
            minLength: 0
        });
    },

    Populate_Grid: function () {

        var E = "{FromDate: '" + $("#txtFromDate").val() + "', ToDate: '" + $("#txtToDate").val() + "', Desc:'" + $('#txtVoucherNo').val() + "'}";
        var URL = "/Accounts_Form/Bank_Payment_Updation/Show_Detail";
        $.ajax({
            url: URL,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: E,
            dataType: 'json',
            success: function (D) {
                var t = D.Data;
                if (t.length > 0 && D.Status == 200) {
                    Bank.populate(t);
                }
            }
        });
    },
    populate: function (Data) {

        var table = $("#myTable");
        table.find('thead').empty();
        table.find('tbody').empty();
        //Header
        table.find("thead").append("<tr><th style='text-align:center;'>Voucher No.</th><th style='text-align:center;'>Voucher Date</th>" +
                                   "<th style='text-align:center;'>Payment Date</th>"+
                                   "<th style='text-align:center;'>InFavour of</th><th style='text-align:center;'>Amount</th>"+
                                   "<th style='text-align:center;'>Bank</th><th style='text-align:center;'>Instrument Type</th>" +
                                   "<th style='text-align:center;'>Instrument No.</th><th style='text-align:center;'>Instrument Date</th>"+
            "<th style='text-align:center;'></th></tr>");

        //Set Current Date
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var CurrDate = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();

        //Data
        for (var i = 0; i < Data.length; i++) {
            table.find("tbody").append("<tr class='myData'>" +
                "<td style='display:none;' class='BillID'>" + Data[i].BillId + "</td>" +
                "<td style='display:none;' class='VoucherDetailID'>" + Data[i].VoucherDetailID + "</td>" +
                "<td style='display:none;' class='VoucherNumber'>" + Data[i].VoucherNumber + "</td>" +
                "<td class='VchNo' style='font-size:10px'>" + Data[i].VchNo + "</td>" +
                "<td class='VoucherDate' style='font-size:10px'>" + Data[i].VoucherDate + "</td>" +



                "<td><input id='txtPaymentDate_" + i + "' class='cls_PaymentDate form-control' type='text' value='" + Data[i].PaymentDate + "' placeholder='DD/MM/YYYY' style='font-size:10px' />" +
                "<script type='text/javascript'> $('#txtPaymentDate_" + i + "').datepicker({" +
                "showOtherMonths: true," +
                "selectOtherMonths: true," +
                "closeText: 'X'," +
                "showAnim: 'drop'," +
                "changeYear: true," +
                "changeMonth: true," +
                "duration: 'slow'," +
                "minDate: '" + Data[i].VoucherDate + "'," +
                "maxDate: '" + CurrDate +"'," +
                "dateFormat: 'dd/mm/yy'" +
                "});</script></td>" +




                "<td class='InFavour' style='text-align:left; font-size:10px'>" + Data[i].InFavour + "</td>" +
                "<td class='AdjustmentAmount' style='text-align:right; font-size:10px'>" + (Data[i].AdjustmentAmount).toFixed(2) + "</td>" +

                "<td style='text-align:center; width:140px; font-size:10px'><input class='cls_Bank form-control' type='text' id=txtBank_" + i + " />" +
                "<input type=hidden class=hdn ID=hdnBank_" + i + " value=" + Data[i].AccountCode + " style='font-size:10px' /></td>" +
                
                "<td style='text-align:center; width:140px; font-size:10px; font-size:10px'><select class='form-control cls_InstType' id='ddlInstType_" + i + "' >" +
                "</select></td>" +

                "<td><input class='cls_InstNo form-control' style='font-size:10px' type='text' value='" + Data[i].InstrumentNumber + "' /></td>" +

               "<td><input id='txtInstDate_" + i + "' class='cls_InstDate form-control' type='text' value='" + Data[i].InstrumentDate + "' placeholder='DD/MM/YYYY' style='font-size:10px' />" +
                "<script type='text/javascript'> $('#txtInstDate_" + i + "').datepicker({" +
                "showOtherMonths: true," +
                "selectOtherMonths: true," +
                "closeText: 'X'," +
                "showAnim: 'drop'," +
                "changeYear: true," +
                "changeMonth: true," +
                "duration: 'slow'," +
                "dateFormat: 'dd/mm/yy'" +
                "});</script></td>" +

                "<td><input class='cls_Save form-control btn btn-primary' type='button' value='Save'  /></td></tr>");

            if(Data[i].InstrumentDate == "")
                $('#txtInstDate_' + i).val(Data[i].VoucherDate);
        }
        Bank.Populate_Inst_Type();
    },
    Populate_Inst_Type: function()
    {
        var URL = "/Accounts_Form/Bank_Payment_Updation/Load_Instrument";
        $.ajax({
            url: URL,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: {},
            dataType: 'json',
            success: function (D) {
                var a = D.Data.length;
                if (a > 0) {
                    $(".cls_InstType").empty();
                    $(".cls_InstType").append("<option value=''>--Select--</option>")
                    $.each(D.Data, function (index, value) {
                        var label = value.Instrument;
                        var val = value.InstrumentTypeID;
                        $(".cls_InstType").append($("<option></option>").val(val).html(label));
                    });
                }
            }
        });
    },
    auto: function (ref) {
        $('#' + ref).autocomplete({

            source: function (request, response) {
                var V = "{ AccountDescription :'" + $('#' + ref).val() + "', SectorID: '" + $('#ddlSector').val() + "'}"; 
                $.ajax({
                    url: '/Accounts_Form/Bank_Payment_Updation/GetAuto',
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: V,
                    dataType: 'json',
                    success: function (serverResponse) {
                        var SubledgerAutoComplete = [];
                        if ((serverResponse.Data).length > 0) {
                            $.each(serverResponse.Data, function (index, item) {
                                SubledgerAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });
                            response(SubledgerAutoComplete);
                        }
                    },
                    error: function () { }
                });
            },
            select: function (e, i) {
                //$(this).val(i.item.label);
                var id = i.item.AccountCode;
                $('#' + ref).closest('tr').find('.hdn').val(id);
                //alert($('#' + ref.id).closest('tr').find('.hdn').val());
            },
            minLength: 0
        }).click(function () {
            $(ref).autocomplete('search', ($(ref).val()))
        });
    },
    Save: function (BillID, VoucherNumber, VoucherDetailID, AccountCode, InstTypeID, InstNo, InstDate, PaymentDate) {
     
        var FinalInstDate = ""; var FinalPaymentDate = "";
        if (InstDate != "") {
            var aa = InstDate.split("/");
            FinalInstDate = aa[2] + "-" + aa[1] + "-" + aa[0];
        }
     
        if (PaymentDate != "") {
            var bb = PaymentDate.split("/");
            FinalPaymentDate = bb[2] + "-" + bb[1] + "-" + bb[0];
        }

        var V = "{BillID:'" + BillID + "', VoucherNumber:'" + VoucherNumber + "', VoucherDetailID:'" + VoucherDetailID + "', AccountCode:'" + AccountCode + "',"+
                "InstTypeID:'" + InstTypeID + "', InstNo:'" + InstNo + "', InstDate:'" + FinalInstDate + "', PaymentDate:'" + FinalPaymentDate + "'}";

        var URL = "/Accounts_Form/Bank_Payment_Updation/Save";

        $.ajax({
            url: URL,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: V,
            dataType: 'json',
            success: function (D) {
                var t = D.Data;
                if (t == "success" && D.Status == 200) {
                    alert('Details has been Updated Successfully !!');
                    Bank.Populate_Grid();
                    return false;
                }
                else {
                    alert('There is Some Problem !!');
                    return false;
                }
            }
        });
    }
}

function SearchTable() {

    var forSearchprefix = $("#searchInput").val().trim().toUpperCase();
    var tablerow = $('#myTable').find('.myData');
    $.each(tablerow, function (index, value) {
        var VchNo = $(this).find('.VchNo').text().toUpperCase();
        var VoucherDate = $(this).find('.VoucherDate').text().toUpperCase();
        var InFavour = $(this).find('.InFavour').text().toUpperCase();

        if (VchNo.indexOf(forSearchprefix) > -1 || VoucherDate.indexOf(forSearchprefix) > -1 || InFavour.indexOf(forSearchprefix) > -1 ) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}
function Excel() {

    var fromDate = $("#txtFromDate").val();
    var toDate = $("#txtToDate").val();
    if (fromDate == "") {
        $("#txtFromDate").focus(); return false;
    }
    if (toDate == "") {
        $("#txtToDate").focus(); return false;
    }

    var res = confirm('Are You Sure Want to download Excel Report ??');
    if (res == true) {

        //var fDate = ""; var tDate = "";
        //if ($("#txtFromDate").val() != "") {
        //    var a = ($("#txtFromDate").val()).split('/');
        //    fDate = a[2] + '-' + a[1] + '-' + a[0];
        //}
        //if ($("#txtToDate").val() != "") {
        //    var b = ($("#txtToDate").val()).split('/');
        //    tDate = b[2] + '-' + b[1] + '-' + b[0];
        //}

        var master = []; var final = {};

        master.push({
            FromDate: $("#txtFromDate").val(),
            ToDate: $("#txtToDate").val(),
            VoucherNo: $("#txtVoucherNo").val(),
            RepType: "E",
            FileName: "BankPayment"
        });
        final = {
            Master: master
        }

        location.href = "/Accounts_Form/Bank_Payment_Updation/Excel?ReportName=" + JSON.stringify(final);
    }
}
function UnPaidExcel() {

    var fromDate = $("#txtFromDate").val();
    var toDate = $("#txtToDate").val();
    if (fromDate == "") {
        $("#txtFromDate").focus(); return false;
    }
    if (toDate == "") {
        $("#txtToDate").focus(); return false;
    }

    var res = confirm('Are You Sure Want to download Excel Report ??');
    if (res == true) {

        var fDate = ""; var tDate = "";
        if ($("#txtFromDate").val() != "") {
            var a = ($("#txtFromDate").val()).split('/');
            fDate = a[2] + '-' + a[1] + '-' + a[0];
        }
        if ($("#txtToDate").val() != "") {
            var b = ($("#txtToDate").val()).split('/');
            tDate = b[2] + '-' + b[1] + '-' + b[0];
        }

        var master = []; var final = {};

        master.push({
            FromDate: fDate,
            ToDate: tDate,
            VoucherNo: $("#txtVoucherNo").val(),
            RepType: "E",
            FileName: "UnPaidVoucherList"
        });
        final = {
            Master: master
        }

        location.href = "/Accounts_Form/Bank_Payment_Updation/UnPaidExcel?ReportName=" + JSON.stringify(final);
    }
}
