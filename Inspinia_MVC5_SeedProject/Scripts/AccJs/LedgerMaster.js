﻿
var MaintainAccCode = "";
var isGLSL = "1";
var Ledger = {};
var unPaidVch = ""; var isunpaidvch = 0;

$(document).ready(function () {
    AccountRule();
    Ledger.Ledger_AutoCom();
    Ledger.SubLedger_AutoCom();

    //Date Checking
    Set_DefaultDate();
    SetFrom_Date();
    SetTo_Date();
    $("#txtToDate").datepicker("option", "minDate", $('#txtFromDate').val());
    $('#txtSearchSubLedger').focus();

    //Sub Ledger hide and show
    $('#radioGL').change(function () {
        isGLSL = "1"; unPaidVch = "";
        $("#pnl_SubLedger").css("display", "none");
        $('#txtSearchSubLedger').focus();
        $('#txtSubLedger').val('');
        $("#hdnSubLedger").val('');
        $('#txtSearchSubLedger').val('');
        $('#hdnSearchSubLedger').val('');
        $('#txtSearchSubLedger').prop('placeholder', 'Ledger');

        var table = $("#myTable");
        table.find('thead').empty();
        table.find('tbody').empty();
    });
    $('#radioSL').change(function () {
        isGLSL = "2"; unPaidVch = "";
        $("#pnl_SubLedger").css("display", "block");
        $('#txtSubLedger').focus();
        $('#txtSearchSubLedger').val('');
        $('#hdnSearchSubLedger').val('');
        $('#txtSearchSubLedger').prop('placeholder', 'Sub Ledger');

        var table = $("#myTable");
        table.find('thead').empty();
        table.find('tbody').empty();
    });
    $('#radioUV').click(function () {

        var isChecked = $('#radioUV').prop('checked'); 
        if (isChecked == true && isunpaidvch ==0) {
            unPaidVch = "I"; isunpaidvch = 1;
        }
        else {
            unPaidVch = ""; isunpaidvch = 0; $('#radioUV').prop('checked', false);
        }
       
        $("#pnl_SubLedger").css("display", "none");
        $('#txtSearchSubLedger').focus();
        $('#txtSubLedger').val('');
        $("#hdnSubLedger").val('');
        $('#txtSearchSubLedger').val('');
        $('#hdnSearchSubLedger').val('');
        $('#txtSearchSubLedger').prop('placeholder', 'Ledger');

        var table = $("#myTable");
        table.find('thead').empty();
        table.find('tbody').empty();
    });

    $('#txtSubLedger').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtSubLedger").val('');
            $("#hdnSubLedger").val('');
            $("#txtSearchSubLedger").val('');
            $("#hdnSearchSubLedger").val('');
        }
        if (iKeyCode == 46) {
            $("#txtSubLedger").val('');
            $("#hdnSubLedger").val('');
            $("#txtSearchSubLedger").val('');
            $("#hdnSearchSubLedger").val('');
        }
    });
    $('#txtSearchSubLedger').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            if ($("#hdnSearchSubLedger").val() != "")
            {
                $("#txtSearchSubLedger").val('');
                //$("#hdnSearchSubLedger").val('');
            }
            else
            {
                $("#txtSearchSubLedger").val('');
                $("#hdnSearchSubLedger").val('');
            }
        }
        if (iKeyCode == 46) {
            if ($("#hdnSearchSubLedger").val() != "") {
                $("#txtSearchSubLedger").val('');
                //$("#hdnSearchSubLedger").val('');
            }
            else {
                $("#txtSearchSubLedger").val('');
                $("#hdnSearchSubLedger").val('');
            }
        }
    });

    //Print
    $('#btnPrintLedger').click(function (evt) { Ledger.Print_Ledger(); });
    //Excel
    $('#btnExcelLedger').click(function (evt) { Ledger.Excel_Ledger(); });

});

//Date Checking
function SetFrom_Date() {
    $('#txtFromDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy',
        onSelect: function () {
            var a = $('#txtAccFinYear').val();
            if (a != "") {
                var res = FinYear_Validation(a);
                if (res == false) {
                    alert('Sorry ! Please Enter Valid Account Financial Year.'); $('#txtFromDate').val(''); return false;
                }
                else {
                    //Between_Dates();
                    $("#txtToDate").datepicker("option", "minDate", $('#txtFromDate').val());
                }
            }
        }
    });
}
function SetTo_Date() {
    $('#txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}
function Set_DefaultDate() {
    //Set From Date
    var a = $('#txtAccFinYear').val();
    var aa = FinYear_Validation(a);
    if (aa == true) {
        var fy = a.split('-');
        var validDate = '01/04/' + fy[0];
        $('#txtFromDate').val(validDate);
    }
    else {
        alert('Sorry ! Wrong Account Fin Year.'); return false;
    }

    //Set To Date
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (('' + day).length < 2 ? '0' : '') + day + '/' + (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear();
    $('#txtToDate').val(output);
}
function FinYear_Validation(year) {

    if (year != "") {

        var str1 = $.trim(year);
        var str2 = "-";
        var a1 = ""; var a2 = "";
        //Special Character Checking
        if (str1.indexOf(str2) == -1) {
            return false;
        }
        else {
            var a = year.split('-');
            a1 = a[0];
            a2 = a[1];
            //Length Checking
            if ($.trim(a1).length != 4)
                return false;
            if ($.trim(a2).length != 4)
                return false;

            //isNumeric Checking
            var s1 = $.isNumeric(a1);
            var s2 = $.isNumeric(a2);
            if (s1 == false)
                return false;
            if (s2 == false)
                return false;

            //Max and Min Year Checking
            var g = parseInt(a1) + 1;
            if (g != a2)
                return false;
        }

        return true;
    }
    else
        return false;

}
function Between_Dates() {
    var fdate = $('#txtFromDate').val();
    var tdate = $("#txtToDate").val();

    var fSplitDate = fdate.split('/');
    var tSplitDate = tdate.split('/');

    var a = $('#txtAccFinYear').val();
    if (a != '') {
        var fy = a.split('-');
        a1 = fy[0];
        a2 = fy[1];

        var startDate = "01/04/" + a1;
        var endDate = "31/03/" + a2;

        var newStartDate = new Date(a1, 03, 01);
        var newendDate = new Date(a2, 02, 31);

        var givenFromDate = new Date(fSplitDate[2], parseInt(fSplitDate[1]) - 1, fSplitDate[0]);
        var givenToDate = new Date(tSplitDate[2], parseInt(tSplitDate[1]) - 1, tSplitDate[0]);

        if ((givenFromDate >= newStartDate && givenFromDate <= newendDate)) { }
        else
        {
            alert('Sorry ! Given From Date is not within Account Financial Year.'); Set_DefaultDate(); return false;
        }

        if ((givenToDate >= newStartDate && givenToDate <= newendDate)) { }
        else {
            alert('Sorry ! Given To Date is not within Account Financial Year.'); Set_DefaultDate(); return false;
        }

    }
}


Ledger = {

    Ledger_AutoCom: function () {
        $("#txtSearchSubLedger").autocomplete({
            source: function (request, response) {

                var URL = ""; var V = "";
                if (isGLSL == 1) { //GL  
                    URL = "/Accounts_Form/LedgerMaster/Get_Ledger_Details";
                    V = "{Desc:'" + $("#txtSearchSubLedger").val() + "', FinYear:'" + $("#txtAccFinYear").val() + "',  SectorID:'" + $("#ddlSector").val() + "'}";
                }
                else               //SL
                {
                    URL = "/Accounts_Form/AccountsVoucherOpening/Auto_SubLedger_Details";
                    V = "{AccountCode:'" + $("#hdnSubLedger").val() + "', Desc:'" + $("#txtSearchSubLedger").val() + "', FinYear:'" + $("#txtAccFinYear").val() + "',  SectorID:'" + $("#ddlSector").val() + "'}";
                }
                $.ajax({
                    type: "POST",
                    url: URL,
                    data: V,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var t = D.Data;
                        var DataAutoComplete = [];
                        if (t.length > 0) {
                            $.each(t, function (index, item) {
                                DataAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode,
                                    GroupLedger: item.GroupLedger
                                });
                            });
                            response(DataAutoComplete);
                        }
                    }
                });
            },
            minLength: 0,
            select: function (e, i) {

                $("#hdnSearchSubLedger").val(i.item.AccountCode);
				 $("#hdnGroupLedger").val(i.item.GroupLedger);

                var fDate = ""; var tDate = "";
                if ($("#txtFromDate").val() != "") {
                    var a = ($("#txtFromDate").val()).split('/');
                    fDate = a[2] + '-' + a[1] + '-' + a[0];
                }
                if ($("#txtToDate").val() != "") {
                    var b = ($("#txtToDate").val()).split('/');
                    tDate = b[2] + '-' + b[1] + '-' + b[0];
                }

                E = "{AccountCode:'" + ($('#hdnSubLedger').val() + i.item.AccountCode ) + "', Desc:'" + $("#txtSearchSubLedger").val() + "', FinYear:'" + $("#txtAccFinYear").val() + "'," +
                    " SectorID:'" + $("#ddlSector").val() + "', FromDate:'" + fDate + "', ToDate:'" + tDate + "', " +
                    " LegerType: '" + i.item.GroupLedger + "', unPaidVch:'" + unPaidVch + "'}";

                //alert(E);
                $.ajax({
                    type: "POST",
                    url: "/Accounts_Form/LedgerMaster/Show_GeneralLedger",   
                    data: E,
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var t = D.Data;
                        if (t.length > 0 && D.Status == 200) {

                            Ledger.ShowDetail(t);
                        }
                    }
                });

            },
        }).click(function () {
            if (isGLSL == 1)  //GL
            {

            }
            else {
                if ($("#hdnSubLedger").val() == "") {
                    alert('First Select Proper Ledger.'); $("#txtSubLedger").focus(); return false;
                }
            }
            $(this).autocomplete('search', ($(this).val()));
        });
    },
    SubLedger_AutoCom: function () {
        $("#txtSubLedger").autocomplete({
            source: function (request, response) {

                var V = "{AccountCode:'" + "" + "', Desc:'" + $("#txtSubLedger").val() + "', FinYear:'" + $("#txtAccFinYear").val() + "',  SectorID:'" + $("#ddlSector").val() + "'}";
                $.ajax({
                    type: "POST",
                    url: "/Accounts_Form/AccountsVoucherOpening/Auto_SubLedger_Details",
                    data: V,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (D) {
                        var t = D.Data;
                        var DataAutoComplete = [];
                        if (t.length > 0) {
                            $.each(t, function (index, item) {
                                DataAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });
                            response(DataAutoComplete);
                        }
                    }
                });
            },
            minLength: 0,
            select: function (e, i) {

                $("#hdnSubLedger").val(i.item.AccountCode);

            },
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });
    },
    ShowDetail: function (data) {

        var html = "";
        var table = $("#myTable");

        table.find('thead').empty();
        table.find('tbody').empty();



        html += "<tr>";
        table.find('thead').append("<tr><th class='My_Header'>Date</th><th class='My_Header' style='display:" + (MaintainAccCode == "N" ? "none" : "normal") + "'>Account Head</th><th class='My_Header'>Account Description</th><th class='My_Header newField' style='display:none;'>Name</th><th class='My_Header newField' style='display:none;'>PAN</th><th class='My_Header'>GST</th><th class='My_Header'>Instrument No.</th><th class='My_Header'>Vch No.</th><th class='My_Header'>Debit</th><th class='My_Header'>Credit</th><th class='My_Header grdImg'>Edit</th></tr>");
        for (var i = 0; i < data.length; i++) {
            var VchNo = data[i].VoucherNumber;
            var AcctDesc = data[i].AccountDescription;
            if (AcctDesc == "Opening Balance") {
                html += "<tr style='background-color:#F5F5F6;'>";
                html += "<td style='text-align:center; font-weight:bold;'>" + data[i].VchDate + "</td>";
                html += "<td style='text-align:right; font-weight:bold; display:" + (MaintainAccCode == "N" ? "none" : "normal") + "'>" + "" + "</td>";
                html += "<td colspan='4' style='text-align:center; font-weight:bold;' class='forAlign'>" + data[i].AccountDescription + "</td>";
                html += "<td style='text-align:right; font-weight:bold;'>" + (data[i].DrAmount).toFixed(2) + "</td>";
                html += "<td style='text-align:right; font-weight:bold;'>" + (data[i].CrAmount).toFixed(2) + "</td>";
                html += "<td style='text-align:right; font-weight:bold;' class='grdImg'></td>";
                html += "</tr>";
            }
            else if (AcctDesc == "Closing Balance") {
                html += "<tr style='background-color:#F5F5F6;'>";
                html += "<td style='text-align:center; font-weight:bold;'>" + data[i].VchDate + "</td>";
                html += "<td style='text-align:right; font-weight:bold; display:" + (MaintainAccCode == "N" ? "none" : "normal") + "'>" + "" + "</td>";
                html += "<td colspan='4' style='text-align:center; font-weight:bold;' class='forAlign'>" + data[i].AccountDescription + "</td>";
                html += "<td style='text-align:right; font-weight:bold;'>" + (data[i].DrAmount).toFixed(2) + "</td>";
                html += "<td style='text-align:right; font-weight:bold;'>" + (data[i].CrAmount).toFixed(2) + "</td>";
                html += "<td style='text-align:right; font-weight:bold;' class='grdImg'></td>";
                html += "</tr>";
            }
else if (AcctDesc == "TRANSACTION TOTAL") {
    html += "<tr style='background-color:#F5F5F6;'>";
                html += "<td style='text-align:center; font-weight:bold;'>" + (data[i].VchDate == null ? '' : data[i].VchDate) + "</td>";
                html += "<td style='text-align:right; font-weight:bold; display:" + (MaintainAccCode == "N" ? "none" : "normal") + "'>" + "" + "</td>";
                html += "<td colspan='4' style='text-align:center; font-weight:bold;' class='forAlign'>" + data[i].AccountDescription + "</td>";

                html += "<td style='text-align:right; font-weight:bold;'>" + (data[i].DrAmount).toFixed(2) + "</td>";
                html += "<td style='text-align:right; font-weight:bold;'>" + (data[i].CrAmount).toFixed(2) + "</td>";
                html += "<td style='text-align:right; font-weight:bold;' class='grdImg'></td>";
                html += "</tr>";
            }
            else {
                 html += "<tr class='myData' style='border:none'>";
                html += "<td style='text-align:center;'>" + data[i].VchDate + "</td>";
                html += "<td style='text-align:center; display:" + (MaintainAccCode == "N" ? "none" : "normal") + "'>" + data[i].OldAccountHead + "</td>";
                html += "<td style='text-align:left; width:40%'>" + data[i].AccountDescription + "</br><span style='font-size:10px; font-style:italic;'>" + data[i].Narration + "</span></td>";

                html += "<td style='text-align:left; display:none; font-size:11px;' class='newField'>" + (data[i].Pname == null ? '' : data[i].Pname) + "</td>";
                html += "<td style='text-align:left; display:none; font-size:11px;' class='newField'>" + (data[i].Pan == null ? '' : data[i].Pan) + "</td>";

                html += "<td style='text-align:center;'>" + (data[i].GST == null ? '' : data[i].GST) + "</td>";
                html += "<td style='text-align:center;'>" + (data[i].InstrumentNo == null ? '' : data[i].InstrumentNo) + "</td>";
                html += "<td style='text-align:center;'>" + data[i].VoucherNo + "</td>";
                html += "<td style='text-align:right;'>" + (data[i].DrAmount).toFixed(2) + "</td>";
                html += "<td style='text-align:right;'>" + (data[i].CrAmount).toFixed(2) + "</td>";
                html += "<td style='text-align:center;' class='grdImg'><a target='_blank' href='/Accounts_Form/VoucherMaster/Index/" + VchNo + "'><img style='cursor:pointer;' src='/Content/Images/Edit.png' alt='cross' height=20 width=20 /></a></td>";
                html += "</tr>";

            }
        }
        html += "</tr>";
        table.find('tbody').append(html);
    },
    Print_Ledger: function () {

        var table = $("#myTable");
        var tblCount = (table.find('tbody tr.myData')).length; 
        //if (tblCount > 0) {
            var res = confirm('Are You Sure Want to Print Report ??');
            if (res == true) {
                $(".grdImg").hide();
                $(".newField").show(); $(".forAlign").attr('colspan', 6);
                var contents = $("#myDiv").html();
                var frame1 = $('<iframe />');
                frame1[0].name = "frame1";
                frame1.css({ "position": "absolute", "top": "-1000000px" });
                $("body").append(frame1);
                var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
                frameDoc.document.open();
                //Create a new HTML document.
                frameDoc.document.write("<html><head><title></title>");
                frameDoc.document.write('</head><body>');
                //Append the external CSS file.
                //frameDoc.document.write('<link href="~/Content/Gridstyle.css" rel="stylesheet" type="text/css" />');
                //Append the DIV contents.
                frameDoc.document.write("<h3 style='font-family:verdana; font-size:12px'>" + (isGLSL == "1" ? ($('#txtSearchSubLedger').val().trim()) : (($('#txtSubLedger').val().trim())) + " / " + ($('#txtSearchSubLedger').val().trim())) + "</h3>");
              

                frameDoc.document.write(contents);

                 if ($('#hdnSearchSubLedger').val().trim() == "0000001384")
                {
                    var content_Opt = $("#myDivOpt").html();
                    frameDoc.document.write(content_Opt);
                }

                frameDoc.document.write('</body></html>');
                frameDoc.document.close();
                $(".grdImg").show(); $(".newField").hide(); $(".forAlign").attr('colspan', 4);
                setTimeout(function () {
                    window.frames["frame1"].focus();
                    window.frames["frame1"].print();
                    frame1.remove();
                }, 500);

            }
            else {
                alert('Sorry ! Please Select Ledger Details.'); return false;
            }
        //}
        //else
        //{
            //alert('Sorry ! There is no any Data Available to Print.'); return false;
        //}
    },
	Excel_Ledger: function () {

        var table = $("#myTable");
        var tblCount = (table.find('tbody tr.myData')).length;
        if (tblCount > 0) {
            var res = confirm('Are You Sure Want to download Excel Report ??');
            if (res == true) {

                var fDate = ""; var tDate = "";
                if ($("#txtFromDate").val() != "") {
                    var a = ($("#txtFromDate").val()).split('/');
                    fDate = a[2] + '-' + a[1] + '-' + a[0];
                }
                if ($("#txtToDate").val() != "") {
                    var b = ($("#txtToDate").val()).split('/');
                    tDate = b[2] + '-' + b[1] + '-' + b[0];
                }

                var master = []; var final = {};

                alert($('#txtSearchSubLedger').val().replace('&', 'AND'));
                //return false;
                master.push({
                     AccountCode: ($('#hdnSubLedger').val() + $("#hdnSearchSubLedger").val()),
                     Desc: $('#txtSearchSubLedger').val().replace('&', 'AND'),
                    FinYear: $("#txtAccFinYear").val(),
                    SectorID: $("#ddlSector").val(),
                    FromDate: fDate,
                    ToDate: tDate,
                    LegerType: $("#hdnGroupLedger").val(),
                    unPaidVch: unPaidVch,
                    FileName: $('#txtSearchSubLedger').val().replace('&', 'AND')  //$('#txtSearchSubLedger').val()
                    // FileName: isGLSL == "1" ? "Ledger" : "SubLedger"  //$('#txtSearchSubLedger').val()
                });
                final = {
                    Master: master
                }

                location.href = "/Accounts_Form/LedgerMaster/excel?ReportName=" + JSON.stringify(final);
            }
        }
        else {
            alert('Sorry ! There is no any Data Available to download.'); return false;
        }
        
    }
}

function AccountRule() {
    var V = "{SectorID : '" + $("#ddlSector").val() + "'}";
    $.ajax({
        url: '/Accounts_Form/AccountMaster/AccountRule',
        data: V,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (D) {
            var t = D.Data;
            if (t.length > 0 && D.Status == 200) {

                MaintainAccCode = t[0].MaintainAccCode;
            }
        },
        error: function () { }
    });
}