﻿var ServerResponse = "";
var minDATE = "", maxDATE = "";

$(function () {

 var frm = $('#frmApproval');
    BRS.validation();
	
    BRS.Auto_Bank();

    $('#part_btn').hide();

    $('#btnRefresh').click(function () {
        BRS.reset();
    });

    $('#btnShow').click(function () {
        if (!frm.valid()) {
            return false;
        }
        BRS.Show_Detail();
    });
	
    $('#txtBank').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtBank").val('');
            $("#hdnBank").val('');
        }
        if (iKeyCode == 46) {
            $("#txtBank").val('');
            $("#hdnBank").val('');
        }
    });

    $('#btnSave').click(function () {
	if (!frm.valid()) {
            return false;
        }
        BRS.Save();
    });

    $('#ddlGlobalFinYear').change(function () {
        Set_DateBetween_FinYear();
    });

    Set_DateBetween_FinYear();
	
});

var BRS = {
   
    Auto_Bank : function()
    {
        $("#txtBank").autocomplete({
            source: function (request, response) {
                var Type = "";
                var Value = $.trim($("#txtBank").val());
                var isNumorStr = $.isNumeric(Value);

                if (isNumorStr)
                    Type = "C";  //FOR CODE
                else
                    Type = "D";  //FOR DESCRIPTION

                var V = "{Desc:'" + $("#txtBank").val() + "', SectorID:'" + $("#ddlSector").val() + "', Type:'" + Type + "'}";
                var URL = "/Accounts_Form/Accounts_BRS/Auto_Bank";

                $.ajax({
                    url: URL,
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: V,
                    dataType: 'json',
                    success: function (D) {
                        var t = D.Data;
                        var DataAutoComplete = [];
                        if (t.length > 0) {
                            $.each(t, function (index, item) {
                                DataAutoComplete.push({
                                    label: item.AccountDescription,
                                    AccountCode: item.AccountCode
                                });
                            });
                            response(DataAutoComplete);
                        }
                    }
                });
            },
            minLength: 0,
            select: function (e, i) {
                $("#hdnBank").val(i.item.AccountCode);
            },
        }).click(function () {
            $(this).autocomplete('search', ($(this).val()));
        });
    },
    Show_Detail: function () {

        var frmDate = ""; var tDate = "";
        if ($("#txtFrom").val() != "")
        {
            var a = ($("#txtFrom").val()).split("/");
            frmDate = a[2] + "-" + a[1] + "-" + a[0];
        }
        if ($("#txtTo").val() != "") {
            var b = ($("#txtTo").val()).split("/");
            tDate = b[2] + "-" + b[1] + "-" + b[0];
        }
            
            
        var V = "{AccountCode:'" + $("#hdnBank").val() + "', CorresOption:'" + $("#ddlCorrsOption").val() + "', InstRecIss:'" + $("#ddlInstRecIss").val() + "', " + 
                "FromDate:'" + frmDate + "', ToDate:'" + tDate + "', InstrumentNo:'" + $("#txtInstNo").val() + "', SectorID:'" + $("#ddlSector").val() + "'}";

        var URL = "/Accounts_Form/Accounts_BRS/Show_Detail";

        $.ajax({
            url: URL,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: V,
            dataType: 'json',
            success: function (D) {
                var t = D.Data;
                if(t.length > 0 && D.Status ==200)
                {
                    BRS.populate(t);
                }
            }
        });
    },
    populate: function(Data)
    {
        //$('#part_btn').show();
        var TotalInstAmt = 0, TotalVchAmt = 0;
        var table = $("#myTable");
        table.find('thead').empty();
        table.find('tbody').empty();
        table.find('tfoot').empty();

        //Header
        table.find("thead").append("<tr><th>Instrument Type</th><th>InstrumentNo.</th><th>Instrument Date</th>"+
                                   "<th>Instrument Amount</th><th>Cleared</th><th>Cleared on Date</th>"+
                                   "<th>Amount Vouchered</th><th>Drawee Bank/Branch</th><th></th></tr>");

        //Data
        for (var i = 0; i < Data.length; i++) {

            TotalInstAmt = parseFloat(TotalInstAmt) + parseFloat(Data[i].AdjustmentAmount);
            TotalVchAmt = parseFloat(TotalVchAmt) + parseFloat(Data[i].AdjustmentOtherAmount);

            table.find("tbody").append("<tr class='myData'>" +
                "<td style='display:none;' class='BillID'>" + Data[i].BillId + "</td>" +
                "<td style='display:none;' class='cls_VNo'>" + Data[i].VoucherNumber + "</td>" +

                "<td style='text-align:center; width:140px'><select disabled class='form-control cls_InstType' id='ddlInstType_" + i + "' >" +
                "<option value='Q'>Cheque</option>" +
                "<option value='R'>NEFT/RTGS</option>" +
                "<option value='D'>Draft</option>" +
                "<option value='T'>ByTransfer</option>" +
                "</select></td>" +

                "<td class='cls_InstNo'>" + Data[i].INST_NO + "</td>" +
                "<td style='text-align:center;' class='cls_InstDate'>" + Data[i].INST_DT + "</td>" +

                "<td class='cls_AdjAmount'>" + (Data[i].AdjustmentAmount).toFixed(2) + "</td>" +

                "<td style='width:120px'><select class='form-control  cls_Cleared' id='ddlCleared_" + i + "'>" +
                "<option value='N'>Pending</option>" +
                "<option value='C'>Cleared</option>" +
                "<option value='R'>Return</option>" +
                "</select></td>" +

                "<td style='text-align:center; width:115px'><input ID='txtClearedOn_" + i + "' class='cls_clearedOn clsFinDate input-sm form-control' type='text' value='" + Data[i].CLEARED_ON + "' placeholder='DD/MM/YYYY' />" +
                "<script type='text/javascript'> $('#txtClearedOn_" + i + "').datepicker({" +
                "showOtherMonths: true," +
                "selectOtherMonths: true,"+
                "closeText: 'X',"+
                "showAnim: 'drop',"+
                "changeYear: true,"+
                "changeMonth: true,"+
                "duration: 'slow'," +
                "minDate: '" + ((Data[i].INST_DT == "" || Data[i].INST_DT == null) ? minDATE : Data[i].INST_DT) + "'," +
				"maxDate: '" + maxDATE + "'," +
                "dateFormat: 'dd/mm/yy'"+
                "});</script></td>" +

                //"<td style='width:150px' ><input class='cls_VchAmount form-control' type='text' style='text-align:right;' value='" + (Data[i].AdjustmentOtherAmount).toFixed(2) + "' /></td>" +
                //"<td style='width:150px'><input class='cls_InFavour form-control' type='text' value='" + Data[i].InFavour + "' /></td>" +
                //"</tr>");

                "<td style='width:150px' ><input class='cls_VchAmount form-control' type='text' style='text-align:right;' value='" + (Data[i].AdjustmentOtherAmount).toFixed(2) + "' /></td>" +
                "<td style='width:150px'><input class='cls_InFavour form-control' type='text' value='" + Data[i].InFavour + "' /></td>" +
                "<td style='cursor:pointer; color:blue;'><span onclick='BRS.Save(this, 1)'>Save</span>/<span onclick='BRS.Save(this, 2)'>View</span></td>" +
                "</tr>");

            $('#ddlInstType_' + i).val(Data[i].InstrumentType);
            $('#ddlCleared_' + i).val(Data[i].CLEARED);
			//Set_DateBetween_FinYear();
        }
        //Footer
        table.find("tfoot").append("<tr>" +
            "<td colspan='3' style='font-weight:bold;'>Total (&#8377)</td>" +
            "<td style='font-weight:bold;'>" + TotalInstAmt.toFixed(2) + "</td>" +
            "<td colspan='3' style='font-weight:bold;'>" + TotalVchAmt.toFixed(2) + "</td>" +
            "<td colspan='2'></td></tr > ");
    },
    Save: function (ID, Con)
    {
        if (Con == 1) {

            var res = confirm('Are you sure want to final save ?')
            if (res == true) {
                var myArrary = [];
                var table = $("#myTable");

                //table.find('tbody  tr.myData').each(function () {
                //    var clearedOn = "";
                //    var BillID = $(this).find(".BillID").text();
                //    var Cleared = $(this).find(".cls_Cleared").val();
                //    var ClearedOn = $(this).find(".cls_clearedOn").val();
                //    var VchAmount = $(this).find(".cls_VchAmount").val();
                //    var InFavour = $(this).find(".cls_InFavour").val();

                //    if (ClearedOn != "") {
                //        var aa = ClearedOn.split("/");
                //        clearedOn = aa[2] + "-" + aa[1] + "-" + aa[0];
                //    }

                //    myArrary.push({ 'BillID': BillID, 'Cleared': Cleared, 'ClearedOn': clearedOn, 'VchAmount': VchAmount, 'InFavour': InFavour });
                //});

                var clearedOn = "";
                var BillID = $(ID).closest('tr').find(".BillID").text();
                var Cleared = $(ID).closest('tr').find(".cls_Cleared").val();
                var ClearedOn = $(ID).closest('tr').find(".cls_clearedOn").val();
                var VchAmount = $(ID).closest('tr').find(".cls_VchAmount").val();
                var InFavour = $(ID).closest('tr').find(".cls_InFavour").val();

                if (ClearedOn != "") {
                    var aa = ClearedOn.split("/");
                    clearedOn = aa[2] + "-" + aa[1] + "-" + aa[0];
                }
                myArrary.push({ 'BillID': BillID, 'Cleared': Cleared, 'ClearedOn': clearedOn, 'VchAmount': VchAmount, 'InFavour': $.trim(InFavour) });

                var V = "{Master:" + JSON.stringify(myArrary) + "}";
                var URL = "/Accounts_Form/Accounts_BRS/Save";

                $.ajax({
                    url: URL,
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: V,
                    dataType: 'json',
                    success: function (D) {
                        var t = D.Data;
                        if (t == "success" && D.Status == 200) {
                            alert('Details has been changed Successfully !!');
                            BRS.Show_Detail();
                            return false;
                        }
                        else {
                            alert('There is Some Problem !!');
                            return false;
                        }
                    }
                });
            }
        }
        else {
            VchNo = $(ID).closest('tr').find('.cls_VNo').text();
            window.open('/Accounts_Form/VoucherMaster/Index/' + VchNo, '_blank');
        }
    },
	reset: function () {
        $('#txtBank').val(''); $('#hdnBank').val('');
        $('#txtFrom').val('');
        $('#txtTo').val(''); 
        $('#txtInstNo').val('');
        $('#searchInput').val('');
         $('#myTable tbody tr.myData').empty();
    },
    validation: function () {
        jQuery.validator.addMethod("validDate", function (value, element) {
            return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
        });

        $.validator.addMethod('validBank', function (value, element) {
            if ($('#hdnBank').val() == "")
                return false;
            else
                return true;
        });

        $('#frmApproval').validate({
            rules: {
                txtFrom: {
                    required: true,
                    validDate: true
                },
                txtTo: {
                    required: true,
                    validDate: true
                },
                txtBank: {
                    required: true,
                    validBank: true
                }
            },
            messages: {
                txtFrom: {
                    required: "Please enter from date",
                    validDate: "Please enter a valid date in the format DD/MM/YYYY"
                },
                txtTo: {
                    required: "Please enter to date",
                    validDate: "Please enter a valid date in the format DD/MM/YYYY"
                },
                txtBank: {
                    required: "Please select bank",
                    validBank: "Please select valid bank"
                }
            }
        });
    }
}

function SearchTable() {

    var forSearchprefix = $("#searchInput").val().trim().toUpperCase();
    var tablerow = $('#myTable').find('.myData');
    $.each(tablerow, function (index, value) {
        var cls_InFavour = $(this).find('.cls_InFavour').val().toUpperCase();
        var cls_VchAmount = $(this).find('.cls_VchAmount').val().toUpperCase();
        var cls_clearedOn = $(this).find('.cls_clearedOn').val().toUpperCase();
        var cls_Cleared = $(this).find('.cls_Cleared option:selected').text().toUpperCase();
        var cls_AdjAmount = $(this).find('.cls_AdjAmount').text().toUpperCase();
        var cls_InstDate = $(this).find('.cls_InstDate').text().toUpperCase();
        var cls_InstNo = $(this).find('.cls_InstNo').text().toUpperCase();
        var cls_InstType = $(this).find('.cls_InstType option:selected').text().toUpperCase();

        if (cls_InFavour.indexOf(forSearchprefix) > -1 || cls_VchAmount.indexOf(forSearchprefix) > -1 || cls_clearedOn.indexOf(forSearchprefix) > -1
            || cls_Cleared.indexOf(forSearchprefix) > -1 || cls_AdjAmount.indexOf(forSearchprefix) > -1 || cls_InstDate.indexOf(forSearchprefix) > -1
            || cls_InstNo.indexOf(forSearchprefix) > -1 || cls_InstType.indexOf(forSearchprefix) > -1) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}
function Set_DateBetween_FinYear() {

    //var finyear = $("#ddlGlobalFinYear").val();
    //var FinYear = $("#ddlGlobalFinYear option:selected").text();

    //if (finyear == "O") {
    //    var dateFrom = "", dateTo = "";
    //    if (FinYear != "") {
    //        var sFinYear = FinYear.split('-');
    //        var s = sFinYear[0];
    //        var t = sFinYear[1];

    //        minDATE = "01/04/" + s;
    //        maxDATE = "31/03/" + t;
    //    }
        ////$('.clsFinDate').prop('readonly', false);
        $('.clsFinDate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            closeText: 'X',
            showAnim: 'drop',
            changeYear: true,
            changeMonth: true,
            duration: 'slow',
            dateFormat: 'dd/mm/yy',
            minDate: minDATE,
            maxDate: maxDATE
        });
    //}
    //else {
    //    $('.clsFinDate').datepicker('destroy');
    //    $('.clsFinDate').prop('readonly', true);
    //    //$('.clsFinDate').val('');
    //}
}
