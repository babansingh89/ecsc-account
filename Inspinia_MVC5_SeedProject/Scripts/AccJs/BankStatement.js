﻿$(document).ready(function () {

    SetFrom_Date();

    $('#txtToDate').datepicker('setDate', 'today');

    var date = new Date();
    date.setMonth(date.getMonth() - 1);
    date.setFullYear(date.getFullYear());
    if ((date.getMonth() > 10)) {
        $("#txtFromDate").val('0' + (date.getDate()) + '/' + (date.getMonth() + 1) + '/' + (date.getFullYear()));
    } else {
        $("#txtFromDate").val('0' + (date.getDate()) + '/' + '0' + (date.getMonth() + 1) + '/' + (date.getFullYear()));
    }

    $(".allownumericwithoutdecimal").on("keypress keyup blur, onkeydown", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    //    BANK AUTOCOMPLETE
    $('#txtBankName').autocomplete({
        source: function (request, response) {

            var S = "{TransactionType:'" + "Vendor_BankAccountNo" + "' ,Desc:'" + $('#txtBankName').val() + "', MND:'" + "B" + "', SetorID:'" + $('#ddlSector').val() + "'}"; //alert(S);
            $.ajax({
                url: '/Accounts_Form/BankStatement/Auto_Bank',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode,
                                AccountNumber: item.AccountNumber
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnBankName').val(i.item.AccountCode);
            //var remrks = "Please credit the following S.B. A/cs, with the amount shown against the name of the following Persons/Vendor/Institute, after debiting the University Fund A/C. maintained at your Branch bearing";
            //if (i.item.AccountNumber != "" && i.item.AccountNumber != null) {

            //    $('#txtRemarks').val(remrks + " A/c No. " + i.item.AccountNumber + ".");
            //}
            //else
            //{
            //    $('#txtRemarks').val(remrks + ".");
            //}

            //BindGrid(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtBankName').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtBankName").val('');
            $("#hdnBankName").val('');
        }
        if (iKeyCode == 46) {
            $("#txtBankName").val('');
            $("#hdnBankName").val('');
        }
    });

    //MEMO NO. AUTOCOMPLETE
    $('#txtSearchMemoNo').autocomplete({
        source: function (request, response) {

            var S = "{TransType: '" + "MemoNo" + "', AccountCode: '" + "" + "', Desc: '" + $('#txtSearchMemoNo').val() + "'}";
            $.ajax({
                url: '/Accounts_Form/BankStatement/BindVendor',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {

                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.MemoNo,
                                StatementID: item.StatementID
                            });
                        });

                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $('#hdnSearchMemoNo').val(i.item.StatementID);
            //Print(i.item.StatementID);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtSearchMemoNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtSearchMemoNo").val('');
            $("#hdnSearchMemoNo").val('');
        }
        if (iKeyCode == 46) {
            $("#txtSearchMemoNo").val('');
            $("#hdnSearchMemoNo").val('');
        }
    });

    //CHEQUE NO. AUTOCOMPLETE
    $('#txtChequeNo').autocomplete({
        source: function (request, response) {

            //var S = "{TransType: '" + "ChequeNo" + "', AccountCode: '" + $('#hdnBankName').val() + "', Desc: '" + $('#txtChequeNo').val() + "',}";

            var S = "{TransType: '" + "ChequeNo" + "', AccountCode: '" + $('#hdnBankName').val() + "', FromDate: '" + $('#txtFromDate').val() + "', To: '" + $('#txtToDate').val() + "', Desc: '" + $('#txtChequeNo').val() + "'}";

            $.ajax({
                url: '/Accounts_Form/BankStatement/BindVendor',
                type: 'POST',
                data: S,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function (serverResponse) {
                    var AutoComplete = [];
                    if ((serverResponse.Data).length > 0) {
                        $.each(serverResponse.Data, function (index, item) {
                            AutoComplete.push({
                                label: item.InstrumentNumber,
                                InstrumentNumber: item.InstrumentNumber
                            });
                        });
                        response(AutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtChequeNo').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtChequeNo").val('');
        }
        if (iKeyCode == 46) {
            $("#txtChequeNo").val('');
        }
    });

    //Show Button Click
    $("#btnShow").click(function () {
        BindGrid();
    });
    
});

function SetFrom_Date() {
    $('#txtFromDate, #txtToDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}

function BindGrid() {
    if ($("#txtFromDate").val() == "") { $("#txtFromDate").focus(); return false; }
    if ($("#txtToDate").val() == "") { $("#txtToDate").focus(); return false; }
    if ($("#hdnBankName").val() == "") { $("#txtBankName").focus(); return false; }
    if ($("#txtChequeNo").val() == "") { $("#txtChequeNo").focus(); return false; }


    var E = "{TransType: '" + "Load" + "', AccountCode: '" + $('#hdnBankName').val() + "', FromDate: '" + $('#txtFromDate').val() + "', To: '" + $('#txtToDate').val() + "', ChequeNo: '" + $('#txtChequeNo').val() + "', Desc: '" + "" + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/BankStatement/BindVendor',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            var t = data.Data;

            $('#datatable tbody tr').remove();
            if (data.Status == 200 && t.length > 0) {

                var columnData = [{ "mDataProp": "VoucherNumber" },
                { "mDataProp": "RefVoucherSlNo" },
                { "mDataProp": "ReferenceDate" },
                { "mDataProp": "AccountCode" },
                { "mDataProp": "AccountDescription" },
                { "mDataProp": "Amount" },
                { "mDataProp": "IFSCCode" },
                { "mDataProp": "BankName" },
                { "mDataProp": "VoucherDetailID" },
                //{ "mDataProp": "InstrumentType" },
                //{ "mDataProp": "InstrumentNumber" },
                { "mDataProp": "BranchName" },
                { "mDataProp": "AccountNumber" },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        return ' <input type="checkbox" class="chk" value="' + row.VoucherNumber + '" checked/>';

                    }
                }];

                var columnDataHide = [0, 3, 8];

                var oTable = $('#datatable').DataTable({
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        { extend: 'copy' },
                        { extend: 'csv' },
                        { extend: 'excel', title: 'MyExcel' },
                        { extend: 'pdf', title: 'MyPDF', width: '100%' },
                        {
                            extend: 'print',
                            customize: function (win) {
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                            }
                        }
                    ],

                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aaData": t,

                    "aoColumns": columnData,
                    "aoColumnDefs": [
                        {
                            "targets": columnDataHide,
                            "visible": false,
                            "searchable": false
                        },
                        //{
                        //    "targets": [9],
                        //    "className": "text-center instrumenttype",
                        //    "width": "10%"
                        //},
                        {
                            "targets": [0],
                            "className": "vchno"
                        },
                        {
                            "targets": [2],
                            "className": "text-center vchdate",
                            "width": "11%"
                        },
                        {
                            "targets": [3],
                            "className": "acccode"
                        },
                        {
                            "targets": [4],
                            "className": "vendor"
                        },
                        {
                            "targets": [5],
                            "className": "text-right amount"
                        },
                        {
                            "targets": [6],
                            "className": "ifsccode"
                        },
                        {
                            "targets": [7],
                            "className": "bankname"
                        },
                        {
                            "targets": [8],
                            "className": "voucherdetailid"
                        },
                        {
                            "targets": [9],
                            "className": "text-center"
                        },
                        //{
                        //    "targets": [10],
                        //    "className": "text-center instnumber"
                        //},
                        {
                            "targets": [10],
                            "className": "text-center BranchName"
                        },
                        {
                            "targets": [11],
                            "className": "text-center AccountNumber"
                        }
                    ],
                    footerCallback: function (row, data, start, end, display) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                            .column(5)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Total over this page
                        pageTotal = api
                            .column(5, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(5).footer()).html(
                            'Page Total:'+'$' + pageTotal + 'Total:'+' ( $' + total +')'
                        );
                    },
                    'iDisplayLength': 10,
                    destroy: true,
                });
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            AddErrorAlert("error", "request failed");
        }

    });

}

function Refresh() {
    $("#txtBankName").val(''); $("#hdnBankName").val('');
    $("#ddlSignature").val(''); $("#txtMemoNo").val('');
    $("#txtMemoDate").val(''); $("#txtChequeNo").val('');
    $("#txtChequeDate").val(''); $("#txtRemarks").val('');
    $('#datatable tbody tr').remove();
}

function Preview() {
    var grdLen = $('#datatable tbody tr').length; var ArrList = [];
    if (grdLen > 0) {

        var html = "", html = "";


        var oTable = $('#datatable').dataTable();
        var rowcollection = oTable.$(".chk:checked", { "page": "all" });
        rowcollection.each(function (index, elem) {
            var checkbox_value = $(elem).val();

            var row = $(this).closest('tr');
            var date = $(this).closest('tr').find('.vchdate').text();
            var vendor = $(this).closest('tr').find('.vendor').text();
            var amount = $(this).closest('tr').find('.amount').text();
            var vchno = oTable.dataTable().fnGetData(row).VoucherNumber;

            html += "<tr>";
            html += "<td>" + vendor + "</td>";
            html += "<td style='text-align:center;'>" + date + "</td>";
            html += "<td style='text-align:right;'>" + amount + "</td>";
            html += "</tr>";
        });

        var table = $('#tbl tbody');
        table.empty();
        table.append(html);
        $("#dialog").modal('show');
    }
}

function Save() {
    if ($("#txtFromDate").val() == "") { $("#txtFromDate").focus(); return false; }
    if ($("#txtToDate").val() == "") { $("#txtToDate").focus(); return false; }
    if ($("#hdnBankName").val() == "") { $("#txtBankName").focus(); return false; }

    if ($("#txtRemarks").val() == "") {
        toastr.error('Please, Enter Remarks !!', 'Warning'); $("#txtRemarks").focus(); return false;
    }

    var grdLen = $('#datatable tbody tr').length;
    if (grdLen > 0) {

        var ArrList = [];
        var oTable = $('#datatable').dataTable();
        var rowcollection = oTable.$(".chk:checked", { "page": "all" });
        rowcollection.each(function (index, elem) {
            var checkbox_value = $(elem).val();
            ArrList.push({
                'checkbox_value': checkbox_value
            });
        });
    }
    else {
        toastr.error('Sorry !! No Vendors available !!', 'Warning'); return false;
    }

    if (ArrList.length == 0) {
        toastr.error('Please, Select Vendors !!', 'Warning'); return false;
    }


    swal({
        title: "Warning",
        text: "Are you sure want to save selected Vendors ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                Confirm_Save();
            }

        });
}
function Confirm_Save() {

    var ArrList = [];
    var oTable = $('#datatable').dataTable();
    var rowcollection = oTable.$(".chk:checked", { "page": "all" });
    rowcollection.each(function (index, elem) {
        var checkbox_value = $(elem).val();

        var row = $(this).closest('tr');
        var vchno = oTable.dataTable().fnGetData(row).VoucherNumber;
        var acccode = oTable.dataTable().fnGetData(row).AccountCode;
        var date = $(this).closest('tr').find('.vchdate').text();
        var amount = $(this).closest('tr').find('.amount').text();
        var vendor = $(this).closest('tr').find('.vendor').text();
        var ifsc = $(this).closest('tr').find('.ifsccode').text();
        var bankname = $(this).closest('tr').find('.bankname').text();
        var branchname = $(this).closest('tr').find('.BranchName').text();
        var instrumenttype = $(this).closest('tr').find('.instrumenttype').text();
        var voucherdetailid = oTable.dataTable().fnGetData(row).VoucherDetailID; //$(this).closest('tr').find('.voucherdetailid').text();
        var InstrumentNumber = oTable.dataTable().fnGetData(row).InstrumentNumber;

        ArrList.push({
            VoucherNo: vchno, 'AccountCode': acccode, 'VendorName': vendor, 'VoucherDate': date, 'Amount': amount, 'IFSC': ifsc, 'BankName': bankname, 'BranchName': branchname, 'VoucherDetailID': voucherdetailid, 'InstrumentNumber': InstrumentNumber, 'InstrumentType': instrumenttype
        });
    });

    var EffectedGL = JSON.stringify(ArrList);
    var obj = "{ BankAccountCode: '" + $('#hdnBankName').val() + "', Remarks: '" + $('#txtRemarks').val() + "', EffectedGL: " + EffectedGL + ", SectorID: '" + $('#ddlSector').val() + "'}";

    //alert(EffectedGL);
    //return false;

    $.ajax({
        url: '/Accounts_Form/BankStatement/Insert',
        type: 'POST',
        data: obj,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            var t = response.Data;

            if (t != "") {
                swal({
                    title: "Success",
                    text: "Bank Statement has been Successfully Inserted.\nMemoNo. is " + t,
                    type: "success",
                    confirmButtonColor: "#AEDEF4",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                    closeOnCancel: true,
                },
                    function (isConfirm) {
                        if (isConfirm) {
                            BindGrid();
                            //Print(t);
                        }
                    });
            }
        }
    });
}
function Print(StatementID, MemoNo) {
    var final = {}; var master = []; var detail = [];

    master.push({
        ReportName: "BankStatement.rpt",
        FileName: MemoNo.replace('&', '')
    });

    detail.push({
        StatementID: StatementID
    });

    final = {
        Master: master,
        Detail: detail
    }

    var left = ($(window).width() / 2) - (950 / 2),
        top = ($(window).height() / 2) - (650 / 2),
        popup = window.open("/OpenReport/OpenReport.aspx?ReportName=" + JSON.stringify(final), "popup", "width=950, height=650, top=" + top + ", left=" + left);
    popup.focus();

}

function Search() {
    $('#dialog-print').modal('show');
}
function SearchPrint() {
    Print($("#hdnSearchMemoNo").val());
}
function CloseMemoNo() {
    swal({
        title: "Warning",
        text: "Are you sure want to Close this Memo No. ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                if ($("#hdnSearchMemoNo").val() != "") {
                    var E = "{TransType: '" + "CloseMemo" + "', StatementID: '" + $("#hdnSearchMemoNo").val() + "'}";
                    $.ajax({
                        type: "POST",
                        url: '/Accounts_Form/BankStatement/CloseMemo',
                        contentType: "application/json; charset=utf-8",
                        data: E,
                        dataType: "json",
                        async: false,
                        success: function (data, status) {
                            var t = data.Data;
                            var result = t[0].Result;

                            if (result == "success") {
                                swal({
                                    title: "Success",
                                    text: "Memo No. Closed Successfully !!",
                                    type: "success",
                                    confirmButtonColor: "#AEDEF4",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: true,
                                },
                                    function (isConfirm) {
                                        if (isConfirm) {
                                            $("#txtSearchMemoNo").val(''); $("#hdnSearchMemoNo").val('');
                                        }
                                    });
                            }
                        }
                    });
                }
                else {
                    $("#txtSearchMemoNo").focus(); return false;
                }
            }

        });
}

function GeneratedAdvice() {
    var E = "{TransType: '" + "GeneratedAdvice" + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/BankStatement/GeneratedAdvice',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        async: false,
        success: function (data, status) {
            var t = data.Data;

            $('#tbl_Advice tbody tr').remove();
            if (data.Status == 200 && t.length > 0) {

                var columnData = [{ "mDataProp": "StatementID" },
                { "mDataProp": "MemoNo" },
                { "mDataProp": "MemoDate" },
                { "mDataProp": "InstrumentNo" },
                { "mDataProp": "BankName" },
                { "mDataProp": "Status" },
                { "mDataProp": "Bank" },
                {
                    "mDataProp": "Edit",
                    "render": function (data, type, row) {
                        return '<div class="clsAction">' +
                            "<img src='/Content/Images/Save.png' title='Save' style='height: 22px; width: 22px; cursor: pointer; ' onclick='Save_Row(\"" + row.StatementID + "\")' />  " +
                            "<img src='/Content/Images/download.png' title='Excel' style='height: 17px; width: 19px; cursor: pointer; ' onclick='Excel(\"" + row.StatementID + "\", \"" + row.MemoNo + "\", \"" + row.Bank + "\")' />   " +
                            "<img src='/Content/Images/Delete.png' title='Delete' style='height: 20px; width: 20px; cursor: pointer; display:" + (row.Status == "D" ? "" : "none") + "' onclick='Delete_Row(\"" + row.StatementID + "\")' />  " +
                            "<img src='/Content/Images/close.png' title='Close' style='height: 18px; width: 18px; cursor: pointer; display:" + (row.Status == "P" ? "" : "none") + "' onclick='Close_Row(\"" + row.StatementID + "\")' />" +
                            '</div>'
                    }
                }];
                //,
                //

                var columnDataHide = [0, 5];

                var oTable = $('#tbl_Advice').DataTable({
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [],
                    "oLanguage": {
                        "sSearch": "Search all columns:"
                    },
                    "aaData": t,

                    "aoColumns": columnData,
                    "aoColumnDefs": [
                        {
                            "targets": columnDataHide,
                            "visible": false,
                            "searchable": false
                        },
                        {
                            "targets": [0],
                            "className": "StatementID"
                        },
                        {
                            "targets": [2, 3],
                            "className": "text-center"
                        },
                        {
                            "targets": [6],
                            "className": "Bank",
                            "width": "10%"
                        },
                        {
                            "targets": [7],
                            "className": "text-center"
                        }
                    ],
                    'iDisplayLength': 10,
                    destroy: true,
                });

            }

        },
        error: function (xhr, textStatus, errorThrown) {
            AddErrorAlert("error", "request failed");
        }

    });
}
function Delete_Row(StatementID) {
    swal({
        title: "Warning",
        text: "Are you sure want to delete selected Memo. ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                if (StatementID != "") {
                    var E = "{TransType: '" + "DeleteMemo" + "', StatementID: '" + StatementID + "'}";
                    $.ajax({
                        type: "POST",
                        url: '/Accounts_Form/BankStatement/CloseMemo',
                        contentType: "application/json; charset=utf-8",
                        data: E,
                        dataType: "json",
                        async: false,
                        success: function (data, status) {
                            var t = data.Data;
                            var result = t[0].Result;
                            if (result == "success") {
                                GeneratedAdvice();
                            }
                        }
                    });
                }
            }
        });
}
function Close_Row(StatementID) {
    swal({
        title: "Warning",
        text: "Are you sure want to close selected Memo. ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                if (StatementID != "") {
                    var E = "{TransType: '" + "CloseMemo" + "', StatementID: '" + StatementID + "'}";
                    $.ajax({
                        type: "POST",
                        url: '/Accounts_Form/BankStatement/CloseMemo',
                        contentType: "application/json; charset=utf-8",
                        data: E,
                        dataType: "json",
                        async: false,
                        success: function (data, status) {
                            var t = data.Data;
                            var result = t[0].Result;
                            if (result == "success") {
                                GeneratedAdvice();
                            }
                        }
                    });
                }
            }
        });
}
function Print_Row(StatementID, MemoNo) {
    Print(StatementID, MemoNo);
}
function Save_Row(StatementID) {
    swal({
        title: "Warning",
        text: "Are you sure want to final save selected Memo. ??",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                if (StatementID != "") {
                    var E = "{TransType: '" + "FinalSave" + "', StatementID: '" + StatementID + "'}";
                    $.ajax({
                        type: "POST",
                        url: '/Accounts_Form/BankStatement/CloseMemo',
                        contentType: "application/json; charset=utf-8",
                        data: E,
                        dataType: "json",
                        async: false,
                        success: function (data, status) {
                            var t = data.Data;
                            var result = t[0].Result;
                            if (result == "success") {
                                GeneratedAdvice();
                            }
                        }
                    });
                }
            }
        });
}

function Excel(StatementID, MemoNo,Bank) {
    var res = confirm('Are You Sure Want to download Excel Report ??');
    if (res == true) {
        var master = []; var final = {};

        master.push({
            StatementID: StatementID,
            MemoNo: MemoNo,
            SectorID: $("#ddlSector").val(),
            BankNm: Bank
        });
        final = {
            Master: master
        }

        location.href = "/Accounts_Form/BankStatement/Excel?ReportName=" + JSON.stringify(final);
    }
}
