﻿
$(document).ready(function () {

    Set_Date();

    //Sub Ledger hide and show
    $('#btnAdd').click(function () {
        Add();
    });
    $('#btnRefresh').click(function () {
        Refresh();
    });


    
    $(".allownumericwithdecimal").on("keypress keyup blur, onkeydown", function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $(".allownumericwithoutdecimal").on("keypress keyup blur, onkeydown", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    //======================================== ACCOUNT DESCRIPTION AUTO-COMPLETE =============================================
    $("#txtLedger").autocomplete({
        source: function (request, response) {

            var S = "{Desc:'" + $('#txtLedger').val() + "', MND:'" + isGLSL + "'}"; //alert(S);

            $.ajax({
                type: "POST",
                url: '/Accounts_Form/AccountMaster/Account_Description',
                data: S,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (D) {
                    var t = D.Data;
                    var DataAutoComplete = [];
                    if (t.length > 0) {
                        $.each(t, function (index, item) {
                            DataAutoComplete.push({
                                label: item.AccountDescription,
                                AccountCode: item.AccountCode

                            });
                        });
                        response(DataAutoComplete);
                    }
                }
            });
        },
        select: function (e, i) {
            $("#hdnLedger").val(i.item.AccountCode);
        },
        minLength: 0
    }).click(function () {
        $(this).autocomplete('search', ($(this).val()));
    });
    $('#txtLedger').keydown(function (evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode == 8) {
            $("#txtLedger").val('');
            $("#hdnLedger").val('');
        }
        if (iKeyCode == 46) {
            $("#txtLedger").val('');
            $("#hdnLedger").val('');
        }
    });


});

//Date Checking
function Set_Date() {
    $('#txtInvestmentDate, #txtMaturityDate, #txtReleaseDate, #txtDueDate, #txtReceiveDate').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        closeText: 'X',
        showAnim: 'drop',
        changeYear: true,
        changeMonth: true,
        duration: 'slow',
        dateFormat: 'dd/mm/yy'
    });
}

function Add() {
    var addValue = $(".clsVariables").attr('data-add');

    if ($("#txtDueDate").val() == "") { $("#txtDueDate").focus(); return false; }
    if ($("#txtReceiveDate").val() == "") { $("#txtReceiveDate").focus(); return false; }
    if ($("#txtIntAmt").val() == "") { $("#txtIntAmt").focus(); return false; }
    if ($("#txtBankCode").val() == "") { $("#txtBankCode").focus(); return false; }
    if ($("#txtChequeNo").val() == "") { $("#txtChequeNo").focus(); return false; }
    if ($("#txtVoucherNo").val() == "") { $("#txtVoucherNo").focus(); return false; }


    //var l = $('#tbl tr.myData').find("td[chk-data='" + chkAccCode + "']").length;
    //if (l > 0) {
    //    alert('Sorry ! This Account Description is Already Available.'); $("#txtEfftGL").val(''); $("#hdnEffectAdd").val(''); return false;
    //}

    var html = ""

    var $this = $('#tbl .test_0');
    $parentTR = $this.closest('tr');

    html += "<tr class='myData' >"
        + "<td style='display:none;' class='clsAccCode' chk-data='" + $("#txtDueDate").val() + "' >" + $("#txtDueDate").val() + "</td>"
        + "<td>" + $("#txtDueDate").val() + "</td>"
        + "<td>" + $("#txtReceiveDate").val() + "</td>"
        + "<td>" + $("#txtIntAmt").val() + "</td>"
        + "<td>" + $("#txtBankCode").val() + "</td>"
        + "<td>" + $("#txtChequeNo").val() + "</td>"
        + "<td>" + $("#txtVoucherNo").val() + "</td>"
        + "<td style='text-align:center'>"
        + "<button type='button' class='btn'><span class='glyphicon glyphicon-floppy-save' style='padding-right:4px'></span>Post</button>"
        + "<img src= '/Content/Images/Delete.png' title= 'Edit' style= 'width:20px;height:20px;cursor:pointer; text-align:center;' onClick= 'app.DeleteInstDetail(this);' /></td > "
    html + "</tr>";

    $parentTR.after(html);

    $("#txtDueDate").val(''); $("#txtReceiveDate").val(''); $("#txtIntAmt").val(''); $("#txtBankCode").val(''); $("#txtChequeNo").val(''); $("#txtVoucherNo").val('');
}

function Refresh() {
    window.location.reload();
}

















