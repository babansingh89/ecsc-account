﻿$(document).ready(function () {

    InitUI();


});
function InitUI() {

    BindGridForAgent();
}
function BindGridForAgent() {

    var E = "{AccountCode: '" + "" + "'}";
    $.ajax({
        type: "POST",
        url: '/Accounts_Form/Item/Load_Item',
        contentType: "application/json; charset=utf-8",
        data: E,
        dataType: "json",
        success: function (data, status) {
            //data = JSON.parse(data);
            var oTable = $('#datatable').DataTable({

                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aaData": data,

                "aoColumns": columnData,
                "aoColumnDefs": [
                     {
                         "targets": columnDataHide,
                         "visible": false,
                         "searchable": false
                     }],
                'iDisplayLength': 10
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            AddErrorAlert("error", "request failed");
        }
    });
}

