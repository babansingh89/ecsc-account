﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.AppResource
{
    public class AppUrl
    {
        public int UrlID { get; set; }
        public string RelativeDirectory { get; set; }
        public string ControllerPackage { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public bool IsSharedUrl { get; set; }
        public bool IsAnonymousUrl { get; set; }
        public string ParamExpression { get; set; }
    }
}