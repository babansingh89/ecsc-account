﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.AppResource
{
    public class EcscSetting
    {
        public int Id { get; set; }
        public string ApprovalServerAddress { get; set; }
        public string ChiperKey { get; set; }
    }
}