﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wbEcsc.Models.Application;

namespace wbEcsc.Models.Account
{
    public class Bill_MD : Account_MD
    {
        public long? BillId { get; set; }
        public long? PoId { get; set; }
        public string BillNo { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string VendEmail { get; set; }
        public string BillStatus { get; set; }
        public string IncomeExpense { get; set; }
        public string Address { get; set; }
        public string ShipTo { get; set; }
        public string ShipToName { get; set; }
        public string ShipAddr { get; set; }
        public string ShipVia { get; set; }
        public string PoNo { get; set; }
        public string BillDate { get; set; }
        public string DueDate { get; set; }
        public decimal? CreditTerms { get; set; }
        public string TaxType { get; set; }
        public string BankID { get; set; }
        public string BankName { get; set; }
        public string InstrumentType { get; set; }
        public string InstrumentNo { get; set; }
        public string InstrumentDate { get; set; }
        public string VendorBillNo { get; set; }
        public string VendorBillDate { get; set; }
        public string TransType { get; set; }
        public decimal? GrossAmt { get; set; }
        public string AdjType { get; set; }
        public string TaxTypeonTax { get; set; }
        public decimal? TaxTypeonTaxAmount { get; set; }
        public decimal? AdjAmt { get; set; }
        public string BillAdjHead { get; set; }
        public decimal? RoundOff { get; set; }
        public decimal? NetAmt { get; set; }
        public string Remarks { get; set; }
        public string DocFilePath { get; set; }
        public int? Sectorid { get; set; }
        public List<BillDetail_MD> BillDetail { get; set; }
        public List<BillSub_MD> BillSubDetail { get; set; }
        public Bill_MD()
        {
            BillDetail = new List<BillDetail_MD>();
            BillSubDetail = new List<BillSub_MD>();
        }
    }
    public class BillDetail_MD
    {
        public long? BillId { get; set; }
        public int? DatBillId { get; set; }
        public string ItemId { get; set; }
        public string ItemDescription { get; set; }
        public string ProductServiseDesc { get; set; }
        public decimal? Qty { get; set; }
        public decimal? Rate { get; set; }
        public decimal? GrossAmt { get; set; }
        public decimal? Discount { get; set; }
        public int? TaxTypeId { get; set; }
        public string TaxTypeDesc { get; set; }
        public decimal? TaxAmt { get; set; }
        public decimal? NetAmt { get; set; }
        public int? sectorid { get; set; }
    }
    public class BillSub_MD
    {
        public long? PoId { get; set; }
        public int? DatPoId { get; set; }
        public int? TaxID { get; set; }
        public int? taxTypeDetID { get; set; }
        public string TaxDes { get; set; }
        public decimal? TaxNetAmount { get; set; }
        public decimal? GrossAmt { get; set; }
    }

    public class BillVch_MD
    {
        public long? TabId { get; set; }
        public long? BillId { get; set; }
        public string VoucherNumber { get; set; }
        public string PostStatus { get; set; }
    }
    public class BillSubVch_MD
    {
        public long? TabId { get; set; }
        public int? Ord { get; set; }
        public string Drcr { get; set; }
        public string AccountCode { get; set; }
        public decimal? Amount { get; set; }
    }
}