﻿namespace wbEcsc.Models.Account
{
    public interface IInternalUser   ///:IUserType
    {
        string Email { get; set; }
        string FirstName { get; set; }
        bool IsAccountLocked { get; set; }
        bool IsMailVerified { get; set; }
        string LastName { get; set; }
        string PhoneNo { get; set; }
        long UserID { get; set; }
        int UserType { get; set; }
    }
}
