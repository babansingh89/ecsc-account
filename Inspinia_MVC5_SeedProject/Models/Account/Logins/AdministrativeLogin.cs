﻿namespace wbEcsc.Models.Account.Logins
{
    public class AdministrativeLogin 
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        
    }
}