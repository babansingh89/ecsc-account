﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class TrailBalance_MD
    {
        public string AccountCode { get; set; }
        public string OldAccountHead { get; set; }
        public string AccountDescription { get; set; }
        public decimal? OpeningBalDR { get; set; }
        public decimal? OpeningBalCR { get; set; }
        public decimal? TransactionBalDR { get; set; }
        public decimal? TransactionBalCR { get; set; }
        public decimal? ClosingBalDR { get; set; }
        public decimal? ClosingBalCR { get; set; }
    }
}