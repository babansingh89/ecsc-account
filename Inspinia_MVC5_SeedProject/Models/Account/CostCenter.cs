﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wbEcsc.Models.Account
{
    public class CostCenter
    {
        public int? VCH_Type { get; set; }
        public string Fromdate { get; set; }
        public string ToDate { get; set; }
        public int? SectorID { get; set; }

        public int? VCH_No { get; set; }
        public string VCH_Date { get; set; }
        public string VCH_Ref { get; set; }
        public string Narration { get; set; }

        public int? VoucherDetailID { get; set; }
        public string AccountCode { get; set; }
        public string OldAccountHead { get; set; }
        public string AccountDescription { get; set; }
        public string DRCR { get; set; }
        public decimal CR_AMT { get; set; }
        public decimal DR_AMT { get; set; }

        public string Amt { get; set; }


        public Int64 BillId { get; set; }
        public string CostCenterCode { get; set; }
        public string CostCenterDesc { get; set; }
        public string DeptCode { get; set; }
        public string DeptDesc { get; set; }
        public decimal AdjustmentAmount { get; set; }
        public string InstrumentNumber { get; set; }
        public string InstrumentDate { get; set; }
        public string AdjustmentNumber { get; set; }
        public string InFavour { get; set; }
        public int? WardNo { get; set; }
        public int? radioVal { get; set; }

        
        public long InsertedBy { get; set; }





    }
}